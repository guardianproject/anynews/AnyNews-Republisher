#!/bin/bash
set -e
# For use with cron:
# 	Run the content retriever with timing control
#

# verbose mode?
MODE=$1

# full path to the aggegator's directory
AHOME=${REPUBLISHER_HOME:-$HOME/Software/guardianproject/anynews}
COMMANDS=./commands

PHP="$(which php)"

# path into which data will be stored
DPATH=$($PHP $COMMANDS/read_config.php FEED_DIRECTORY)

# control the hours at which the retriever actually run
# (assumes cron is running at hourly intervals)
(cd $AHOME; $PHP $COMMANDS/timer.php h 1 4 7 10 13 15 16 17 19 21 22 23)
if [[ $? == 0 ]]
then
	if [[ $MODE == "verbose" ]]
	then
		(cd $AHOME; $PHP $COMMANDS/s3retriever.php $DPATH)
	else
		(cd $AHOME; $PHP $COMMANDS/s3retriever.php $DPATH) > /dev/null
	fi
fi
