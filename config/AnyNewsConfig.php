<?php
/**
 * ----------------------------------------------------------------------
 * component: AnyNewsConfig
 * defined constants for the Guardian Project AnyNews library
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/
 
// DON'T NAMESPACE THIS CLASS 

class AnyNewsConfig {
	// use AWS S3 [s3] or IPFS [ipfs] as your intermediate store 
	const INTERMEDIATE_STORE = 's3';
	
	// AWS S3 information
	const S3_ACCOUNT_NAME = './.s3';
	const S3_BASE_URL     = 'https://s3.amazonaws.com/';
	const S3_VERSION      = '2006-03-01';

	const S3_REGION       = 'us-east-1'; // ADD S3 REGION HERE
	const S3_BUCKET       = '57b004aBUCKET1733AGAIN3b7cb1'; // ADD S3 BUCKET HERE
	
	// Publisher's IPFS IPNS NAME
	const IPNS_NAME       = '/ipns/PUBLISHERS_IPNS_NAME';
	 
	// name of the file containing the feeds to aggregate
	const FEED_LIST = './data/feeds.json';
	
	// feed list backup directory;
	const LIST_BACKUP_DIRECTORY = './data/feeds-backup';
	
	// directory into which to aggregate or dis-assemble the feeds
	const FEED_DIRECTORY = './data/feeds';
	
	// cleanup (remove old files in feed directory) after completion?
	const AUTO_CLEANUP = true;

	// "sell-by" date for feed items.  Only items younger than this 
	// time/date (in seconds) will be aggregated
	const OLDEST_ITEM = 30 * 24 * 60 * 60;   // thirty days

	// filename of to-be-downloaded content bundle
	const CONTENT_BUNDLE = 'blob.tar';
	
	// cache period - how long should content remain on the retriever site? (in days)
	const CACHE_DURATION = 7;
	
	// auto-cleanup enabled or disabled
	const CLEANUP_ENABLED = true;
	
	// for export to shell
	public static function get($val) {
		switch($val) {
			case 'INTERMEDIATE_STORE':
				return self::INTERMEDIATE_STORE;
				break;
			case 'FEED_LIST' :
				return self::FEED_LIST;
				break;
			case 'LIST_BACKUP_DIRECTORY' :
				return self::LIST_BACKUP_DIRECTORY;
				break;
			case 'FEED_DIRECTORY' :
				return self::FEED_DIRECTORY;
				break;
			case 'AUTO_CLEANUP' :
				if (self::AUTO_CLEANUP) { return 'true'; }
				else { return 'false'; }
				break;
			case 'OLDEST_ITEM' :
				return self::OLDEST_ITEM;
				break;
			case 'CONTENT_BUNDLE' :
				return self::CONTENT_BUNDLE;
				break;
			case 'CACHE_DURATION' :
				return self::CACHE_DURATION;
				break;
			case 'CACHE_DURATION' :
				return self::CACHE_DURATION;
				break;
			case 'CLEANUP_ENABLED' :
				if (self::CLEANUP_ENABLED) { return 'true'; }
				else { return 'false'; }
				break;
			case 'S3_ACCOUNT_NAME':
				return self::S3_ACCOUNT_NAME;
				break;
			case 'S3_BASE_URL':
				return self::S3_BASE_URL;
				break;
			case 'S3_BUCKET':
				return self::S3_BUCKET;
				break;
		}
	}
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>
