<?php
/**
 * ----------------------------------------------------------------------
 * component: UtilitiesConfig
 * defined constants for the Guardian Project Proxy Service Utilities library
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/
 
// DON'T NAMESPACE THIS CLASS 

class UtilitiesConfig {

	// directory definitions (not used directly: see static methods below)
	const DB_DIRECTORY    = './data/db'; 	// where YOUR SQLITE databases will reside
	const DATA_DIRECTORY  = './data/data';	// where the raw data to build YOUR databases resides
	const LOG_DIRECTORY   = './data/logs';	// where YOUR APPLICATION will log

	// this is where the Utilities-owned databases and data are located
	const UT_DB_DIRECTORY    = './data/proxyservices/utilities/db';
	const UT_DATA_DIRECTORY  = './data/proxyservices/utilities/data';

	// logging definitions
	const LOGFILE         = 'anynews.log'; 
	const IS_LOGGING      = true; // turn logging on/off
	//const LOG_LEVEL       = 1039; // E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_USER_NOTICE
    const LOG_LEVEL       = E_ERROR | E_NOTICE;  // E_ERROR | E_WARNING | E_PARSE | E_NOTICE

	public static function isLogging() {
		if (self::IS_LOGGING) { return true; }
		return false;
	}
	
	public static function showLogging($quiet = true){
		if (self::IS_LOGGING) {
			print "logging [ON]; messages to file " . self::LOG_DIRECTORY . '/' . self::LOGFILE . "\n";
		} else {
			if (! $quiet) { print "logging [OFF]\n"; }
		}
	}
    
	// encryption-related
	const PRIV_DIRECTORY      = '.priv2'; // RELATIVE to user's HOME directory
	const KEY_FILE            = '.key2';  // RELATIVE to user's HOME directory
	const ENC_ALGORITHM       = 'aes-256-cbc';	
	const ENCRYPT_CREDENTIALS = true;
	
	public static function encryptionKeyFile() { 
		$dir = self::privDirectory();
		return $dir . '/' . self::KEY_FILE; 
	}

	// directory methods
	public static function databaseDirectory() { return self::DB_DIRECTORY; }
	public static function dataDirectory() { return self::DATA_DIRECTORY; }
	public static function logDirectory() { return self::LOG_DIRECTORY; }
	public static function privDirectory() { 
		$home = getenv('HOME');
		if (! $home) { $home = '.'; }
		return $home . '/' . self::PRIV_DIRECTORY; 
	}  
	
	public static function utilitiesDbDirectory() { return self::UT_DB_DIRECTORY; }
	public static function utilitiesDataDirectory() { return self::UT_DATA_DIRECTORY; }

	// export some values
	public static function get($val) {
		switch($val) {
			default:
				return null;
				break;
		}
	}				
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>
