#!/bin/bash
# 
# set up AWS credentials for s3setup.php
#
COMMANDS=./commands

PHP="$(which php)"

BUCKET=$($PHP $COMMANDS/read_config.php S3_BUCKET)
if [ -z $BUCKET ]
then
	echo "Please create your Amazon bucket using the AWS Console;"
	echo "Configure the bucket identifier in AnyNewsConfig::S3_BUCKET before proceeding."
	exit
fi

$PHP $COMMANDS/s3setup.php $1 $2 $3 $BUCKET