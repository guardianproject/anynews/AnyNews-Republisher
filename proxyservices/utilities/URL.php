<?php
/**
 * ----------------------------------------------------------------------
 * component: URL
 * A crisply-defined subset of CURL function necessary for other objects
 * in this library.
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\proxyservices\utilities;
 
class URL {
    private static $defaultReqHeaders = array();

	// settings
	private $access_timeout = 10;

	// data retrieved
	private $http_code;
	private $content_type;
	private $header_array;
	private $req_headers;
	private $curl_info;
	
	private $use_credentials = false;
	private $credentials;

	private $url;
	private $mt;
	
	private $last_request_timed_out;

    public static function setDefaultReqHeaders($headersArray) {
        self::$defaultReqHeaders = $headersArray;
    }

	public function __construct($url = null) {
		$this->req_headers = self::$defaultReqHeaders;
		$this->mt = new MIMEType(true);
		if ($url) {
			$this->setUrl($url);
		}
		$this->last_request_timed_out = false;
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// PUBLIC FUNCTIONS
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ------------------------------------------------------------------
	// setUrl
	// ------------------------------------------------------------------	

	public function setUrl($url) {
		$this->url = $url;
		
		// reset state
		$this->header_array = $this->curl_info = array();
	}

	// ------------------------------------------------------------------
	// setRequestHeader
	// ------------------------------------------------------------------	

	public function setRequestHeader($name, $content) {
        $header = $name . ': ' . $content;
        $updated = false;

        foreach ($this->req_headers as $key => $value) {
            if (strpos($value, $name . ':') === 0) {
                $this->req_headers[$key] = $header;
                $updated = true;
                break;
            }
        }

        if (!$updated) {
            $this->req_headers[] = $header;
        }
	}

	// ------------------------------------------------------------------
	// setUserAgent
	// ------------------------------------------------------------------	

	public function setUserAgent($agent) {
		$this->setRequestHeader('User-Agent', $agent);
	}
	// ------------------------------------------------------------------
	// setCredentials
	// ------------------------------------------------------------------	

	public function setCredentials($user, $pass) {
		$this->use_credentials = true;
		$this->credentials = $user . ':' . $pass;
	}
		
	// ------------------------------------------------------------------
	// setTimeout - set CURL's access timeout
	// ------------------------------------------------------------------	

	public function setTimeout($secs) {
		$this->access_timeout = $secs;
	}
	
	// ------------------------------------------------------------------
	// ping - URL lookup/validation (for metadata)
	// ------------------------------------------------------------------	
	
	public function effectiveUrl($url) {
		$correct_url = $url;
		do {
			$res = $this->ping($correct_url, false, false);
			if ($this->is('redirection')) {
				$correct_url = $res['redirect_url'];
			}
			sleep(1);
		} while ($res['http_code'] != '200');
		
		return $correct_url;
	}
	
	public function ping($url, $follow = false, $with_hdrs = false) {	
		$res = array();
		if (! $url) { 
			if (! $this->url) {
				$res['http_code'] = '400';  // client error
				return $res;
			}
			$url = $this->url;
		}
		
		$curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $url);    
   		curl_setopt($curl, CURLOPT_HEADER, true);
   		curl_setopt($curl, CURLOPT_TIMEOUT, $this->access_timeout);	    
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
    	// together, these implement the HTTP HEAD method
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_NOBODY, true);
		
		if ($this->use_credentials) {
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
			curl_setopt($curl, CURLOPT_USERPWD, $this->credentials); 
		}
		if (count($this->req_headers) > 0) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, $this->req_headers);
		} else {
			// in any case we want to send the user agent
			curl_setopt($curl, CURLOPT_HTTPHEADER, [$this->req_headers["User-Agent"]]);
		}

		 
        $response = @curl_exec($curl);
        $info = curl_getinfo($curl);
        $this->curl_info = $info;
        $this->http_code = $info['http_code'];
        $res['http_code'] = $this->http_code;
        $res['url'] = $url;

		$res['total_time'] =  $info['total_time']; // check for time-out
		if (($info['total_time'] - $info['namelookup_time']) >= $this->access_timeout) { 
			$this->last_request_timed_out = true;
			$res['request_timeout'] = 1;
        } else { 
        	$this->last_request_timed_out = false;
        	$res['request_timeout'] = 0;
        }
               
        // request crapped out
        if ($this->is('dead')) {
        	return $res;
        } 

        // save headers
		$headers = trim(substr($response, 0, $info['header_size']));  // as string			
		$this->header_array = $this->parse_headers($headers);
		if ($with_hdrs) {	
			$res['headers'] = $this->header_array;
        }

        // figure out redirection if any
        if ((!$follow) && $this->is('redirection')) {
        	$res['redirect_url'] = $info['redirect_url'];
        	return $res;
        } else if ($follow) {
        	$eu = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        	if ($url != $eu) {
        		$res['effective_url'] = $eu;
        	}
		}
        curl_close($curl);
        
        // we have enough helpful information now to die if we need to
        if (! $this->is('success')) {
        	return $res;
        }
        
        // check for header content type mismatch
    	list ($ict, $other) = explode('; ', $info['content_type']);
        if (($cts = $this->header_array['Content-Type']) != null) {
	        list ($hct, $other) = explode('; ', $cts);
        	if ($hct != $ict) {
        		Utilities::logger('URL: ' . $url . ' header Content-Type [' . $hct .
        			'] different from CURL info [' . $ict . ']', E_USER_NOTICE);
        	}
        }
        $res['content_type'] = $ict;
   		$res['content_length'] = $info['download_content_length'];

        return $res;
	}

	// ------------------------------------------------------------------
	// acquire - GET a URL
	// ------------------------------------------------------------------	
	
	public function get($url, $follow = false, $with_hdrs = false, $destination = null) {
		return $this->acquire($url, $follow, $with_hdrs, $destination);
	}
	public function acquire($url, $follow = false, $with_hdrs = false, $destination = null) {
		$res = array();
		if (! $url) { 
			if (! $this->url) {
				Utilities::logger('URL not specified', E_ERROR);
				$res['http_code'] = '400';  // client error
				return $res;
			}
			$url = $this->url;
		}

		$curl = curl_init();	
		
	    curl_setopt($curl, CURLOPT_URL, $url);  
	    curl_setopt($curl, CURLOPT_TIMEOUT, $this->access_timeout);
   		curl_setopt($curl, CURLOPT_HEADER, $with_hdrs);	    
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    if ($destination) {
	    	$fp = fopen($destination, "w");
	    	if (! $fp) {
	    		Utilities::logger('download destination file [' . $destination . 
	    			'] could not be opened for writing ', E_ERROR);
	    		$res['http_code'] = '400';  // client error
	    		return $res;
	    	}
	    	curl_setopt($curl, CURLOPT_FILE, $fp);
	    }
		if ($this->use_credentials) { 
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
			curl_setopt($curl, CURLOPT_USERPWD, $this->credentials);  
		}
		if (count($this->req_headers) > 0) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, $this->req_headers);
		}
					
        $response = @curl_exec($curl);
        $info = curl_getinfo($curl);
        $this->curl_info = $info;
        $this->http_code = $info['http_code'];
        $res['http_code'] = $this->http_code;
        $res['url'] = $url;

		$res['total_time'] =  $info['total_time']; // check for time-out
		if (($res['total_time'] - $info['namelookup_time']) >= $this->access_timeout) { 
			$this->last_request_timed_out = true;
			$res['request_timeout'] = 1;
        } else { 
        	$this->last_request_timed_out = false;
        	$res['request_timeout'] = 0;
        }
        
        if ($destination) { fclose($fp); }
        
        // request crapped out
        if ($this->is('dead')) {
        	return $res;
        } 

        // save headers
        if ($with_hdrs) {
			$headers = trim(substr($response, 0, $info['header_size']));  // as string			
			$this->header_array = $this->parse_headers($headers);
			//print 'Header Content-type: ' . $this->header_array['Content-Type'] . "\n";
			if ($with_hdrs) {	
				$res['headers'] = $this->header_array;
        	}
        	$body = trim(substr($response, $info['header_size']));
        	$res['body'] = $body;
        } else {
        	$res['body'] = $response;
        }
        
        // figure out redirection if any
        if ((!$follow) && $this->is('redirection')) {
        	$res['redirect_url'] = $this->header_array['Location'];
        	return $res;
        } else if ($follow) {
        	$eu = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        	if ($url != $eu) {
        		$res['effective_url'] = $eu;
        	}
		}
        curl_close($curl);
        
        // we have enough helpful information now to die if we need to
        if (! $this->is('success')) {
        	return $res;
        }
        
        // check for header content type mismatch
        list ($ict, $other) = explode('; ', $info['content_type']);
/**
        $cts = $this->header_array['Content-Type'];
        if ($cts) {
	        list ($hct, $other) = explode('; ', $cts);
        	if (($hct != null) && ($hct != $ict)) {
        		Utilities::logger('URL: ' . $url . ' header Content-Type [' . $hct .
        			'] different from CURL info [' . $ict . ']', E_USER_NOTICE);
        	}
        }
**/
        $res['content_type'] = $ict;
        $res['content_length'] = $info['download_content_length'];
        
		return $res;
	}
	
	// ------------------------------------------------------------------
	// post - POST information to a URL
	// ------------------------------------------------------------------	
	
	public function post($url, $params, $follow = false) {
		$res = array();
		if (! $url) { 
			if (! $this->url) {
				$res['http_code'] = '400';  // client error
				return $res;
			}
			$url = $this->url;
		}
		
		$curl = curl_init();	
		
   		curl_setopt($curl, CURLOPT_TIMEOUT, $this->access_timeout);
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
    	curl_setopt($curl, CURLOPT_HEADER, 1);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    //curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_POST, 1);
				
		if (is_array($params)) { curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params)); }
		else { curl_setopt($curl, CURLOPT_POSTFIELDS, $params); }

		if ($this->use_credentials) {
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
			curl_setopt($curl, CURLOPT_USERPWD, $this->credentials); 
		}

		if (count($this->req_headers) > 0) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, $this->req_headers);
		}		
			
        $response = @curl_exec($curl);
        $info = curl_getinfo($curl);
        $this->curl_info = $info;
        $this->http_code = $info['http_code'];
        $res['http_code'] = $this->http_code;
        $res['url'] = $url;
       
		$res['total_time'] =  $info['total_time']; // check for time-out
		if (($res['total_time'] - $res['namelookup_time']) >= $this->access_timeout) { 
			$this->$last_request_timed_out = true;
			$res['request_timeout'] = 1;
        } else { 
        	$this->$last_request_timed_out = false;
        	$res['request_timeout'] = 0;
        }
    	
    	// request crapped out
        if ($this->is('dead')) {
        	return $res;
        } 
        
        // save headers
		$headers = trim(substr($response, 0, $info['header_size']));  // as string			
		$this->header_array = $this->parse_headers($headers);
		if ($with_hdrs) {	
			$res['headers'] = $this->header_array;
        }
        $body = trim(substr($response, $info['header_size']));
        $res['body'] = $body;
                
        // figure out redirection if any
        if ((!$follow) && $this->is('redirection')) {
        	$res['redirect_url'] = $this->header_array['Location'];
        	return $res;
        } else if ($follow) {
        	$eu = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        	if ($url != $eu) {
        		$res['effective_url'] = $eu;
        	}
		}
        curl_close($curl);
        
        // we have enough helpful information now to die if we need to
        if (! $this->is('success')) {
        	return $res;
        }
        
        // check for header content type mismatch
        $cts = $this->header_array['Content-Type'];
        list ($hct, $other) = explode('; ', $cts);
        list ($ict, $other) = explode('; ', $info['content_type']);
        if ($hct != $ict) {
        	Utilities::logger('URL: ' . $url . ' header Content-Type [' . $hct .
        		'] different from CURL info [' . $ict . ']', E_USER_NOTICE);
        }
        $res['content_type'] = $ict;
        $res['content_length'] = $info['download_content_length'];
        
		return $res;
	}
	
	// ------------------------------------------------------------------
	// put - PUT information to a URL
	// SLIGHTLY ODD: this accepts data in the $params argument, writes it
	// to file, then uploads that file.
	// ------------------------------------------------------------------	
	
	public function put($url, $params, $follow = false) {
		$res = array();
		if (! $url) { 
			if (! $this->url) {
				$res['http_code'] = '400';  // client error
				return $res;
			}
			$url = $this->url;
		}
		
		$curl = curl_init();	
		
   		curl_setopt($curl, CURLOPT_TIMEOUT, $this->access_timeout);
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
    	curl_setopt($curl, CURLOPT_HEADER, 1);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    //curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_PUT, 1);		
		
		// PUT verb (in PHP) requires uploading from a FILE, use a temp one
		$filepath = tempnam(sys_get_temp_dir(), 'curl_put_' . substr(sha1(time()), 0, 4));		
		if (is_array($params)) { 
			$formatted = http_build_query($params);
		} else {
			$formatted = $params;
		}
		file_put_contents($filepath, $formatted);
		$fp = fopen($filepath, 'r');
		curl_setopt($curl, CURLOPT_INFILE, $fp);
        curl_setopt($curl, CURLOPT_INFILESIZE, strlen($formatted));

		if ($this->use_credentials) {
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); 
			curl_setopt($curl, CURLOPT_USERPWD, $this->credentials); 
		}

		if (count($this->req_headers) > 0) {
			curl_setopt($curl, CURLOPT_HTTPHEADER, $this->req_headers);
		}		
			
        $response = @curl_exec($curl);
        
        // close remove our temporary file
        fclose($fp);
        unlink($filepath);

        $info = curl_getinfo($curl);
        $this->curl_info = $info;
        $this->http_code = $info['http_code'];
        $res['http_code'] = $this->http_code;
        $res['url'] = $url;
       
		$res['total_time'] =  $info['total_time']; // check for time-out
		if (($res['total_time'] - $res['namelookup_time']) >= $this->access_timeout) { 
			$this->$last_request_timed_out = true;
			$res['request_timeout'] = 1;
        } else { 
        	$this->$last_request_timed_out = false;
        	$res['request_timeout'] = 0;
        }
        
        // request crapped out
        if ($this->is('dead')) {
        	return $res;
        } 
        
        // save headers
		$headers = trim(substr($response, 0, $info['header_size']));  // as string			
		$this->header_array = $this->parse_headers($headers);
		if ($with_hdrs) {	
			$res['headers'] = $this->header_array;
        }
        $body = trim(substr($response, $info['header_size']));
        $res['body'] = $body;
                
        // figure out redirection if any
        if ((!$follow) && $this->is('redirection')) {
        	$res['redirect_url'] = $this->header_array['Location'];
        	return $res;
        } else if ($follow) {
        	$eu = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        	if ($url != $eu) {
        		$res['effective_url'] = $eu;
        	}
		}
        curl_close($curl);
        
        // we have enough helpful information now to die if we need to
        if (! $this->is('success')) {
        	return $res;
        }
        
        // check for header content type mismatch
        $cts = $this->header_array['Content-Type'];
        list ($hct, $other) = explode('; ', $cts);
        list ($ict, $other) = explode('; ', $info['content_type']);
        if ($hct != $ict) {
        	Utilities::logger('URL: ' . $url . ' header Content-Type [' . $hct .
        		'] different from CURL info [' . $ict . ']', E_USER_NOTICE);
        }
        $res['content_type'] = $ict;
        $res['content_length'] = $info['download_content_length'];
        
		return $res;
	}

	// ------------------------------------------------------------------
	// string matches URL pattern
	// ------------------------------------------------------------------
			
	public function isLikelyAUrl($string) {
		// from https://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www
		$regex = "((https?|ftp)://)?"; // SCHEME
    	$regex .= "([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?"; // User and Pass
    	$regex .= "([a-z0-9\-\.]*)\.(([a-z]{2,4})|([0-9]{1,3}\.([0-9]{1,3})\.([0-9]{1,3})))"; // Host or IP
    	$regex .= "(:[0-9]{2,5})?"; // Port
    	$regex .= "(/([a-z0-9+$_%-]\.?)+)*/?"; // Path
    	$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+/$_.-]*)?"; // GET Query
    	$regex .= "(#[a-z_.-][a-z0-9+$%_.-]*)?"; // Anchor
	
		if (preg_match("~^$regex$~i", $string, $m) != 1) { return false; }
		return true;
	}
	
	// ------------------------------------------------------------------
	// did last request fail?
	// ------------------------------------------------------------------
			
	public function lastRequestTimedOut() {
		return $this->last_request_timed_out;
	}
	
	// ------------------------------------------------------------------
	// ------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ------------------------------------------------------------------
	// ------------------------------------------------------------------
				
	// ------------------------------------------------------------------
	// parse_headers - HTTP header string to array
	// ------------------------------------------------------------------	

	private function parse_headers($hdr_string) {
		if (! $hdr_string) { return array(); }
		$hdr_array = array();
		$lines = explode("\n", $hdr_string);
		foreach ($lines as $line) { 
			$line = trim($line);
			if ($line != "") {
				if (strstr($line, ':')) { 
					$name = trim(strstr($line, ':', true));
					$value = trim(substr(strstr($line, ':', false), 1));
					$hdr_array[$name] = $value;
				}
			} 
		}
			
		return $hdr_array;
	}

	// ------------------------------------------------------------------
	// is - state of HTTP event
	// ------------------------------------------------------------------	
	
	private function is($type) {
		switch ($this->http_code) {
			case 0:
				$state = 'dead';
				break;
			case 100:
			case 101:
			case 103:
				$state = 'informational';
				break;
			case 200:
			case 201:
			case 202:
			case 203:
			case 204:
			case 205:
			case 206:
				$state = 'success';
				break;
			case 300:
			case 301:
			case 302:
			case 303:
			case 304:
			case 306:
			case 307:
			case 308:			
				$state = 'redirection';
				break;
			case 400:
			case 401:
			case 402:
			case 403:
			case 404:
			case 405:
			case 406:
			case 407:
			case 408:
			case 409:
			case 410:
			case 411:
			case 412:
			case 413:
			case 414:
			case 415:
			case 416:
			case 417:
				$state = 'client error';
				break;
			case 500:
			case 501:
			case 502:
			case 503:
			case 504:
			case 505:
			case 511:
				$state = 'server error';
				break;
			default:
				$state = 'unofficial';	// any code not among the official standard
		}

		return ($state == $type); // boolean response
	}	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
