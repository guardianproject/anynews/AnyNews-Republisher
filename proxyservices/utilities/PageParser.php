<?php
/**
 * ----------------------------------------------------------------------
 * component: PageParser
 * Acquires meta-information from HTML pages.  This class is useful 
 * principally to locate a site's auxiliary information (feeds, site icon,
 * keyword, etc). Secondarily, it validates that a URL exists (rather than
 * not, or being redirected).
 *
 * 3Dec17 - new (re-combines functions from other older objects)
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\proxyservices\utilities;

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\MIMEType;
use guardianproject\proxyservices\utilities\SiteIcon;
use guardianproject\proxyservices\utilities\Utilities;

use \DOMDocument;

Class PageParser {
	private $url;
	
	private $uh;
	private $mt;
	private $si;

	// our content
	private $header_array  = array();
	private $request_error = false;
	private $html_head;
	private $html_body;
	private $complete_body;
		
	// information we discover from the document
	private $content_type;
	private $rel_favicon;
	private $favicon_bits;
	private $force_icon_search = true;
	
	private $rel_canonical;
	private $site_root;
	private $is_root = false;
	private $redirect_url;
	private $redirected = false;

	private $site_title;
	private $site_description;
	private $site_keywords = array();
	private $links = array();
	private $metas = array();
	private $num_metas = 0;
	private $feeds = array();
	private $num_feeds = 0;
	private $current_feed = 0;
	
	public function __construct() {
		$this->uh = new URL();
		$this->uh->setUserAgent('EthicalPageParser/1.0');
		$this->mt = new MIMEType();
		$this->si = new SiteIcon();
		
		// initialize internal state
		
		$this->request_error = $this->is_root = $this->redirected = false;
		$this->content_type = $this->rel_favicon = $this->rel_canonical = null;
		$this->site_title = $this->site_root = $this->redirect_url = $this->redirect_url = null;
		$this->html_head = $this->html_body = $this->complete_body = null;
		$this->links = array();
		$this->metas = array();
		$this->feeds = array();
		$this->site_description = null;
		$this->site_keywords = array();
		 
		$this->num_feeds = $this->current_feed = 0;
		$this->num_metas = $this->current_meta = 0;
		$this->header_array = array();		
	}
		
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// PUBLIC FUNCTIONS
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// setUrl - set the URL to acquire or ping, secondarily reset internal state 
	// ----------------------------------------------------------------------
	
	public function setUrl($url) {
		$this->url = $url;
		
		// clear internal state for "re-use"
		
		$this->request_error = $this->is_root = $this->redirected = false;
		$this->content_type = $this->rel_favicon = $this->rel_canonical = null;
		$this->site_title = $this->site_root = $this->redirect_url = $this->redirect_url = null;
		$this->html_head = $this->html_body = $this->complete_body = null;
		$this->links = array();
		$this->metas = array();
		$this->feeds = array();
		$this->site_description = null;
		$this->site_keywords = array();
		 
		$this->num_feeds = $this->current_feed = 0;
		$this->header_array = array();				
	}
	
	// ----------------------------------------------------------------------
	// acquire - 
	// ----------------------------------------------------------------------
	
	public function acquire($url = null) {
		if (! $url) { $url = $this->url; }
		$response = $this->uh->acquire($url, true, true);
		if ($response['http_code'] != 200) {
			$this->request_error = true;
			return $response;
		}
		// capture data we can gleen from headers
		$this->request_error = false;
		list($this->content_type, $charset) = explode(';', $response['content_type']);
		$this->header_array = $response['headers'];
		
		$ht = $this->mt->getTypeForFileExtension('html');
		if ($this->content_type != $ht) {
			Utilities::logger('content type of response [' .$this->content_type . '] is not [' . $ht . ']', E_ERROR);
			$this->request_error = true;
			return $response;
		} 
		
		if (array_key_exists('redirect_url', $response)) {
			$this->redirected = true;
			$this->redirect_url = $response['redirect_url'];
			return $response;
		} else if (array_key_exists('effective_url', $response)) {
			$this->redirected = true;
			$this->redirect_url = $response['effective_url'];
		}

		// parse the content
		$this->complete_body = $response['body'];
		if (! $this->parse_content()) {
			return false;
		}
		
		return $response;
	}

	// ----------------------------------------------------------------------
	// feed handlers	
	// ----------------------------------------------------------------------
	
	public function hasFeeds() { 
		return ($this->num_feeds > 0);
	}
	
	public function getNextFeed() { 
		if ($this->current_feed < $this->num_feeds) {
			$f = $this->feeds[$this->current_feed];
			$this->current_feed += 1;
			return $f;
		} else {
			$this->current_feed = 0;
			return null;
		}
	}

	// ----------------------------------------------------------------------
	// metadata handlers	
	// ----------------------------------------------------------------------
	
	public function hasMetas() { 
		return ($this->num_metas > 0);
	}

	// ----------------------------------------------------------------------	
	// externalize our data
	// ----------------------------------------------------------------------
	
	public function asJson() { return $this->externalize(); }

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
				
	// ------------------------------------------------------------------
	// parse_content - parse and store a page's meta-contents
	// ------------------------------------------------------------------	
	
	private function parse_content() {
		$page = $this->complete_body;
		
		// get the root and determine if the URL supplied is the site's root
		// (we'll get the canonical URL later)
		$parts = parse_url($this->url);
		if ($parts) {
			unset($parts['path']);
			unset($parts['query']);
			unset($parts['fragment']);
			$this->site_root = Utilities::recompose_url($parts);
			if ($this->url == $this->site_root) { $this->is_root = true; }
			else { $this->is_root = false; }
		} else {
			$this->site_root = 'https://' . $this->url . '/';
		}
		
		// parse up the links in the document <head>
		$links = $this->parse_links($page);
		$this->links = $links;
		$this->mine_links($links);

		// verify favicon
		if ($this->rel_favicon) {
			// check for a relative path returned
			if (substr($this->rel_favicon, 0, 3) != 'htt') {
				//print "relative URL in page: " . $i_url . "\n";
				if (substr($this->rel_favicon, 0, 2) == '//') {	// odd missing-scheme format
					$parts = parse_url($this->rel_favicon);
					$this->rel_favicon = trim($this->site_root, '/') . trim($parts['path'], '/'); 
				} else { // normal relative path
					$this->rel_favicon = trim($this->site_root, '/') . '/' . trim($this->rel_favicon, '/');
				}
			}
		}
		
		if ($this->rel_canonical) { $this->site_root = $this->rel_canonical; }
		
		// parse up the metas in the document <head>
		$metas = $this->parse_metas($page);
		$this->num_metas = count($metas);
		$this->metas = $metas;
		$this->mine_metas($metas);
		
		// get site title
		$this->site_title = $this->parse_title($page);

		// force a search for icon, if required
		if ($this->force_icon_search && ($this->rel_favicon == null)) {
			$this->rel_favicon = $this->si->findIconUrl($this->site_root);
		}
		if ($this->force_icon_search) {
			if ($this->rel_favicon) {
				$this->favicon_bits = $this->si->acquireIcon($this->rel_favicon, $this->site_root);
			} else {
				$this->favicon_bits = $this->si->acquireIcon($this->site_root);
			}
		}
		
		return true;
	}

	// ----------------------------------------------------------------------
	// parse_links - find the <link> components
	// ----------------------------------------------------------------------

	private function parse_links($html_page) {
		return $this->parse_items($html_page, 'link');
	}

	// ----------------------------------------------------------------------
	// mine_links - find data we need in the <link> components
	// ----------------------------------------------------------------------

	private function mine_links() {
		$this->num_feeds = 0;
		$got_icon = false;
		foreach ($this->links as $data) { 
			switch ($data['rel']) {
				// <link rel="canonical" href="http://www.nytimes.com" />
				case 'canonical':
					$this->rel_canonical = $data['href'];
					break;
				
				// <link rel="alternate" type="application/rss+xml" title="RSS" href="http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml" />	
				case 'alternate':
					// get this alternate's type value 
					$val = $data['type'];
					$href = $data['href'];
					if (strstr($val, '+xml')) {
						// look at HREF, since we don't want blog comments,tags,categories
						if (strstr($href, '/comments') || 
							strstr($href, '=comments') ||
							strstr($href, 'cat=') ||
							strstr($href, '/category') ||
							strstr($href, 'tag=') ||
							strstr($href, '/tag')) {
							break;
						} else {
							//print "feed:  " . $href . "\n";
							// make sure we provide a full URL in the href
							$data['href'] = $this->inflate($href);
							//print "feed full URL: " . $data['href'] . "\n";
							$this->feeds[] = $data;
							$this->num_feeds++;
						}
					}
					break;
				case 'shortcut icon': // prefer this one
					$this->rel_favicon = $data['href'];
					$got_icon = true;
					break;
				case 'icon':	
				case 'apple-touch-icon':
				case 'apple-touch-icon-precomposed';
					if (!$got_icon) {
						$this->rel_favicon = $data['href'];
						$got_icon = true;
					}
					break;
				default:		// this link is dead to me
					break;
			}
		}
		
		return true;
	}

	// ----------------------------------------------------------------------
	// parse_metas - find the <meta> components
	// ----------------------------------------------------------------------

	private function parse_metas($html_page) {
		$dom = new DOMDocument;
		@$dom->loadHTML($html_page);

		$item_data = array();

		$items = $dom->getElementsByTagName('meta');
		foreach ($items as $item) {
    		foreach ($item->attributes as $attribute) {
    			if ($attribute->nodeName == 'property') { $pname =  $attribute->nodeValue; }
    			if ($attribute->nodeName == 'content') { $pvalue = $attribute->nodeValue; }
			}
			
			if (($pname != null) && ($pvalue != null)) {
				$item_data[$pname] = $pvalue;
				$this->num_metas++;
			}
		}
		
		return $item_data;
	}

	// ----------------------------------------------------------------------
	// mine_metas - find data we need in the <meta> components
	// ----------------------------------------------------------------------

	private function mine_metas() {
		foreach ($this->metas as $name => $value) { 
			if ($name == 'description') {
				$this->site_description = str_replace(array("\r", "\n"), "", $value);
			}
			if ($name == 'keywords') {
				if (strstr($value, ',')) { $sep = ','; }
				else { $sep = ' '; }
				
				$this->site_keywords = $this->parse_keywords($sep, $value);
			}
			// Facebook "Open Graph" & Twitter proprietary fields sometimes useful...
			switch ($name) {
				case 'og:title':
				case 'twitter:title':
					if (! $this->site_title) { $this->site_title = $value; }
					break;
				case 'og:url':
					if (! $this->site_root) { $this->site_root = $value; }
					break;
				case 'og:description':
				case 'twitter:description':				
					if (! $this->site_description) { 
						$this->site_description = str_replace(array("\r", "\n"), "", $value);
					}
					break;
				case 'og:image':
				case 'twitter:image':
					if ((! $this->rel_favicon) && (substr($value, 0, 4) == 'http')) { 
						$this->rel_favicon = $value;
					}
					break;
				default:
					break;
			} 
		}
		
		return true;
	}

	// ----------------------------------------------------------------------
	// retrieve - get HTML content
	// ----------------------------------------------------------------------

	private function retrieve($url) {
		$response = $this->url->acquire($url, true, true);
		return $response;
	}

	// ----------------------------------------------------------------------
	// parse_keywords - retrieve site keywords
	// ----------------------------------------------------------------------
	
	private function parse_keywords($sep, $keyword_string) {
		$in = explode($sep, $keyword_string);
		$keywords = array();
		foreach ($in as $word) {
			$keywords[] = trim($word);
		}
		
		return $keywords; 
	}
	
	// ----------------------------------------------------------------------
	// parse_items - find and parse header items in the supplied HTML text
	// ----------------------------------------------------------------------
	
	private function parse_items($html_page, $type) {
		$dom = new DOMDocument;
		@$dom->loadHTML($html_page);

		$item_data = array();
		
		$items = $dom->getElementsByTagName($type);
		foreach ($items as $item) {
    		$an_item = array();
    		foreach ($item->attributes as $attribute) {
				$name = $item->attributes->item($i)->name;
				$value = $item->attributes->item($i);
    			$an_item[$attribute->nodeName] = $attribute->nodeValue;
			}
			$item_data[] = $an_item;
		}
		return $item_data;
	}

	// ------------------------------------------------------------------
	// parse out the site's title element
	// ------------------------------------------------------------------		
	
	private function parse_title($html_page) {
		$dom = new DOMDocument;
		@$dom->loadHTML($html_page);
		
		$title = $dom->getElementsByTagName('title')->item(0)->nodeValue;
		return $title;		
	}
	
	// ------------------------------------------------------------------
	// inflate - make sure href is FULL (not path-only)
	// ------------------------------------------------------------------	

	private function inflate($href) {
		$p = parse_url($href);
		if (array_key_exists('scheme', $p)) { return $href; }

		// apparently, I've got only the path part, so inflate it best we can
		
		$path = $href;
		if ('/' == substr($path, 0, 1)) {
			$path = substr($path, 1);
		}
		if ($this->site_root) { $s = parse_url($this->site_root); } 
		else { $s = parse_url($this->url); }
		
		$full = $s['scheme'] . '://' . $s['host'];
		if ($s['port']) {
			$full .=  ':' . $s['port'];
		}
		$full .= '/' . $path;
		return $full;
	}

	// ----------------------------------------------------------------------
	// create a JSON representation of what we've found
	// ----------------------------------------------------------------------	
	
	private function externalize() {
		$data = array();
		
		$data['provided_url'] = $this->url;

		$data['content_type'] = $this->content_type;
		if ($this->rel_canonical) {
			$data['canonical_link'] =  $this->rel_canonical;
		} 
		if ($this->redirected) {
			$data['redirect_url']   =  $this->redirect_url;
		}
		
		$data['title'] = $this->site_title;
		$data['description'] = $this->site_description;
		if ($this->site_keywords != null) {
			$k = array();
			foreach($this->site_keywords as $key) {
				$k[] = $key;
			}
			$data['keywords'] = $k;
		}
		$data['icon'] = $this->rel_favicon;
		$data['site_root'] = $this->site_root;
			
		if ($this->hasFeeds()) {
			$feeds = array();
			while ($next = $this->getNextFeed()) {
			  	$feeds[] = $next['href'];
			}
			$data['feeds'] = $feeds; 		
		}
		
		if ($this->hasMetas()) {
			$data['metas'] = $this->metas;
		}
		
		if ($this->favicon_bits != null) {
			$data['icon_bits'] = 'icon collected';
		}

		return json_encode($data);
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}

?>
