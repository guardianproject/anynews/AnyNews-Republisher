<?php
/**
 * ----------------------------------------------------------------------
 * component: HttpResponse
 * strings and explanations for the HTTP response codes. See
 * more information:
 *    http://www.w3schools.com/tags/ref_httpmessages.asp
 *
 * 20Nov17 - updated dump() method to write file in system temporary storage  
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\proxyservices\utilities;
use \PDO as PDO;
use \PDOException as PDOException;
use guardianproject\proxyservices\utilities\Utilities;
use UtilitiesConfig as Config;

class HttpResponse extends DataStore {

	public function __construct($force = false) {
		$this->builds_from_source = false;
		
		$this->info_database = 'auxiliary_info.db';
		$this->static_data   = 'http_codes.csv';
		$this->table_name    = 'http';

		$this->db_loc   = Config::utilitiesDbDirectory() . '/' . $this->info_database;
		$this->data_loc = Config::utilitiesDataDirectory() . '/' . $this->static_data;

		if (! $this->initialize($force)) {
			throw new Exception('Object creation failed. Database ' . $this->info_database . ' corrupt or incomplete');
		}
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
			
	// ----------------------------------------------------------------------
	// codenameExists
	// ----------------------------------------------------------------------
	
	public function httpCodeExists($code) {
		if (! $code) return false; 
		if ($this->retrieveDescriptions($code)) { return true; }
		return false;
	}

	// ----------------------------------------------------------------------
	// retrieveLongDescription
	// retrieveShortDescription
	// ----------------------------------------------------------------------
	
	public function retrieveLongDescription($code) {
		$desc = $this->retrieveDescriptions($code);
		if (! $desc) { return null; }
		return $desc['l_desc'];
	}

	public function retrieveShortDescription($code) {
		$desc = $this->retrieveDescriptions($code);
		if (! $desc) { return null; }
		return $desc['s_desc'];
	}
	
	// ----------------------------------------------------------------------
	// retrieveDescriptions
	// ----------------------------------------------------------------------
	
	public function retrieveDescriptions($code) {
		if (! $code) return false;
		
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE http_code=?;';
	    Utilities::logger("PDO query: " . $sql . ' with ' . json_encode(array($code)), E_USER_NOTICE);

	    try {
	    	$q = $this->db->prepare($sql);
	  		$q->execute(array($code));
	   		$res = $q->fetch(PDO::FETCH_ASSOC);
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving http code [' . $code . ']', E_USER_NOTICE);
			return null;
		}
		
		return $res;
	}

	// ----------------------------------------------------------------------
	// urlResponse
	// ----------------------------------------------------------------------
	
	public function urlResponse($url, $follow = true) {
		$info = $this->poke($url, $follow);
		if ($info) {
			$desc = $this->retrieveDescriptions($info['http_code']);
			if ($desc) { 
				return $desc;
			}
		}
		
		return null;
	}

	// ----------------------------------------------------------------------
	// dump
	// ----------------------------------------------------------------------

	public function dump($to_file = null) {
		if ($to_file == null) {
			$ran = substr(sha1(time()), 0, 12);
			$to_file = tempnam(sys_get_temp_dir(), 'http_code_backup_' . $ran);
		}
		$fp = fopen($to_file, "w");
		if (! $fp) {
			Utilities::logger("dump: could not open " . $to_file . " for writing", E_WARNING);
			return false;
		}	

		fputcsv($fp, array('http_code', 's_desc', 'l_desc'));	// write CSV header
		
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		fputcsv($fp, array(
	    			$res['http_code'],
	    			$res['s_desc'],
	    			$res['l_desc']
	    		));
	    	}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error dumping HTTP code data (' . $e->getMessage() . ')', E_ERROR);
			return false;
		}		

		fclose($fp);
		Utilities::logger("contents of " . $this->table_name . " dumped to " . $to_file, E_NOTICE);		
		return true;
	}
		
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ------------------------------------------------------------------
	// poke - URL lookup using CURL
	// ------------------------------------------------------------------	
	
	private function poke($url, $follow = true) {	
		$ua = new UASwapper();
		$curl = curl_init();
		
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_USERAGENT, $ua->next());	    
   		curl_setopt($curl, CURLOPT_HEADER, false);	    
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, false);
		 
        @curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        
        return $info;
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// protected methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	
	// ----------------------------------------------------------------------
	// load_table
	// ----------------------------------------------------------------------

	protected function load_table() {
		$fdev = fopen($this->data_loc, 'r');
		if (! $fdev) {
			Utilities::logger("error opening static http data file " . $this->static_data, E_ERROR);
			return false;
		}
		if (! $this->unload_table()) {
			return false;
		}
		
		$num = 0;		
		$vals = fgetcsv($fdev, 200, ','); // strip header
		while ($vals = fgetcsv($fdev, 200, ',')) {
			$this->load_entry(trim($vals[0]), trim($vals[1]), trim($vals[2]));
			$num++;		
		}
		
		Utilities::logger($num . ' entries added to http database', E_NOTICE);
		
		fclose($fdev);
		return true;
	}
	
	// ----------------------------------------------------------------------
	// load_entry
	// ----------------------------------------------------------------------

	protected function load_entry($code, $short_desc, $long_desc) {
	    $sql = 'INSERT OR REPLACE INTO ' . $this->table_name . ' VALUES (?,?,?);';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    try {
			$q = $this->db->prepare($sql);
			$q->execute(array($code, $short_desc, $long_desc));
		} catch (PDOException $e) {
			Utilities::logger('PDO: load_entry failed to insert values (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// create_table
	// ----------------------------------------------------------------------

	protected function create_table() {
		$sql = 'CREATE TABLE ' . $this->table_name . ' (http_code CHARACTER(3) PRIMARY KEY, s_desc TEXT, l_desc TEXT);';
		Utilities::logger("PDO stmt: " . $sql, E_NOTICE);
		
		try {
			$this->db->exec($sql);
		} catch (PDOException $e) {
	    	Utilities::logger("PDO error creating database table in " . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
	    	return false;
	    }
	    
	    return true;
	}
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>