<?php
/**
 * ----------------------------------------------------------------------
 * component: DataStore 
 * This object encapsulates the routines likely to change if one changes
 * PDO drivers, along with some base access routines useful throughout.
 * 
 * This class can not be instantiated on its own, and subclasses need
 * to implement at least the (abstract) load_table() method.
 *
 * 9Nov17 - add JSON table output
 * 20Nov17 - add abstract dump() method all subclasses must implement 
 * 21Nov17 - improved class initialization to handle both statically- and 
 *           dynamically-loaded class types (so sub-classes no longer need
 *           to override initialize() method
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\proxyservices\utilities;
use \PDO as PDO;
use \PDOException as PDOException;
use guardianproject\proxyservices\utilities\Utilities;

abstract class DataStore {

	// We're using SQLite3 as our database driver. If another is desired,
	// change here and update code with methods for your driver. 
	private $db_driver = 'sqlite';	// actually SQLite3 :-(

	protected $info_database; // these are filled in by the subclass
	protected $static_data;
	protected $table_name;
	protected $db_loc;
	protected $data_loc;
	
	protected $db_user;		// this set is NOT used by SQLite3 driver
	protected $db_pass;
	protected $db_host;
	protected $db_port;
	protected $db_options;
	
	// Certain subclasses have their tables build from "live" Internet
	// sources, necessitating a change in how that subclass is initialized.
	protected $builds_from_source = false;
		
	protected $db;			// used by the subclass (has-a, not is-a!)
	protected $columns;
	
	public function __destruct() {
       return $this->closeDb();
    }

	// ----------------------------------------------------------------------
	// __constructor
	// Each subclass has its unique constructor that sets the items above
	// and calls the initialize() method
	// ----------------------------------------------------------------------

	abstract public function __construct($force = false);
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// setters for start-up items
	// ----------------------------------------------------------------------
	
	public function setDatabase($db) { $this->info_database = $db; }
	public function setDatafile($file) { $this->static_data = $file; }
	public function setTable($table) { $this->table_name = $table; }

	// ----------------------------------------------------------------------
	// setters for PDO DSN (not used with SQLite3 driver)
	// ----------------------------------------------------------------------
	
	public function setDbUser($u) { $this->db_user = $u; }
	public function setDbPassword($p) { $this->db_pass = $p; }
	public function setDbHost($h) { $this->db_host = $h; }
	public function setDbPort($p) { $this->db_port = $p; }
	public function setDbOptions($o) { $this->db_options = $o; }

	// ----------------------------------------------------------------------
	// retrieveInfoLike - retrieve row values that are similar to a provided 
	// string (or the wildcard for "all") in the requested field
	// ----------------------------------------------------------------------
	
	public function retrieveInfoLike($field, $str) {
		if (! $str) { return null; }
		if (! in_array($field, $this->columns)) { return null; }
		
		// SQL escape the string
		if (strstr($str, '\'')) {
			list($first_part, $last_part) = explode ('\'', $str);
			$str = $first_part . '\'\'' . $last_part;
		} else if (strstr($str, '’')) {
			list($first_part, $last_part) = explode ('’', $str);
			$str = $first_part . '\'\'' . $last_part;
		}
		// allow full wildcard to get all values
		if ($str == '*') {
			$like = '%';
		} else {
			$like = '%' . $str . '%';
		}
		
		// this query is VERY finicky.  The field parameter does not want
		// to be quoted, and the like parameter needs to be bound (below)
		
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE ' . $field . ' LIKE :like';
	    Utilities::logger("PDO query: " . $sql . ' with ' . json_encode(array($field, $like)), E_USER_NOTICE);
	     
	    try {
	    	$q = $this->db->prepare($sql);
		  	$q->bindValue(':like', $like, PDO::PARAM_STR);
	    	$q->execute();
	    	$vals = array();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		$next = array();
	    		foreach ($this->columns as $col) {
	    			$next[$col] = $res[$col];
	    		}
	    		$vals[] = $next;
	     	}
	   		return $vals;
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving entries matching field [' . $field . '] and [' . $str . ']', E_USER_NOTICE);
			return null;
		}	    
	}		

	// ----------------------------------------------------------------------
	// retrieveFieldLike - retrieve values from a given field that are
	// similar to a provided string (or the wildcard for "all")
	// ----------------------------------------------------------------------
	
	public function retrieveFieldLike($field, $str) {
		if (! $str) { return null; }
		if (! in_array($field, $this->columns)) { return null; }
		
		// allow full wildcard to get all values
		if ($str == '*') {
			$like = '%';
		} else {
			$like = '%' . $str . '%';
		}
		
		// this query is VERY finicky.  The field parameter does not want
		// to be quoted, and the like parameter needs to be bound (below)
		
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE ' . $field . ' LIKE :like';
	    Utilities::logger("PDO query: " . $sql . ' with ' . json_encode(array($field, $like)), E_USER_NOTICE);
	     
	    try {
	    	$q = $this->db->prepare($sql);
		   	$q->bindValue(':like', $like, PDO::PARAM_STR);
	    	$q->execute();
	    	$vals = array();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		$vals[] = $res[$field];
	     	}
	   		return $vals;
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving entries matching field [' . $field . '] and [' . $str . ']', E_USER_NOTICE);
			return null;
		}	    
	}		
		
	// ----------------------------------------------------------------------
	// closeDb
	// ----------------------------------------------------------------------
	
	public function closeDb() {
		// PDO will force the SQLite3 driver to close the database when handle
		// is removed

		$this->db = null;	
	}	

	// ----------------------------------------------------------------------
	// rebuild - same as reload() for "static" type classes, but overriden
	// by classes that dynamically load content from the network
	// ----------------------------------------------------------------------
	
	public function rebuild() {
		return $this->load_table();
	}
	
	// ----------------------------------------------------------------------
	// reload - rebuild the table from the existing (local) data file
	// ----------------------------------------------------------------------
	
	public function reload() {
		return $this->load_table();
	}

	// ----------------------------------------------------------------------
	// columns - database column names
	// ----------------------------------------------------------------------
	
	public function columns() {
		if (! $this->columns) {
			$this->columns = $this->columnNames();
		}	
		return $this->columns;
	}
	
	// ----------------------------------------------------------------------
	// asJSON - output the table (or selected columns) in JSON format
	// ----------------------------------------------------------------------
 
 	public function asJSON($columns = null) {
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
	    Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    if (! $columns) { $columns = $this->columns(); }
	    if (! $columns) {
	    	Utilities::logger('asJSON: no column definition provided for table ' . $this->table_name, E_ERROR);
	    	return null;
	    }
	    
	    // table properties
	    $json = array(
	    	'table_name' => $this->table_name,
	    	'column_names' => $columns,
	    );
	    
	    // table rows
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	$vals = array();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		$next = array();
	    		foreach ($columns as $col) {
	    			$next[$col] = $res[$col];
	    		}
	    		$vals[] = $next;
	     	}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving table entries matching fields ' . json_encode($columns), E_NOTICE);
			return null;
		}	
		
		$json['table_rows'] = $vals;    

		// return as encoded
 		return json_encode($json);
 	}
 	
	// ----------------------------------------------------------------------
	// dump - write database contents in CSV format to text file in a
	// system-defined temporary storage directory (location written to log
	// if logging enabled);
	// ----------------------------------------------------------------------
	
	abstract public function dump($to_file = null);
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	
	// ----------------------------------------------------------------------
	// columnNames - introspection
	// This is specific to the SQLite3 driver, but easy to implement using
	// MySQL/SQLServer/PostGres - INFORMATION_SCHEMA
	// Oracle -  USER_TAB_COLS
	// ----------------------------------------------------------------------
	
	private function columnNames() {
		$stmt = 'echo \'pragma table_info(' . $this->table_name .');\' | sqlite3 ' . $this->db_loc;
		try {
	    	exec($stmt, $lines);
	    	if (count($lines) > 0) {
	    		$vals = array();
	    		for ($i = 0; $i < count($lines); $i++) {
	    			$dat = explode('|', $lines[$i]);
	    			$vals[] = $dat[1];
	    		}
	    	} else {
	    		return null;
	    	}
	    } catch (Exception $e) {
	    	Utilities::logger("error reading column names is " . $e->getMessage(), E_ERROR);
	    }
	    
	    return $vals;
	}	
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// protected methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
		
	// ----------------------------------------------------------------------
	// load_table - must be over-ridden by subclass
	// ----------------------------------------------------------------------
		
	abstract protected function load_table();

	// ----------------------------------------------------------------------
	// initialize - get an instance ready to use, including set-up of its
	// underlying database table as necessary (or as "forced")
	// ----------------------------------------------------------------------		
	
	protected function initialize($force = false) {
		if (!$this->db_exists() && $force) {
			Utilities::logger('Database does not exist. Creating database file [' .  $this->db_loc . ']', E_WARNING);
			$this->create_db();
		}
		
		// make sure we have a database (and table)
		if (! $this->open_db()) { return false; }
		if (! $this->has_table()) {
			if (! $this->create_table()) { return false; }
			$force = true;
		}
		
		// Build the database table
		if ($force) {	
			if ($this->has_data_file()) {
				// load the table from the local data file
				Utilities::logger("rebuilding " . $this->table_name . " from local file", E_NOTICE);
				if (! $this->load_table()) { return false; } 
			} else {
				// load the table from network source, then
				// dump retrieved contents to local storage
				// (so that, in future, reload method works
				// from local storage)
				if ($this->builds_from_source) {
					Utilities::logger("rebuilding " . $this->table_name . " from source", E_NOTICE);
					if (! $this->rebuild()) { return false; }
					if (! $this->dump($this->data_loc)) { return false; }
				} else {
					Utilities::logger('file ' . $this->data_loc .
						' does not exist; not loading database table', E_ERROR);
					return true;
				}
			}
		}
			
		// get the column names from this database and store them
		// locally for future use
		$this->columns();
		return true;
	}
	
	// ----------------------------------------------------------------------
	// has_data_file
	// ----------------------------------------------------------------------
		
	protected function has_data_file() {
		if (file_exists($this->data_loc)) { return true; }
		return false;
	}
		
	// ----------------------------------------------------------------------
	// has_table
	// ----------------------------------------------------------------------
		
	protected function has_table() {
		$sql = 'SELECT name FROM sqlite_master WHERE type=\'table\' AND name=\'' . $this->table_name . '\';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
		
		try {
			$q = $this->db->query($sql);
			if (! $q || ($q->fetch(\PDO::FETCH_ASSOC) == false)) {
				Utilities::logger("database " . $this->db_loc . " lacks table " . $this->table_name, E_WARNING);
				return false;
			}
		} catch (PDOException $e) { 
			Utilities::logger("PDO error testing for " . $this->table_name . " in " . $this->db_loc . ' is ' . $e->getMessage() . ')', E_ERROR);
			return false;
		}
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// unload_table
	// ----------------------------------------------------------------------
	
	protected function unload_table() {
		$sql = 'DELETE FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);

	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    } catch (PDOException $e) {
			Utilities::logger('PDO error deleting all entries from ' . $this->table_name . ' (' . $e->getMessage() . ')', E_ERROR);
			return false;
		}
		
		Utilities::logger('all entries removed from ' . $this->table_name, E_NOTICE);
		return true;
	}	

	// ----------------------------------------------------------------------
	// db_exists
	// ----------------------------------------------------------------------

	protected function db_exists() {
		// with SQLite3, only need to test for the existence of the DB file
		if (file_exists($this->db_loc)) { return true; }
		Utilities::logger("database file [" . $this->db_loc . "] not present\n", E_NOTICE);
		return false;
	}

	// ----------------------------------------------------------------------
	// create_db
	// ----------------------------------------------------------------------

	protected function create_db() {
		Utilities::logger('PDO: creating database ' . $this->db_loc, E_USER_NOTICE);
		try {
			// with SQLite3, merely constructing the object will 
			// create the new database for us (empty of course)
			
			$this->db = new \PDO($this->db_driver . ':' . $this->db_loc);
			$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			Utilities::logger('PDO error creating database ' . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
			return false;
		}
		
		return true;
	}	

	// ----------------------------------------------------------------------
	// open_db
	// ----------------------------------------------------------------------

	protected function open_db() {
		// with SQLite3 this will create the database (empty) if none exists;
		// has_table() and create_table() can then be invoked to create
		// required elements
		
		try {
			$this->db = new \PDO($this->db_driver . ':' . $this->db_loc);
			$this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			Utilities::logger("PDO error opening database " . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
			return false;
		}

		Utilities::logger("opened PDO database " . $this->db_loc, E_USER_NOTICE);
	    return true;	
	}
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>