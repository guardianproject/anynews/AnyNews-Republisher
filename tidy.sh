#!/bin/bash
set -e
# For use with cron:
# 	tidy up the content pool
#

# full path to the aggegator's directory
AHOME=${REPUBLISHER_HOME:-$HOME/Software/guardianproject/anynews}
COMMANDS=./commands

PHP="$(which php)"

# path into which data is stored
DPATH=$($PHP $COMMANDS/read_config.php FEED_DIRECTORY)

# age (in days) of oldest content
DAYS=$($PHP $COMMANDS/read_config.php CACHE_DURATION)

# control the hours at which the retriever actually run
# (assumes cron is running at hourly intervals)
(cd $AHOME; $PHP $COMMANDS/timer.php h 4 21 22 23)
if [[ $? == 0 ]]
then
	(cd $AHOME; $PHP $COMMANDS/tidy_up.php $DPATH $DAYS)
fi
