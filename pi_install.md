# Installation on Raspberry Pi

The Amazon S3-based republisher software runs on Raspberry Pi computers as small as the [Pi Zero 2W](https://www.raspberrypi.com/products/raspberry-pi-zero-2-w/).  If running the standard Raspberry Pi OS (*Raspian*) distribution, you'll need to see if the required tools are available.  Three items are necessary:
- curl
- SQLite3
- PHP 7.4+ (with curl package and with PDO driver support for SQLite3)

## Verify PHP support

```
cmd> which php
```

If PHP is installed, you'll see the path to the executable.  If not, you'll need to install PHP.  If it is installed, verify the version:

```
cmd> echo "<?php phpinfo() ?>" | php | grep version
```

You need PHP 7.4 or above. 

## Verify SQLite3 and PDO support for SQLite3

```
cmd> which sqlite3
```
If SQLite3 is installed, you'll see the path to the executable.  If not, you'll need to install SQLite3. Next, we need to make sure that your version of PHP has support for PHP Data Objects (PDO) and, within that, has drivers for SQLite3.  If PHP is installed:

```
cmd> echo "<?php phpinfo() ?>" | php | grep PDO
```

You should see something like

```
PDO
PDO support => enabled
PDO drivers => sqlite, mysql
PDO Driver for MySQL => enabled
Client API version => mysqlnd 7.4.25
PDO Driver for SQLite 3.x => enabled
SQLite Library => 3.27.2

```

If you don't see 

```
PDO support => enabled 
PDO drivers => sqlite
``` 

You will to install a new version of PHP with SQLite3 support built in, and likely the SQLite3 binary as well.  See below.

## Verify curl installed

You'll need to have the curl command line utility installed (PHP uses it programmatically).  To verify:

```
cmd> which curl
```

You should see something like

```
/usr/bin/curl
```

If not, you'll need to install curl.  

## Installing any necessary updates

Use the following commands to acquire or update the curl or SQLite3 command line tools, or PHP itself:

```
cmd> sudo apt-get install curl
cmd> sudo apt-get install sqlite3
cmd> sudo apt-get install php7.4

...or...
sudo apt-get install php7.4-sqlite3
...or...
sudo apt-get install php7.4-curl

```

## Assess cron tasks

The shell scripts `retrieve.sh` and `tidy.sh` generally run under the control of cron.  If yoiu use your Raspberry Pi in a partly- or mostly-disconnected way, you might consider a different approach to running these scripts - such as on-boot.  
