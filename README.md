# AnyNews Deployment Proxy

These tools help you mirror news content (in the form of RSS feeds) to alternative distribution points to avoid censorship or make content available to communities suffering from high Internet cost, slow or limited access, or natural disaster.The AnyNews app can then be configured to use this mirror (or more than one such mirror).

## Proxy Operation

We'll call the organization with the original news content the "publisher".  We'll call placeswhere content is made available to users "republishers" to avoid sometimes-confusing terms.  The publisher can operate one or more republishers, but other entities can do so on  the publisher behalf.  
  
The AnyNews Deployment Proxy works as follows:

1. The publisher runs an "aggregator" that packages one or more news feeds into a deployment bundle.
2. The publisher runs an "uploader" to upload that deployment bundle to some intermediate store - a place that's hard for censors to block, but easy for others to access.
3. The republishers run a "retriever" that grabs the deployment bundle from the intermediate store and deploys the contents locally for service to end users.

Both the publisher's tools and the republisher's tools can (and, in most cases, should) berun in an automated fashion as `cron` tasks using the provided scripts. 

## Prerequisites

### Publishers

Our intermediate store is Amazon S3. *If you're immediately concerned about this, we have an experimental alternative based on the InterPlanetary File System (IPFS), documented [HERE](./IPFS.md)*.

Our tools for publishers work with libraries for Amazon Web Services (AWS), provided by Amazon.  Use `composer` to install the AWS packages (along with its dependencies) into your AnyNews directory with the following command:

```
# use your AnyNews directory here
cd anynews
composer install
```

Don't have `composer` yet?  Get it [HERE](https://getcomposer.org/download/).

Publishers also require an AWS account. See below for additional AWS setup steps. 


### Republishers

Republishers do not need the AWS tools or an AWS account. 

## Specifying the Content to Publish

Publishers need to specify the RSS feed or feeds to publish. These are defined in a JSON format file whose name is defined in `AnyNewsConfig.php` (the default is `feeds.json`).  We've provided a tool to build and manage this file since editing  the content by hand is a little finicky. Use the supplied script `manage.sh`. 

For each feed you want to publish, you'll need to know:
- the homepage URL for the site serving the feed
- the feed URL
- an image URL, somehow representative of the site, to use when no news item content imagery is provided

You might already know the feeds to be published. Alternatively you can find feeds by doing your own digging around the Internet.  Or our tool can help.  You only need to know thehomepage address of the sites you want to check.  Try:

`./manage.sh check https://www.smh.com.au/`

which returns:

```
Homepage URL: https://www.smh.com.au/
Feed URL:     https://www.smh.com.au/rss/feed.xml [ContentType: application/xml+rss]
Image URL:    https://www.smh.com.au/icons/rss/smh.png w[] h[]
Feed URL:     https://www.smh.com.au/rss/politics/federal.xml [ContentType: application/xml+rss]
Image URL:    https://www.smh.com.au/icons/rss/smh.png w[] h[]
Feed URL:     https://www.smh.com.au/rss/national/nsw.xml [ContentType: application/xml+rss]
Image URL:    https://www.smh.com.au/icons/rss/smh.png w[] h[]
Feed URL:     https://www.smh.com.au/rss/world.xml [ContentType: application/xml+rss]
Image URL:    https://www.smh.com.au/icons/rss/smh.png w[] h[]
Feed URL:     https://www.smh.com.au/rss/national.xml [ContentType: application/xml+rss]
...
Title:        Australian Breaking News Headlines & World News Online | SMH.com.au
Description:  Breaking news from Sydney, Australia and the world. Features the latest business, sport, entertainment, travel, lifestyle, and technology news.

```

Use this information with the same script to add a site to your feed file:

```
./manage.sh add sydney_herald https://www.smh.com.au/ https://www.smh.com.au/rss/feed.xml https://www.smh.com.au/icons/rss/smh.png
``` 

Once you've added something, you can verify you entered it correctly like this:

```
./manage.sh verify sydney_herald
```

Use the feed manager script to add, update or remove feeds (or, with no arguments, to see the full list of commands.  The script backs up your existing file before making changes, so you can always recover from errors.  

## Deployment

### Running the Publishing / Republishing Tasks 

Both publisher and republisher components are run 
as `cron` tasks. The shell scripts to be run by `cron` are:
- `aggregate.sh` (on the publisher machine)
- `retrieve.sh` (on the republisher machine)
- `tidy.sh` (on the republisher machine, optionally) 

Edit each of these scripts, changing the `AHOME` variable to the location of this installation.Then, put something like the following in your crontab or in your hosting service's cron console:

```
(cd full-path-to-your-anynews-directory; ./script_name)
```

Ask `cron` to run these scripts every hour.  Using an internal timer, the publisher task (`aggregator.sh`) is, by default, set to run every three hours.  The republisher's task (`retrieve.sh`) runs every three hours, at a one-hour offset from the publisher by default.  

To modify that behavior, update the input to `timer.php` in each script to run less often.  For example, here's how to run the script every 6 hours:

```
# control the hours at which the retriever actually run (assumes cron runs this at hourly intervals)
# every 6 hours
php ./timer.php h 1 7 13 19
```

### Republisher Deployment Directory

Republishers should modify the content FEED_DIRECTORY in `AnyNewsConfig.php` to the full path to the directory receiving the feed update.

## Amazon Web Services/S3 Setup

### Publisher

Our intermediate store is AWS S3 - a hardened, highly-performant storage facility that's difficult for censors to block because it's used by a wide range of businesses to supply in many cases necessary services.

You'll need an AWS developer account to get your access ID and Key for using the AWS APIs.  Find instructions [HERE](https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-setup-api-key-with-console.html).Once you've got your account, create a "bucket" on AWS's S3 service (instructionsfor doing so are [HERE](https://docs.aws.amazon.com/AmazonS3/latest/userguide/create-bucket-overview.html). You'll be asked to create the bucket in a specific AWS region.  Remember the regionidentifier, and modify the `AnyNewsConfig.php` file to define your region:
```
class AnyNewsConfig {
	...
	const S3_REGION  = 'YOUR_S3_REGION_HERE';
	...
}
```

Last, use the provided script `s3setup.sh` to import your AWS credentials (ID and Key)under a name you supply ('uploader' or 'mirror' or something similar).  Simply run:
```
./s3setup.sh acct_name aws_id aws_key s3_bucketname
```

### Republisher

Modify the file `AnyNewsConfig.php` with the proper region and bucket identifiers, as provided by the publisher:

```
class AnyNewsConfig {
	...
	const S3_REGION  = 'YOUR_S3_REGION_HERE';		// provided by publisher
	const S3_BUCKET  = 'PUBLISHER_BUCKET_ID_HERE';	// provided by publisher
	...
}
```

## Life on the Edge

...of the network, that is.  We've experimented with running the AnyNews republisher software on a Raspberry Pi.  [HERE](./pi_install.md) are some additional instructions for getting that environment working.


## Docker Deployment

The latest docker image is available at `registry.gitlab.com/guardianproject/anynews/anynews-republisher:main`

```console
# Run our docker image
$ docker run --rm -it \
 -v $(pwd)/config:/app/config \
 -v $(pwd)/data:/app/data \
 registry.gitlab.com/guardianproject/anynews/anynews-republisher:main \
 /bin/bash
app@fe8b4a2ae910:/app$ ./manage.sh 
Usage: ./commands/feed_manager.php command <arg1>...<arg2>...<arg3>...<arg4>
Commands:
	list (print feed names)
	list name (print current properties for the named feed)
	remove name (remove the named feed)*
	add name homepage_url feed_url default_image_url (add a new feed)*
	update name homepage_url feed_url default_image_url (update an existing feed)*
	verify name (verify a feed's URLs are all valid)
	check URL (visit a URL to see if it offers a news feed)
	backups (list the available backup files)
* - these commands backup existing file before updating it (use command [backups] to see saved editions
app@fe8b4a2ae910:/app$ ./manage.sh add gp https://guardianproject.info https://guardianproject.info/index.xml https://guardianproject.info/GP_Logo_hires.png
feed [gp] added
app@fe8b4a2ae910:/app$ ./manage.sh list
gp
app@fe8b4a2ae910:/app$ ./aggregate.sh 
app@fe8b4a2ae910:/app$ ./tidy.sh 
logging [ON]; messages to file ./data/logs/anynews.log
Working from root directory ./data/feeds
Removing content beyond [7] days (sell-by date: 2023-02-02T14:18:08+00:00) old
scanning ./data/feeds/sydney_herald/media
scanning ./data/feeds/gp/media
Lovely. We're done.
```

For the unattended use case you should setup a cron to run the `s3batch.sh` script:

```cron
$ docker run --rm -it \
 -v $(pwd)/config:/app/config \
 -v $(pwd)/data:/app/data \
 -e AWS_DEFAULT_REGION=us-east-2 \
 -e AWS_S3_BUCKET=your-bucket-name \
 registry.gitlab.com/guardianproject/anynews/anynews-republisher:main \
 s3batch.sh
```

```
# Build the docker image
make docker/build
# Run the new docker image
$ docker run --rm -it \
 -v $(pwd)/config:/app/config \
 -v $(pwd)/data:/app/data \
 registry.gitlab.com/guardianproject/anynews/anynews-republisher:dev \
 /bin/bash
```
