#!/bin/bash
#
# manage the feeds we aggregate
#
COMMANDS=./commands

PHP="$(which php)"

$PHP $COMMANDS/feed_manager.php $1 $2 $3 $4 $5
