SHELL := /bin/bash
export IMAGE_NAME ?= anynews-republisher
export DOCKER_IMAGE ?= registry.gitlab.com/guardianproject/anynews/$(IMAGE_NAME)
export DOCKER_TAG ?= dev
export DOCKER_BUILD_FLAGS ?=
export DOCKER_IMAGE_NAME ?= $(DOCKER_IMAGE):$(DOCKER_TAG)
export TARGET_DOCKER_REGISTRY=registry.gitlab.com/guardianproject/anynews
export SOURCE_DOCKER_REGISTRY=${TARGET_DOCKER_REGISTRY}

-include $(shell curl -sSL -o .build-harness "https://cloudposse.tools/build-harness"; echo .build-harness)

gitlab/login:
	docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)

docker/test:
	docker run --rm $(DOCKER_IMAGE):$(DOCKER_TAG) ls -al /app
	docker run --rm $(DOCKER_IMAGE):$(DOCKER_TAG) ls -al /app/config
	mkdir -p ./test-data/republisher/config
	mkdir -p ./test-data/republisher/data
	mkdir -p ./test-data/republisher/data/logs
	mkdir -p ./test-data/republisher/data/data
	mkdir -p ./test-data/republisher/data/proxyservices
	mkdir -p ./test-data/republisher/data/proxyservices/utilities
	mkdir -p ./test-data/republisher/data/proxyservices/utilities/db
	mkdir -p ./test-data/republisher/data/proxyservices/utilities/data
	mkdir -p ./test-config
	cp config/* ./test-config/
	chown -R 1000:1000 ./test-config
	chown -R 1000:1000 ./test-data
	ls -al config
	ls -al test-config
	docker run --user 1000:1000 --rm -v ./test-config:/app/config:z -v ./test-data:/app/data:z $(DOCKER_IMAGE):$(DOCKER_TAG) ls -al /app
	docker run --user 1000:1000 --rm -v ./test-config:/app/config:z -v ./test-data:/app/data:z $(DOCKER_IMAGE):$(DOCKER_TAG) ls -al /app/config
	docker run --user 1000:1000 --rm -v ./test-config:/app/config:z -v ./test-data:/app/data:z $(DOCKER_IMAGE):$(DOCKER_TAG) ./manage.sh
	docker run --user 1000:1000 --rm -v ./test-config:/app/config:z -v ./test-data:/app/data:z $(DOCKER_IMAGE):$(DOCKER_TAG) ./manage.sh | grep -q "Commands"
	rm -rf test-data test-config
