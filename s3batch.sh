#!/bin/bash
set -e

check_vars()
{
    var_names=("$@")
    for var_name in "${var_names[@]}"; do
        [ -z "${!var_name}" ] && echo "$var_name is unset." && var_unset=true
    done
    [ -n "$var_unset" ] && exit 1
    return 0
}

check_vars AWS_DEFAULT_REGION AWS_S3_BUCKET_NAME
php ./commands/batch_run.php