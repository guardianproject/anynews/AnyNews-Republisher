<?php
/**
 * ----------------------------------------------------------------------
 * component: FeedManager
 * Manages the feed list (rather than asking people to edit by hand).
 * Creates automatic backup of existing file before execution.
 * 
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\anynews; 
use guardianproject\proxyservices\utilities\Utilities as Utilities;
use guardianproject\proxyservices\utilities\PageParser as PageParser;
use guardianproject\anynews\RssFilter as RssFilter;

use AnyNewsConfig;
use \Exception;

define('NO_SUCH_FEED', -1);

class FeedManager {
	// ----------------------------------------------------------------------
	// private variables
	// ----------------------------------------------------------------------

	private $feed_file = AnyNewsConfig::FEED_LIST;
	private $feed_array;
	
	// ---------------------------------------------------------------------- 
	// constructor
	// ---------------------------------------------------------------------- 

	public function __construct() {
		// check backup directory exists
		if (! file_exists(AnyNewsConfig::LIST_BACKUP_DIRECTORY)) {
			Utilities::logger('creating backup directory for feed lists [' . AnyNewsConfig::LIST_BACKUP_DIRECTORY . ']', E_NOTICE);
			mkdir(AnyNewsConfig::LIST_BACKUP_DIRECTORY);
		} else {

			Utilities::logger(' backup directory exists for feed lists [' . AnyNewsConfig::LIST_BACKUP_DIRECTORY . ']', E_NOTICE);
		}
		
		if (file_exists($this->feed_file)) {
			$feeds = file_get_contents($this->feed_file);
			$this->feed_array = json_decode($feeds, true);
		} else {
			$this->feed_array = array();
		}
	}
	
	// ---------------------------------------------------------------------- 
	// ---------------------------------------------------------------------- 
	// public methods
	// ---------------------------------------------------------------------- 
	// ---------------------------------------------------------------------- 

	public function exists($name) {		
		if (array_key_exists($name, $this->feed_array)) { return true; }

		return false;
	}	

	public function get($name) {
		return $this->feed_array[$name];
	}
		
	public function add($name, $home_url, $feed_url, $default_img) {
		$new = array(
			'home_url' => $home_url, 
			'feed_url' => $feed_url, 
			'feed_img' => $default_img
		);
		$this->feed_array[$name] = $new;
		Utilities::logger('added [' . $name . ']' , E_NOTICE);
		return true;
	}
	
	public function update($name, $home_url, $feed_url, $default_img) { 
		if (array_key_exists($name, $this->feed_array)) {
			$this->feed_array[$name]['home_url'] = $home_url;
			$this->feed_array[$name]['feed_url'] = $feed_url; 
			$this->feed_array[$name]['feed_img'] = $default_img;
			Utilities::logger('updated [' . $name . ']' , E_NOTICE);
		} else {
			$this->add($name, $home_url, $feed_url, $default_img);
			Utilities::logger('added [' . $name . ']' , E_NOTICE);
		}
		
		return $true;
	}	

	public function remove($name) {
		if (! array_key_exists($name, $this->feed_array)) { return NO_SUCH_FEED; }		
		unset($this->feed_array[$name]);
		Utilities::logger('removed [' . $name . ']' , E_NOTICE);

		return true;
	}
	
	public function list() {		
		return array_keys($this->feed_array);
	}

	public function save() {
		$this->backup(); // save existing contents before committing
		$str = json_encode($this->feed_array, JSON_PRETTY_PRINT);
		file_put_contents($this->feed_file, $str);
		Utilities::logger('changes saved to [' . $this->feed_file . ']' , E_NOTICE);
	}
		
	public function listBackups() {
		$list = array();
		$dh = dir(AnyNewsConfig::LIST_BACKUP_DIRECTORY);
		while (false !== ($entry = $dh->read())) {
   			if (($entry != '.') && ($entry != '..')) { $list[] = $entry; }
		}
		$dh->close();
		
		return $list;	
	}
	
	public function verify($name) {
		if (! array_key_exists($name, $this->feed_array)) { return null; }
		
		$info = $this->getWebpageInfo($this->feed_array[$name]['home_url']);
		if ($info) { print "URL: " . $info['provided_url'] . " [contentType: " . $info['content_type'] . "]\n"; }
		else { print "bad URL: " . $this->feed_array[$id]['home_url'] . "\n"; }

		$info = $this->getWebpageInfo($this->feed_array[$name]['feed_url']);
		if ($info) { print "URL: " . $info['provided_url'] . " [contentType: " . $info['content_type'] . "]\n"; }
		else { print "bad URL: " . $this->feed_array[$id]['feed_url'] . "\n"; }

		$info = $this->getWebpageInfo($this->feed_array[$name]['feed_img']);
		if ($info) { print "URL: " . $info['provided_url'] . " [contentType: " . $info['content_type'] . "]\n"; }
		else { print "bad URL: " . $this->feed_array[$id]['feed_img'] . "\n"; }

		return true;
	}

	public function check($url) {
		$info = $this->getWebpageInfo($url);
		if (! $info) {
			print "bad URL: " . $url . "\n";
			return false;
		}
		
		if (! $info['feeds'] || count($info['feeds']) < 1) {
			print $url . " DOES NOT advertise news feeds\n";
			print "Here's everything I learned about $url:\n";
			print json_encode($info, JSON_PRETTY_PRINT) . "\n";
			
			return false;
		}
		
		if ($info['canonical_link']) { print "Homepage URL: " . $info['canonical_link'] . "\n"; }
		else { print "Homepage URL: " . $info['provided_url'] . "\n"; }		
		if ($info['redirect_url']) { print "Redirect URL: " . $info['redirect_url'] . "\n"; }
		
		if ($info['feeds'] && count($info['feeds']) > 0) {
			foreach ($info['feeds'] as $feed) {
				$rssf = new RssFilter($feed);
				$ca = json_decode($rssf->channelAttributes(), true);
				if ($ca['content_type'] == 'unknown') {
					print "Feed URL:     " . $feed . " [unknown ContentType]\n";
				} else {
					print "Feed URL:     " . $feed . " [ContentType: " . $ca['content_type'] . "]\n";
					if (array_key_exists('image', $ca) && ($ca['image'][0] != null)) {
						print "Image URL:    " . $ca['image'][0] . " w[" . $ca['image'][1] . "] h[" . $ca['image'][2] . "]\n";
					} else {
						print "NOTE: feed does NOT define a <channel> image\n";
					}
				}
			}
		}	

		if ($info['title']) { print "Title:        " . $info['title'] . "\n"; }
		if ($info['description']) { print "Description:  " . $info['description'] . "\n"; }
				
		return true;
	}

	// ---------------------------------------------------------------------- 
	// ---------------------------------------------------------------------- 
	// private methods
	// ---------------------------------------------------------------------- 
	// ---------------------------------------------------------------------- 

	private function find($name) {
	 	if (! array_key_exists($name, $this->feed_array)) { return NO_SUCH_FEED; }
	 
	 	return $this-feed_array[$name];
	 }

	private function backup() {
		if (! file_exists($this->feed_file)) { return true; }
		
		$feeds = file_get_contents($this->feed_file);
		$next = $this->nextName();
		$dir = dirname($next);
		if (! file_exists($dir) ) {
			mkdir($dir, 0755, true);
		}
		file_put_contents($next, $feeds);
		Utilities::logger('current feeds file backed up to [' . $next . ']' , E_NOTICE);
		return true;
	}
	
	private function getWebpageInfo($url) {
		try {
			$pp = new PageParser();
			$pp->setUrl($url);
			$res = $pp->acquire();
			if ($res['http_code'] != 200) { return null; }
		} catch (Exception $e) {
			print "ERROR:" . $e->getMessage() . "\n";
			return null;
		}
					
		return json_decode($pp->asJson(), true);
	}

	private function nextName() {
		$ds = date('dMY', time());
		$base = AnyNewsConfig::LIST_BACKUP_DIRECTORY . '/' . $this->feed_file . '.' . $ds;

		$i = 1;
		$exists = true;
		do {
			$test_name = $base . '.' . $i;
			if (! file_exists($test_name)) { $exists = false; }
			else { $i++; }
		} while($exists == true);
		
		return $test_name;
	}
		
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>