<?php
/** 
 * ----------------------------------------------------------------------
 * component: S3Credential
 * (access information for the AWS S3 service)
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
**/

namespace guardianproject\anynews;

use guardianproject\proxyservices\utilities\DataStore as DataStore;
use guardianproject\proxyservices\utilities\Utilities as Utilities;
use guardianproject\proxyservices\utilities\Cryptor as Cryptor;
use UtilitiesConfig as Config;
use \Exception;
use \PDO;

class S3Credential extends DataStore {

	private $bucket = null; // AWS S3 bucket name
	private $is_encrypted;
	private $cryptor;
	
	// ----------------------------------------------------------------------
	// constructor
	// ----------------------------------------------------------------------	
	
	public function __construct($force = false) {
		$this->is_encrypted  = Config::ENCRYPT_CREDENTIALS;
		if ($this->is_encrypted) {
			try {
				$this->cryptor = new Cryptor();
			} catch (Exception $e) {
				throw new Exception($e->getMessage());
			}
		}
		$this->info_database = 'cred.db';
		$this->static_data   = 's3_credentials.csv';
		$this->table_name    = 's3cred';
	
		$this->db_loc   = Config::privDirectory() . '/' . $this->info_database;
		$this->data_loc = Config::privDirectory() . '/' . $this->static_data;
		
		if (! $this->initialize($force)) {
			throw new Exception('Object creation failed. Database ' . $this->info_database . ' corrupt or incomplete');
		}
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// getCredentials
	// ----------------------------------------------------------------------
	
	public function getCredentials($acct) {
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE un=?';
		Utilities::logger("PDO query: " . $sql . "(" . $acct . ")", E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
	    	if ($q) {
	    		$q->execute(array($acct));
	    		$res = $q->fetch(PDO::FETCH_ASSOC);
	    	} else {
	    		return null;
	    	}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving credentials for account [' . $acct . ']', E_WARNING);
			return null;
		}
		
		if ($res == null) { return null; }
		
		if ($this->is_encrypted) {
			$res['id'] = $this->cryptor->decrypt($res['id']);
			$res['key'] = $this->cryptor->decrypt($res['key']);
		}
		print_r($res);
		return $res;
	}

	// ----------------------------------------------------------------------
	// setCredentials
	// ----------------------------------------------------------------------
		
	public function setCredentials($credential) {
		if ($this->is_encrypted) {
			$credential['id'] = $this->cryptor->encrypt($credential['id']);
			$credential['key'] = $this->cryptor->encrypt($credential['key']);
		}

		return $this->load_entry($credential['un'], $credential['id'], $credential['key'], $credential['bucket']);
	}

	// ----------------------------------------------------------------------
	// generateKey - generate a new encryption key 
	// ----------------------------------------------------------------------
	
	public function generateKey() {
		Cryptor::generateKey();
	}
	
	// ----------------------------------------------------------------------
	// listAccounts
	// ----------------------------------------------------------------------	
	
	public function listAccounts() {
		$vals = array();
		$sql = 'SELECT un FROM ' . $this->table_name . ';';
    	Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
    	
	    try {
	    	$q = $this->db->query($sql);
	    	if ($q) {
	    		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    			$vals[] = $res['un'];
	    	 	} 
	    	 }
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving accounts', E_USER_NOTICE);
			return null;
		}

		return $vals;	
	}

	// ----------------------------------------------------------------------
	// dump
	// ----------------------------------------------------------------------	
	
	public function dump($z = null) {
		$fp = fopen($this->data_loc, 'w+');
		if (! $fp) {
		}
		fwrite($fp, 'User Name,Access Key Id,Secret Access Key,Bucket' . "\n"); // CSV header
		
		$accts = array();
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
    	Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
    	
	    try {
	    	$q = $this->db->query($sql);
	    	if ($q) {
	    		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    			$fields = array(
	    				$res['un'],
	    				$res['id'],
	    				$res['key'],
	    				$res['bucket']
	    			);
	    			fputcsv($fp, $fields);
	    	 	} 
	    	 }
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving accounts', E_USER_NOTICE);
			return null;
		}

		fclose($fp);
		return true;
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// protected methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// load_table
	// ----------------------------------------------------------------------

	protected function load_table() {
		$fcc = fopen($this->data_loc, 'r');
		if (! $fcc) {
			Utilities::logger("error opening static credential data file " . $this->data_loc, E_ERROR);
			return false;
		}
		
		$num = 0;
		$vals = fgetcsv($fcc, 200, ','); // strip header
		while ($vals = fgetcsv($fcc, 200, ',')) {
			
			$id   = trim($vals[1]);
			$key  = trim($vals[2]);
			$buck = trim($vals[3]);
			$this->load_entry(trim($vals[0]), $id, $key, $buck);
			$num++;
		}
		
		Utilities::logger($num . ' credentials entries added to table', E_USER_NOTICE);
		
		fclose($fcc);
		return true;
	}
	
	// ----------------------------------------------------------------------
	// load_entry
	// ----------------------------------------------------------------------

	protected function load_entry($name, $id, $key, $bucket) {
	    $sql = 'INSERT OR REPLACE INTO ' . $this->table_name . ' VALUES (?,?,?,?)';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    try {
			//$this->db->exec($sql);
			$q = $this->db->prepare($sql);
			if ($q) {
				$q->execute(array($name, $id, $key, $bucket));
			}
		} catch (PDOException $e) {
			Utilities::logger('PDO: load_entry failed to insert values (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// create_table
	// un:     AWS User Name
	// id:     AWS Access Key Id
	// key:    AWS Secret Access Key
	// bucket: AWS S3 bucket name
	// ----------------------------------------------------------------------

	protected function create_table() {
		$sql = 'CREATE TABLE ' . $this->table_name . ' (un TEXT PRIMARY KEY, id TEXT, key TEXT, bucket TEXT);';
		Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
		
		try {
			$this->db->exec($sql);
		} catch (PDOException $e) {
	    	Utilities::logger("PDO error creating database table in " . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
	    	return false;
	    }
	    
	    return true;
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------	
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>