<?php
/** 
 * ----------------------------------------------------------------------
 * component: S3Service
 * Pastes/POSTs blocks of text (or contents of a file) to Amazon S3
 * in a "pastebin.com" style manner, optionally given a URL path of 
 * our choosing ("vanity url"). Subsequently, retrieve pasted text 
 * using computed or vanity URL and
 * optionally delete it (before it's automatic timeout).
 *
 * Supports both text and binary data (the latter to/from file only).
 *
 * Full docs: http://docs.aws.amazon.com/AmazonS3/latest/dev/UploadObjSingleOpPHP.html
 * =============
 * simple usage:
 *    $s3 = new S3Service();
 *    $info = $s3->paste($content, true);  // optional urlencode
 *    $content = $s3->retrieve($info['url'], true); // optional urldecode
 *    print $contents;
 *
 *    $s3->remove($info['uuid']); // remove it prior to sunset if necessary
 *
 * paste method returns JSON:
 * {
 *   "uuid":   "a25c4e05-bf0e-4c2f-bb1a-4f0031d2ddf9",
 *   "status": "created",
 *   "digest": "31e6077fca23627d5ddc64eabf23fd0abf0cf2c6",
 *   "url":    "https:\/\/ptpb.pw\/ADHmB3_KI2J9Xdxk6r8j_Qq_DPLG",
 *   "long":   "ADHmB3_KI2J9Xdxk6r8j_Qq_DPLG",
 *   "label":  "~android46abb1457eee128d0d48c1b8a53a82fceac39ec4_gt",
 *   "sunset": "2015-10-09T16:38:15.595833+00:00"
 * }
 *
 * NOTE: "label" is supplied only if vanity URL requested. Otherwise
 * "long" is the string providing the URL path for the object pasted. UUID
 * is required for deletion.
 * =============
 * richer usage:
 *    $s3 = new S3Service(); 
 *    $s3->setVanityString($str);    // provide a vanity URL for content
 *    $s3->setServiceTimeout($secs); // set wait-time for service
 *    $s3->setSunset($secs);         // set how long the pasted content should survive
 *    $s3->setPrivacy($bool);        // private or public URL?
 *    $s3->setEncryption($bool);   // turns on content encryption (or off if null) 
 *
 *    $s3->setFile($fpath);          // to paste FROM a file
 *    $s3->paste();
 *
 *    $res = $s3->retrieveVanity();  // using vanity URL you provided
 *
 *    $s3 = new S3Service); 
 *    $s3->setFile($fpath);          // to retrieve INTO a file
 *    $fpath = $s3->retrieve($url); 
 *
 *    $bool = $s3->isUnique('...my_string...'); // assure a vanity string is not already in use
 *
 *    $bool = $s3->exists($url);     // is this url already in use?
 *    $bool = $s3->hashExists(sha1($my_content)); // of your content
 *    $bool = $s3->fileHashExists($file); // of your file content   
 *
 * This version of the ContentUploader interface has some special methods
 * for dealing with AWS S3 "buckets":
 *    $bool = $s3->bucketExists($name, $bool); // within my account, or in the global namespace     
 *    $bool = $s3->createBucket($name);
 *    $bool = $s3->deleteBucket($name);
 *    $s3->setBucket($name);
 *    $name = $s3->getBucket();
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
**/

namespace guardianproject\anynews;

use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;
use guardianproject\anynews\ContentUploader as ContentUploader;
use guardianproject\proxyservices\utilities\Cryptor as Cryptor;
use guardianproject\proxyservices\utilities\Utilities as Utilities;
use guardianproject\proxyservices\utilities\UUID as UUID;

use AnyNewsConfig;
use \DateTime;
use \Exception;

class S3Service implements ContentUploader {

	private $pb_type          = 's3';
	
	// ----------------------------------------------------------------------
	// private variables
	// ----------------------------------------------------------------------

	private $service_endpoint = AnyNewsConfig::S3_BASE_URL;
	private $service_acct     = '';
	
	private $s3_client;                // AWS API object
	private $s3_bucket;                // my storage location
	
	private $is_vanity        = false; // is vanity string supplied?
	private $vanity_str;               // the vanity string to use
	
	private $is_binary        = false; // dealing w binary files
	
	private $file_placeholder;         // location to/from 
	private $folder;				   // folder within given bucket
	
	private $timeout          = 10;    // service timeout (seconds)
	private $sunset           = 86401; // content timeout (1 day + 1 sec, in secs)
	private $expiry;				   // content timeout date/time (unix secs)

	private $privacy          = false; // visibility of object pasted
	private $encrypt          = false; // encryption mode
	
	private $cryptor;
	
	// ----------------------------------------------------------------------
	// constructor
	// ----------------------------------------------------------------------	
	
	public function __construct($force = false) {
		$this->setCredentials();
    }
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// setServiceTimeout
	// ----------------------------------------------------------------------
	
	public function setServiceTimeout($secs) {
		$this->timeout = $secs;
	}
	
	// ----------------------------------------------------------------------
	// setVanityString
	// ----------------------------------------------------------------------
	
	public function setVanityString($str) {
		$this->is_vanity = true;
		$this->vanity_str = $str;
	}
	
	// ----------------------------------------------------------------------
	// setSunset
	// ----------------------------------------------------------------------
	
	public function setSunset($secs) {
		$this->sunset = $secs;
		Utilities::logger('timeout NOT SET. S3 supports expiry on BUCKET only', E_WARNING);	
	}

	// ----------------------------------------------------------------------
	// setExpiry
	// ----------------------------------------------------------------------
	
	public function setExpiry($timestamp) {
		$this->expiry = $timestamp;
	}

	// ----------------------------------------------------------------------
	// setFolder
	// ----------------------------------------------------------------------
	
	public function setFolder($fn) {
		$this->folder = $fn;
	}
	
	// ----------------------------------------------------------------------
	// setPrivacy
	// ----------------------------------------------------------------------
	
	public function setPrivacy($bool) {
		$this->privacy = $bool;
	}		

	// ----------------------------------------------------------------------
	// setEncryption
	// ----------------------------------------------------------------------
	
	public function setEncryption($bool) {
		$this->encrypt = $bool;
		if($bool) $this->cryptor = new Cryptor();
		//print "encryption set to: " . json_encode($this->encrypt) . "\n";
	}		
		
	// ----------------------------------------------------------------------
	// setBinary
	// ----------------------------------------------------------------------
	
	public function setBinary($bool) {
		$this->is_binary = $bool;
	}
	
	// ----------------------------------------------------------------------
	// setFile
	// ----------------------------------------------------------------------
	
	public function setFile($fn) {
		$this->file_placeholder = $fn;
	}

	// ----------------------------------------------------------------------
	// folderExists
	// ----------------------------------------------------------------------	
	
	public function folderExists($fn) {
		return $this->privateFolderExists($fn);
	}	

	// ----------------------------------------------------------------------
	// createFolder
	// ----------------------------------------------------------------------	
	
	public function createFolder($fn) {
		return $this->privateCreateFolder($fn);
	}	
	
	// ----------------------------------------------------------------------
	// paste
	// ----------------------------------------------------------------------	
	
	public function paste($content = null) {
		if (!$content && !$this->file_placeholder) {
			Utilities::logger('no content supplied to paste', E_ERROR);
			return array('status' => 'no content supplied to paste');
		}
		
		try {
			$result = $this->post($content);
			if (array_key_exists('status', $result) && ($result['status'] != 'created')) {
				Utilities::logger('post returned status of [' . $result['status'] . ']', E_ERROR);
				return array('status' => 'error; post returned status of [' . $result['status'] . ']');
			}
			$result['type'] = $this->pb_type;
			Utilities::logger('contents posted to ' . $result['url'], E_NOTICE);
			return $result;
		} catch (Exception $e) {
			Utilities::logger("paste error: " . $e->getMessage(), E_ERROR);
			return array('status' => "paste error: " . $e->getMessage());
		}
	}

	// ----------------------------------------------------------------------
	// retrieve
	// ----------------------------------------------------------------------	
	
	public function retrieve($key) {
		try {
			$result = $this->acquire($key);
			Utilities::logger('contents retrieved from url [' . $key . ']', E_NOTICE);
			return $result;
		} catch (Exception $e) {
			Utilities::logger("retrieve error (" . $key . "): " . $e->getMessage(), E_ERROR);
			return null;
		}
	}

	// ----------------------------------------------------------------------
	// retrieveToFile
	// ----------------------------------------------------------------------	
	
	public function retrieveToFile($label, $path = null) {
		if (! $path) {
			$path = $this->file_placeholder;
		}
		if (! $path) {
			Utilities::logger("no file path specified", E_ERROR);
			return null;
		}
		
		try {
			$result = $this->acquireToFile($label, $path);
			Utilities::logger('contents retrieved from label [' . $label . '] to file ' . $path, E_NOTICE);
			return $result;
		} catch (Exception $e) {
			Utilities::logger("retrieve error for " . $label . ": " . $e->getMessage(), E_ERROR);
			return null;
		}
	}
	
	// ----------------------------------------------------------------------
	// retrieveVanity
	// ----------------------------------------------------------------------	
	
	public function retrieveVanity() {
		if (!$this->is_vanity || ($this->vanity_str == null)) {
			Utilities::logger("retrieve error: no vanity string provided", E_ERROR);
			return null;
		}
		
		$url = $this->service_endpoint . $this->s3_bucket . '/' . $this->vanity_str;
		return $this->retrieve($url);
	}

	// ----------------------------------------------------------------------
	// retrieveMetadata
	// ----------------------------------------------------------------------	
	
	public function retrieveMetadata($label) {
		try {
			$result = $this->acquireMetadata($label);
			Utilities::logger('metadata retrieved for label [' . $label . ']', E_USER_NOTICE);
			return $result;
		} catch (Exception $e) {
			Utilities::logger("retrieve error (" . $label . "): " . $e->getMessage(), E_ERROR);
			return null;
		}
	}
	
	// ----------------------------------------------------------------------
	// remove
	// ----------------------------------------------------------------------	
	
	public function remove($uuid) {
		$params = $this->cm->retrieveViaUUID($uuid);
		if (! $params) {
			Utilities::logger('no metadata available for UUID [' . $uuid . ']', E_ERROR);
			return array('status' => 'no metadata available for UUID [' . $uuid . ']');
		}
		
		try {
			$result = $this->delete($params);
			if (array_key_exists('status', $result) && ($result['status'] != 'deleted')) {
				Utilities::logger('remove returned wrong status [' . json_encode($result) . ']', E_ERROR);
			} else {
				Utilities::logger('contents removed for uuid [' . $params['uuid'] . ']' . ' using label [' . $params['label'] . ']', E_NOTICE); 
			}	
		} catch (Exception $e) {
			Utilities::logger("remove error (" . $uuid . "): " . $e->getMessage(), E_ERROR);
			$result = array('status' => 'failed; error: ' . $e->getMessage());
		}
		
		return $result;
	}		

	// ----------------------------------------------------------------------
	// exists - via URL (short or long)
	// ----------------------------------------------------------------------	
	
	public function urlExists($url) {
		try {
			$result = $this->poke($url, true);
			//Ok, I can fetch this URL, so go get it's metadata
			$components = parse_url($url);
			list($nothing,$bucket,$item) = explode('/', $components['path']);
			$result = $this->ping($item, $force);
			return $result;
		} catch (Exception $e) {
			return false;
		}
	}
	
	// ----------------------------------------------------------------------
	// isUnique - is a proposed vanity string unique?
	// ----------------------------------------------------------------------	
	
	public function isUnique($label, $force = true) {
		if (! $force) {
			$params = $this->cm->retrieveViaLabel($label);
			if ($params) { return false; }
			else { return true; }
		}
		
		try {
			$result = $this->ping($label, $force);
		} catch (Exception $e) {
			return true;
		}
		
		return false;
	}
	
	// ----------------------------------------------------------------------
	// fileHashExists - via sha1 hash over a file
	// ----------------------------------------------------------------------	
	
	public function fileHashExists($fn) {
		$hash = sha1(file_get_contents($fn));
		return $this->hashExists($hash);
	}	

	// ----------------------------------------------------------------------
	// hashExists - via sha1 hash
	// ----------------------------------------------------------------------	
	
	public function hashExists($hash) {
		$params = $this->cm->retrieveViaHash($hash);
		if (! $params) {
			return false;
		}  
		
		try {
			$result = $this->ping($params['label'], $force);
			// an item with given hash exists, return data
			return $result;
		} catch (Exception $e) {
			// ERROR - local DB contains item, but item NOT in AWS bucket!
			// So DELETE local DB entry
			$this->cm->remove($params['uuid']);
			return false;
		}
	}

	// ----------------------------------------------------------------------
	// resourceEndpoint
	// ----------------------------------------------------------------------
		
	public function resourceEndpoint($type, $resource) {
		switch($type) {
			case 'vanity':
			case 'label':
				$url = $this->service_endpoint . $this->s3_bucket . '/' . $resource;
				break;
			case 'digest':
			case 'uuid':
			case 'long':	
				$params = $this->cm->retrieveUsing($type, $resource);
				if (! $params) { $url = null; }			
				else { $url = $this->service_endpoint . $this->s3_bucket . '/' . $params['label']; }
				break;
			default:
				$url = $this->service_endpoint;
		}
		
		return $url;
	}

	// ----------------------------------------------------------------------
	// bucketExists
	// ----------------------------------------------------------------------
	    
    public function bucketExists($bucket_name, $globally = false) {
    	$found = false;
    	
    	// exists on this S3 account?
    	try {
			$result = $this->s3_client->listBuckets();
			foreach ($result['Buckets'] as $bucket) {
				if ($bucket['Name'] == $credentials['bucket']) { $found = true; }
			}
		} catch (Exception $e) {
			Utilities::logger('error: could not list buckets - ' . $e->getMessage(), E_ERROR);
		}
		
		if (! $globally) { return $found; }
		
		// does the name exist globally?
		try {
			$found = $this->s3_client->doesBucketExist($bucket_name);
		} catch (Exception $e) {
    		Utilities::logger('failed check for bucket name [' . $bucket_name . ']. error: ' . $e->getMessage(), E_ERROR);
    		$found = false;
    	}
    	
    	return $found;
    }

	// ----------------------------------------------------------------------
	// createBucket
	// ----------------------------------------------------------------------
	    
    public function createBucket($bucket_name) {
    	try {
    		// create it and wait for it to exist
    		$this->s3_client->createBucket(array('Bucket' => $bucket_name));
			$this->s3_client->waitUntil('BucketExists', array('Bucket' => $bucket_name));
			Utilities::logger('created bucket [' . $bucket_name . ']', E_USER_WARNING); 
	   		return true; 
    	} catch (Exception $e) {
    		Utilities::logger('failed to create bucket [' . $bucket_name . ']. error: ' . $e->getMessage(), E_ERROR);
    		return false;
    	}	
    }

	// ----------------------------------------------------------------------
	// getter/setter for the bucket
	// ----------------------------------------------------------------------
    
    public function setBucket($bucket_name) { $this->s3_bucket = $bucket_name; }
    public function getBucket() { return $this->s3_bucket; }
        
	// ----------------------------------------------------------------------
	// deleteBucket
	// ----------------------------------------------------------------------
	    
    public function deleteBucket($bucket_name) {
    	try {
    		// clean up the contents of the bucket (you shouldn't be doing this
    		// unless there are none! ...but still)
    		$this->s3_client->clearBucket($bucket_name);
    	
    		// delete it and wait for (mourn) its passing
    		$this->s3_client->deleteBucket(array('Bucket' => $bucket_name));
    		$this->s3_client->waitUntil('BucketNotExists', array('Bucket' => $bucket_name));
    		Utilities::logger('deleted bucket [' . $bucket_name . ']', E_USER_WARNING);
	   		return true; 
    	} catch (Exception $e) {
    		Utilities::logger('failed to delete bucket [' . $bucket_name . ']. error: ' . $e->getMessage(), E_ERROR);
    		return false;
    	}	
    }
			
	// ------------------------------------------------------------------
	// ------------------------------------------------------------------
	// private methods
	// ------------------------------------------------------------------
	// ------------------------------------------------------------------

	// ------------------------------------------------------------------
	// acquire (HTTP GET)
	// ------------------------------------------------------------------	
	
	private function acquire($key) {
		$tfn = null;
		
		/*
		$params = $this->cm->retrieveViaUrl($url);
		if (! $params) {
			Utilities::logger('error - failed to find metadata for url [' . $url . ']', E_ERROR);
			return null;
		}
		*/
		
		$data = array(
			'Bucket' => $this->s3_bucket, // Bucket is required
			'Key' => $key,    // Key is required
		);
		
		// for decryption, save in a temporary file first
		if ($this->encrypt) {
			$tfn = tempnam("/tmp", substr(sha1(time()), 0, 7));
			//print "saving incoming encrypted contents to temporary file" . $tfn . "\n";
			$data['SaveAs'] = $tfn;
		}

		$result = $this->s3_client->getObject($data);
        
        // handle return 
        
        if ($this->encrypt) {
        	$content = $this->cryptor->decryptFile($tfn);
        	//print "content body decrypted from tempfile [" . $tfn . "]\n";
        	unlink($tfn);
        } else {
			// result is an S3 Guzzle object so we MUST cast it to string to get the contents!
        	$content = strval($result['Body']);         
        }	
        	
        if ($this->file_placeholder) {
        	file_put_contents($this->file_placeholder, $content);
        	//print "content body written to file placeholder [" . $this->file_placeholder . "]\n";
        	return $this->file_placeholder;
        } else {
			return $content;
        }
	}

	// ------------------------------------------------------------------
	// acquireToFile (HTTP GET)
	// ------------------------------------------------------------------	
	
	private function acquireToFile($object, $ofn) {

		// no encryption allowed with this method
		if ($this->encrypt) {
			throw new Exception('acquireToFile does not support encryption');
		}

		$obj_url = $this->resourceEndpoint('label', $object);
		//print "object: " . $object . " endpoint: " . $obj_url . "\n";		
		
		$data = array(
			'Bucket' => $this->s3_bucket, // Bucket is required
			'Key' => $object,    // Key is required
			'SaveAs' => $ofn			  // File location optional
		);
		
		try {
			$result = $this->s3_client->getObject($data);
		} catch (Exception $e) {
        	throw new Exception('S3 service failure: ' . $e->getMessage());
		}
        
		return $ofn;
	}

	// ------------------------------------------------------------------
	// acquireMetadata (HTTP GET)
	// ------------------------------------------------------------------	
	
	private function acquireMetadata($fn) {
		//$obj_url = $this->resourceEndpoint('label', $fn);
		//print "object: " . $object . " endpoint: " . $obj_url . "\n";		

		$data = array(
			'Bucket' => $this->s3_bucket, // Bucket is required
			'Prefix' => $fn               // Prefix is required
		);
		
		try {
			$result = $this->s3_client->listObjects($data);
		} catch (Exception $e) {
        	throw new Exception('S3 service failure: ' . $e->getMessage());
		}	
        	
        if ($this->file_placeholder) {
        	file_put_contents($this->file_placeholder, $result);
        	//print "metadata written to file placeholder [" . $this->file_placeholder . "]\n";
        	return $this->file_placeholder;
        }
        
		return $result;
	}
			
	// ------------------------------------------------------------------
	// post (HTTP POST)
	// ------------------------------------------------------------------
		
	private function post($content) {
		$tmpfile = null;
		
		// manage privacy
		if ($this->privacy) { $acl = 'bucket-owner-full-control'; }
		else { $acl = 'public-read'; } 	
		
		// determine content expiry			
		if (! $this->expiry) {
			$expiry = date(DateTime::ISO8601, (time() + $this->sunset));
		} else {
			$expiry = $this->expiry;
		}
		// content type & naming
		if ($content) {
			$typekey  = 'Body';
			if ($this->encrypt) {
				$enc_con  = $this->cryptor->encrypt($content);
				$name     = $hash = sha1($enc_con);
				$resource = $enc_con;
			} else { 
				$name     = $hash = sha1($content);
				$resource = $content;
			}
		} else {
			$typekey  = 'SourceFile';
			$name     = $this->parsePath($this->file_placeholder);
			// check for subfolder storage
			if ($this->folder) {
				$name = $this->folder . '/' . $name;
			}
			if ($this->encrypt) {
				$tmpfile = $this->cryptor->encryptFile($this->file_placeholder);
				//print "content encrypted into temporary file" . $tmpfile . "\n";
				$hash    = sha1(file_get_contents($tmpfile));
				$resource = $tmpfile;
			} else {
				$hash     = sha1(file_get_contents($this->file_placeholder));
				$resource = $this->file_placeholder;
			}
		}
		if ($this->is_vanity) { $name = $this->vanity_str; }
		
		$uuid = UUID::next();
		
		try {
			$out = $this->s3_client->putObject(array(
				'Bucket'   => $this->s3_bucket, 
				'Key'      => $name, 
				$typekey   => $resource,
				'ACL'      => $acl,
				'Expires'  => $expiry,
				'Metadata' => array('uuid'  => $uuid, 'digest' => $hash)
			));
			
			// wait for the object to actually exist
			$this->s3_client->waitUntil('ObjectExists', array(
			    	'Bucket' => $this->s3_bucket,
				    'Key'    => $name
			));

			// collate output
			$res = array(
				'status' => 'created',
				'url'    => $this->rewriteUrl($out['ObjectURL']),
				'long'   => substr($out['ETag'], 1, strlen($out['ETag'])-2),
				'uuid'   => $uuid,
				'sunset' => strtotime($expiry),
				'digest' => $hash,
				'label'  => $name
			);
		} catch (Exception $e) {
        	throw new Exception('S3 service failure: ' . $e->getMessage());
       	}
       	
       	// don't leave any artifacts, if created
       	if ($tmpfile && file_exists($tmpfile)) { 
       		unlink($tmpfile);
       	}
       	
       	return $res;
	}	

	// ------------------------------------------------------------------
	// delete (HTTP DELETE)
	// ------------------------------------------------------------------	
	
	private function delete($params) {
		$key = $params['label'];
		try {
			$result = $this->s3_client->deleteObject(array(
		    	'Bucket' => $this->s3_bucket, // Bucket is required
			    'Key'    => $key              // Key is required
			));
			return array('status' => 'deleted');
        } catch (Exception $e) {
        	throw new Exception('S3 service failure: ' . $e->getMessage());
        }
	}
	
	// ------------------------------------------------------------------
	// ping (HTTP GET)
	// ------------------------------------------------------------------	
	
	private function ping($label, $force = false, $returnbody = false) {
		try {
			if ($returnbody) {
				$result = $this->s3_client->getObject(array(
			    	'Bucket' => $this->s3_bucket, // Bucket is required
				    'Key'    => $label            // Key is required
				));
				return $result;
			} else {
				$result = $this->s3_client->headObject(array(
			    	'Bucket' => $this->s3_bucket, // Bucket is required
				    'Key'    => $label            // Key is required
				));
				$r = $result->toArray();
				$m = $r['Metadata'];
				$m['status'] = 'exists';
				$m['url']    = $this->service_endpoint . $this->s3_bucket . '/' . $label;
				$m['long']   = substr($r['ETag'], 1, strlen($r['ETag'])-2);
				$m['sunset'] = $this->mungeDate($r['Expires']);
				$m['label']  = $label;
				//print "PING yields:\n" . json_format(json_encode($m)) . "\n";
				return $m;
			}
        } catch (Exception $e) {
        	throw new Exception('S3 service failure: ' . $e->getMessage());
        }
		
	}

	// ------------------------------------------------------------------
	// poke - URL lookup using CURL
	// ------------------------------------------------------------------	
	
	private function poke($url, $follow = true, $returnbody = false) {	
		$curl = curl_init();
		
	    curl_setopt($curl, CURLOPT_URL, $url);	    
	    curl_setopt($curl, CURLOPT_TIMEOUT, $this->timeout);
   		curl_setopt($curl, CURLOPT_HEADER, false);	    
    	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $follow);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_VERBOSE, false);
		 
        $body = @curl_exec($curl);
        $info = curl_getinfo($curl);
        print 'http code: ' . $info['http_code'] . "\n";
        curl_close($curl);
        
        switch($info['http_code']) {
        	case '200':
        		//print "poke returned HTTP response code [" . $info['http_code'] . "]\n";
        		if ($returnbody) { return $body; }
        		else { return true; }
        	case '404':	// not found (URL not in use)
        	case '405':	// operation not allowed (mistake forming URL)
        	case '301': // moved permanently (URL does not use HTTPS)
        	case '307': // moved temporarily (? expired state, maybe)
        	default:
        		throw new Exception('poke returned with HTTP error code ' . $info['http_code']);
        }
	}
	
	// ------------------------------------------------------------------
	// createFolder - create a folder within a bucket
	// ------------------------------------------------------------------	
	
	private function privateCreateFolder($folder_name) {
		try {
			$res = $this->s3_client->PutObject(array(
				'Bucket' => $this->s3_bucket, 	// Bucket is required
				'Key'    => $folder_name . '/', // Key is required
				'Body'   => ''
			));
			$result = $res->toArray();
			if (! $result || !array_key_exists('ObjectURL', $result)) {
				print_r($result);
			} else {
				print "new object URL: " . $result['ObjectURL'] . "\n";
			}
					
			// wait for the object to actually exist
			$this->s3_client->waitUntil('ObjectExists', array(
			    	'Bucket' => $this->s3_bucket,
				    'Key'    => $folder_name . '/'
			));
	
        } catch (Exception $e) {
        	throw new Exception('S3 service failure: ' . $e->getMessage());
        }
        
        return true;
    }
    
	// ------------------------------------------------------------------
	// privateFolderExists - does a folder exist within a bucket
	// ------------------------------------------------------------------	
	
	private function privateFolderExists($folder_name) {
		try {
			$res = $this->s3_client->GetObject(array(
				'Bucket' => $this->s3_bucket, 	// Bucket is required
				'Key'    => $folder_name . '/' // Key is required
			));

			return true;	
        } catch (Exception $e) {
        	//print 'S3 service failure: ' . $e->getMessage() . "\n";
        	return false;
        }
        
        return false;
    }    
    
	// ----------------------------------------------------------------------
	// setCredentials
	// ----------------------------------------------------------------------
	
	private function setCredentials() {
		$s3_env_vars = array ("AWS_DEFAULT_REGION", "AWS_S3_BUCKET_NAME");
		if (count(array_intersect_key(array_flip($s3_env_vars), $_SERVER)) === count($s3_env_vars)) {
			$this->s3_bucket = $_SERVER["AWS_S3_BUCKET_NAME"];
			// the AWS SDK automatically pulls the credentials from the environment variables
			// https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_environment.html
			// so we only need to specify the region and version.
			$s3_config = array(
				'region'  => $_SERVER["AWS_DEFAULT_REGION"],
				'version' => "latest"
			);

			if (array_key_exists("AWS_PROFILE", $_SERVER)) {
				$s3_config["credentials" ]  = CredentialProvider::sso($_SERVER["AWS_PROFILE"]);
			}

			$this->s3_client = new S3Client($s3_config);
			return;
		}

		Utilities::logger('[' . $this->pb_type . '] acquiring fresh credentials', E_USER_NOTICE);
		// which account name?
		if ($this->service_acct == null) {
			if (file_exists(AnyNewsConfig::S3_ACCOUNT_NAME)) {
				$this->service_acct = trim(file_get_contents(AnyNewsConfig::S3_ACCOUNT_NAME));
				Utilities::logger('using S3 account [' . $this->service_acct . ']', E_USER_NOTICE);
			} else {
				throw new Exception('No S3 account name provided and file ' . AnyNewsConfig::S3_ACCOUNT_NAME . ' does not exist');
			}
		}
		
		// get my credentials from secure storage
		$co = new S3Credential();
		$credentials = $co->getCredentials($this->service_acct);
		
		// set my storage location
		$this->s3_bucket = $credentials['bucket'];

		// Instantiate the S3 client with  AWS credentials
		$this->s3_client = new S3Client(array(
    		'region'  => AnyNewsConfig::S3_REGION,
    		'version' => AnyNewsConfig::S3_VERSION,
    		'credentials' => array(
        		'key'    => $credentials['id'],
        		'secret' => $credentials['key'],
    		)
		));
	}

	// ----------------------------------------------------------------------
	// mungeDate - convert an AWS date to an ISO8601 date
	// ----------------------------------------------------------------------	
	
	private function mungeDate($s3date) {
		$dsec = strtotime($s3date);
		if (! $dsec) { return false; }
		
		$iso8601 = date(DateTime::ISO8601, $dsec);
		return $iso8601;
	}

	// ----------------------------------------------------------------------
	// parshPath - get filename out of full path
	// ----------------------------------------------------------------------	
	
	private function parsePath($filepath) {
		$parts = explode('/', $filepath);
		return $parts[count($parts)-1];
	}

	// ----------------------------------------------------------------------
	// rewriteUrl - change URL from domain-naming to path-naming
	// ----------------------------------------------------------------------	
	
	private function rewriteUrl($url) {
		$parts = parse_url($url);
		$fresh = $this->service_endpoint . $this->s3_bucket . $parts['path'];
		return $fresh;
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>
