<?php
/** 
 * ----------------------------------------------------------------------
 * component: ContentUploader (interface)
 * 
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
**/

namespace guardianproject\anynews;

interface ContentUploader {
	// setters
	public function setPrivacy($bool);
	public function setEncryption($bool);
	public function setFile($fn);		    // used for both paste() and retrieve()
	public function setBinary($bool);
	public function setSunset($secs);		// TTL for pasted content
	public function setServiceTimeout($secs);		
	
	// action methods
	public function paste($content = null);
	public function retrieve($url);	
	public function remove($uuid);
	
	// tests
	public function urlExists($url);
	public function isUnique($name, $force);
	public function hashExists($digest);
	public function fileHashExists($fn);
}