<?php

/**
 * ----------------------------------------------------------------------
 * Upload UNAGGREGATED news feeds to an AWS S3 bucket
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

require_once 'feed_aggregation.php';

function uploadDirectory($dry_run, $s3, $root, $main_folder, $cache_days, $private = true)
{

    $cache_secs = 60 * 60 * 24 * $cache_days;
    $sell_by_date = time() - $cache_secs;
    print "Uploading content newer than [$cache_days] days old (sell-by date: " . date('c', $sell_by_date) . ")\n";

    UtilitiesConfig::showLogging(false);

    // walk the directory structure uploading each file found, if it is not too old
    $dirs = subdirectories($root);
    if (!$dirs) {
        return false;
    }

    foreach ($dirs as $dir) {
        $the_dir = $root . '/' . $dir;
        $feeds_dir = $main_folder . '/' . $dir;

        $files = directoryContents($the_dir);

        foreach ($files as $fn) {
            $index_file = $the_dir . '/' . $fn;
            $index_name = $feeds_dir . '/' . $fn;
            if (!isOld($index_file, $sell_by_date)) {
                if (!$dry_run) {
                    uploadFile($s3, $index_file, $index_name, $private);
                } else {
                    print "index upload candidate: " . $index_name . "\n";
                }
            } else {
                print $index_file . " is too old to sync\n";
            }
        }

        $media_dir = $root . '/' . $dir . '/media';
        $files = directoryContents($media_dir);
        foreach ($files as $fn) {
            $media_file = $root . '/' . $dir . '/media/' . $fn;
            $media_name = $feeds_dir . '/media/' . $fn;
            if (!isOld($media_file, $sell_by_date)) {
                if (!$dry_run) {
                    uploadFile($s3, $media_file, $media_name, $private);
                } else {
                    print "media upload candidate: " . $media_name . "\n";
                }
            } else {
                print $media_file . " is too old to sync\n";
            }
        }
    }
}

// ----------------------------------------------------------------------
// Upload file to S3 (all bucket/acct data via S3Credential)
// ----------------------------------------------------------------------

function uploadFile($s3, $fpath, $name, $private = true, $bool = false)
{
    $s3->setVanityString($name);
    $s3->setServiceTimeout(100);
    $s3->setPrivacy($private);
    $s3->setEncryption(false);
    $s3->setBinary($bool);
    $s3->setFile($fpath);
    $result = $s3->paste();
    if (!array_key_exists('uuid', $result)) {
        print "failed: " . $result['status'] . "\n";
        return false;
    }

    print json_encode($result, JSON_PRETTY_PRINT) . "\n";
    return $result;
}
