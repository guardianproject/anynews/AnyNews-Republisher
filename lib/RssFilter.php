<?php
/**
 * ----------------------------------------------------------------------
 * component: RssFilter
 * parse RSS and ATOM feeds to extract components or reformat
 * (ATOM is converted to RSS internally)
 * adapted from: https://github.com/ozgrozer/rss-filter/blob/master/rss-filter.php
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\anynews; 

use guardianproject\proxyservices\utilities\URL as URL;
use \DOMDocument; 
use \Exception as Exception;

class RssFilter {
	private $url;

	// store attributes 
	private $channelAttributes;
	private $itemAttributes;
	private $item = 0;
	private $nitems = 0;
	
	// banned words list (items containing these words will not be processed)
	private $words;
	
	// will contain the processed feed
	private $processed;
	
	private $type;

	// ----------------------------------------------------------------------
	// CONSTRUCTOR
	// ----------------------------------------------------------------------
		
	public function __construct($location, $word_list = null, $url = null) {
		if(!$url) $this->url = new URL();
		else $this->url = $url;
		$this->itemAttributes = array();
		$this->words = array();
		
		if ($word_list) { $this->ban($word_list); }
		$this->init($location);  // parse source document
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// PUBLIC FUNCTIONS
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	public function setUrl($url) {
		$this->url = $url;
	}

	// ----------------------------------------------------------------------
	// MIME type of the acquired feed
	// NOTE: the feed resulting from the disgorge() method will ALWAYS BE RSS
	// ----------------------------------------------------------------------
		
	public function feedType() {
		switch ($this->type) {
			case 'rss':
				return 'application/xml+rss';
			case 'atom':
				return 'application/xml+atom';
			default:
				return null;
		} 
	}

	// ----------------------------------------------------------------------
	// channel information, item information iterator (both as JSON)
	// ----------------------------------------------------------------------

	public function numberOfItems() { return $this->nitems; }
		
	public function channelAttributes() { return json_encode($this->channelAttributes); }
	
	public function nextItemAttributes() { 
		$next = $this->item++;
		if ($next >= $this->nitems) {
			$this->item = 0;
			return null;
		}
		return json_encode($this->itemAttributes[$next]);
	}

	// ----------------------------------------------------------------------
	// return the parsed input feed as a properly-formatted RSS feed
	// ----------------------------------------------------------------------
	
	public function disgorgeRss() {
		if (! $this->feedType()) { return null; }
		
		$out = $this->formatChannel();

		for ($i = 0; $i < $this->nitems; $i++) {
			$out .= $this->formatItem($this->itemAttributes[$i]);
		}
		$out .= $this->formatEnd();
		
		return $out;
	}

	// ----------------------------------------------------------------------
	// don't return any articles that contain specific keywords
	// ----------------------------------------------------------------------

	public function ban($word_list) {
		$this->words = $word_list;
	}
			
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// PRIVATE FUNCTIONS
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// output formatters
	// ----------------------------------------------------------------------
			
	private function formatItem($content) {
		$formatted .= 
			"      " . '<item>' . "\n" .
			"         " . 	'<title>' . trim($content['title']) . '</title>' . "\n" .
			"         " . 	'<link>' . $content['link'] . '</link>' . "\n" .
			"         " . 	'<pubDate>' . trim($content['pubDate']) . '</pubDate>' . "\n";
		
		if (array_key_exists('enclosure', $content)) {
			$formatted .= "         " . 	'<enclosure url="' . $content['enclosure']['url'] . '" type="' . $content['enclosure']['type'] . '" length="' . $content['enclosure']['length'] . '" />' . "\n";
		}
		
		$formatted .= "         " . '<description>' . $content['description'] .
			"         " . '</description>' . "\n";
		if (array_key_exists('c_encoded', $content)) {
			$formatted .= "         <content:encoded>" . $content['c_encoded'] .
				"\n         " . '</content:encoded>' . "\n";
		}

		$formatted .= 	"      " . '</item>' . "\n";		
		return $formatted;
	}
	
	private function formatChannel() {
		$channel = $this->channelAttributes;
		
		if ((array_key_exists('categories', $channel)) && (count($channel['categories']) > 0)) {
			$cats = '';
			foreach ($channel['categories'] as $the_cat) {
				$cats .= '      <category>' . $the_cat . '</category>' . "\n"; 
			}
		}
		$result = '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
			'<rss version="2.0">' . "\n   " . 
			'<channel>' . "\n      " .
		  		'<title>' . htmlspecialchars($channel['title']) . '</title>' . "\n      " .
		  		'<link>' . htmlspecialchars($channel['link']) . '</link>' . "\n" . $cats;
		
		if ($channel['image']) {
		  	$result .= '      <image><url>' . $channel['image'][0] . '</url><width>' . 
		  		$channel['image'][1] . '</width><height>' . $channel['image'][2] . 
		  		'</height></image>' . "\n";
		 }
		 
		 $result .= '      <description>' . htmlspecialchars($channel['description']) . '</description>' . "\n";

		return $result;	
	}
	
	private function formatEnd() {
		return "   </channel>" . "\n" . "</rss>\n";
	}
	
	// ----------------------------------------------------------------------
	// parser
	// ----------------------------------------------------------------------
	
	private function init($source) {
		/**
		if (substr($source, 0, 4) == 'http') {
			$res = $this->url->get($source, true, true);
			print 'code: ' . $res['http_code'] . 'body: ' . strlen($res['body']) . "\n";
			if ($res['http_code'] == '200') {
				$destination = tempnam(sys_get_temp_dir(), 'rss_download_');
				file_put_contents($destination, $res['body']);
				$source = $destination;
			} else {
				print "source: " . $source . " not found\n";
				return false;
			}
		}
		**/
		$words = $this->words;
		$banned = 0;
		
		$xmlDoc = new DOMDocument();
		// $source = trim(file_get_contents($source));	// local file or URL

		$res = $this->url->get($source);
		if ($res['http_code'] != 200)
			throw new Exception("Failed to parse source $source");
		$source = trim($res["body"]);
		$xmlDoc->loadXML($source);


		// figure out out document type
		if ($xmlDoc->getElementsByTagName('feed')->length) {
			$this->type = 'atom';
		} elseif ($xmlDoc->getElementsByTagName('rss')->length) {
			$this->type = 'rss';
		} else {
			$this->type = '';
		}

		// parse the feed into objects
		if ($this->type === 'atom') {
			//
			// ATOM
			//
			$rssTitle = $this->getAttribute($xmlDoc->getElementsByTagName('title'));
			$rssLink = $this->getAttribute($xmlDoc->getElementsByTagName('link'), 'href');
			$rssPubDate = $this->getAttribute($xmlDoc->getElementsByTagName('updated'));
			$rssManagingEditor = $this->getAttribute($xmlDoc->getElementsByTagName('author'), 'email');
			$rssGenerator = $this->getAttribute($xmlDoc->getElementsByTagName('generator'));
			$image = $this->getAttribute($xmlDoc->getElementsByTagName('logo'));
			if (! $image) {
				 $image = $this->getAttribute($xmlDoc->getElementsByTagName('icon'));
				 if ($image) {
				 	$rssImage = array($image, null, null);				
				 }
			}
			$categories = $xmlDoc->getElementsByTagName('category');
			if ($categories) {
				$rssCategories = array();
				$len = $xmlDoc->getElementsByTagName('category')->length;
				$rssCategories = array();
				for ($i = 0; $i < $len; $i++) {
					$cat = $xmlDoc->getElementsByTagName('category')->item($i)->nodeValue;
					if (!in_array($cat, $rssCategories)) { $rssCategories[] = $cat; }
				}
			}
			
			$rssWebMaster = '';
			$rssDescription = '';
			$items = $xmlDoc->getElementsByTagName('entry');
			$countItems = ($xmlDoc->getElementsByTagName('entry')->length);
			
			for ($i = 0; $i < $countItems; $i++) {
				$enclosure = null;
				$item = $items->item($i);
				$title = htmlspecialchars($this->getAttribute($item->getElementsByTagName('title')));
				$link = htmlspecialchars($this->getAttribute($item->getElementsByTagName('link'), 'href'));
				$description = htmlspecialchars($this->getAttribute($item->getElementsByTagName('content')));
				$pubDate = $this->getAttribute($item->getElementsByTagName('published'));
				$updated = $this->getAttribute($item->getElementsByTagName('updated'));
				$published = $pubDate ? $pubDate : $updated;
				
				// special!			
				$all = $item->getElementsByTagNameNS('http://search.yahoo.com/mrss/', '*');
				foreach ($all as $one) {
					if ($one->tagName == 'media:content') {
						$a = $one->attributes;
						$il = $a->getNamedItem('url')->nodeValue;
						// try to acquire it to get attributes (and assure it's valid!)
						$res = $this->url->ping($il, true, true);
						if ($res['http_code'] == '200') {
							$enclosure = array(
								'url' => $il,
								'type' => $res['headers']['content-type'],	
								'length' => $res['headers']['content-length']
							);				
						}
					}
				}
				
			 	$attrs = array(
					'title' => $title,
					'link' => $link, 
					'pubDate' => $pubDate,
					'description' => $description
				);
				if ($enclosure) { $attrs['enclosure'] = $enclosure; }

				// if no banned words, save it
				if (!$this->stristrArray($title, $this->words) && 
					!($this->stristrArray($description, $this->words))) {
					$this->itemAttributes[] = $attrs;
				} else {
					$countItems--;
					$banned++;
				}			 
			}
		} elseif ($this->type === 'rss') {
			//
			// RSS
			//
			$channel = $xmlDoc->getElementsByTagName('channel')->item(0);
			$rssTitle = $this->getAttribute($channel->getElementsByTagName('title'));
			$rssLanguage = $this->getAttribute($channel->getElementsByTagName('language'));
			$rssLink = $this->getAttribute($channel->getElementsByTagName('link'));
			$rssDescription = $this->getAttribute($channel->getElementsByTagName('description'));
			$rssPubDate = $this->getAttribute($channel->getElementsByTagName('pubDate'));
			$rssWebMaster = $this->getAttribute($channel->getElementsByTagName('webMaster'));
			$rssManagingEditor = $this->getAttribute($channel->getElementsByTagName('managingEditor'));
			$rssGenerator = $this->getAttribute($channel->getElementsByTagName('generator'));
			$categories = $this->getAttribute($channel->getElementsByTagName('category'));
			if ($categories) {
				$len = $channel->getElementsByTagName('category')->length;
				$rssCategories = array();
				for ($i = 0; $i < $len; $i++) {
					$cat = $channel->getElementsByTagName('category')->item($i)->nodeValue;
					if (!in_array($cat, $rssCategories)) { $rssCategories[] = $cat; }
				}
			}
			$img = $channel->getElementsByTagName('image')->item(0);
			if ($img) {
				$i_url    = $img->getElementsByTagName('url')->item(0)->nodeValue;
				$i_width  = $img->getElementsByTagName('width')->item(0)->nodeValue;
				$i_height = $img->getElementsByTagName('height')->item(0)->nodeValue;
				$rssImage = array($i_url, $i_width, $i_height);
			} else {
				$imedia = $channel->getElementsByTagNameNS('http://www.itunes.com/dtds/podcast-1.0.dtd', '*');	
				if ($imedia) {
					foreach ($imedia as $test) {
						if ($test->tagName == 'itunes:image') {
							$in = $test->nodeValue;
							$rssImage = array($in, null, null);
						}
					}
				}	
			}
			$items = $xmlDoc->getElementsByTagName('item');
			$countItems = ($xmlDoc->getElementsByTagName('item')->length);
			
		 	for ($i = 0; $i < $countItems; $i++) {
				$item = $items->item($i);
				$title = htmlspecialchars($this->getAttribute($item->getElementsByTagName('title')));
				$link = htmlspecialchars($this->getAttribute($item->getElementsByTagName('link')));
				$pubDate = $this->getAttribute($item->getElementsByTagName('pubDate'));
				$description = htmlspecialchars($this->getAttribute($item->getElementsByTagName('description')));
				$cenc = $item->getElementsByTagNameNS('http://purl.org/rss/1.0/modules/content/', '*')->item(0)->nodeValue;		

				$enclosure = ['url' => '', 'type' => '', 'length' => ''];
			  	$imageAttributes = $item->getElementsByTagName('enclosure')[0]->attributes;
				if ($imageAttributes) {
			  		foreach ($imageAttributes as $key => $imageAttribute) {
			   			$enclosure[$imageAttribute->nodeName] = htmlspecialchars($imageAttribute->nodeValue);
			  		}
				}
				
				$attrs = array(
			   		'title' => $title,
			    	'link' => $link, 
			    	'pubDate' => $pubDate,
			    	'description' => $description
				);
				
				if ($enclosure) { $attrs['enclosure'] = $enclosure; }
				if ($cenc) { $attrs['c_encoded'] = $cenc; }
							
				// ban 				
				if (!$this->stristrArray($title, $this->words) && 
					!($this->stristrArray($description, $this->words))) {
			  	 	$this->itemAttributes[] = $attrs;
			  	} else {
			  		$countItems--;
					$banned++;
				}
			}
		}
		
		$this->channelAttributes = array(
			'content_type' => $this->feedType($this->type), 
			'language' => $rssLanguage,
			'title' => $rssTitle,
			'link' => $rssLink,
		  	'webmaster' => $rssWebMaster,
	    	'editor' => $rssManagingEditor,
	    	'generator' => $rssGenerator,
			'item_count' => $countItems,
			'pubdate' => $rssPubDate,
			'image' => $rssImage,	
			'banned_items' => $banned,		    				
			'description' => $rssDescription
		);
		if ($rssCategories) { $this->channelAttributes['categories'] = $rssCategories; }
		$this->nitems = $countItems;

		return true;
	}

	// ----------------------------------------------------------------------
	// helpers
	// ----------------------------------------------------------------------
	
	private function stristrArray($haystack, $needle) {
		if (!is_array($needle)) { $needle = array($needle); }
		
		foreach ($needle as $searchstring) {
			$found = stristr($haystack, $searchstring);
			if ($found) {
				return $found;
			}
		}
		return false;
	}
	
	private function getAttribute($string, $attribute = '') {
		if ($string->length === 0) {
			$result = '';
		} else {
			if ($attribute) {
				$result = $string->item(0)->getAttribute($attribute);
			} else {
				if ($string->item(0)->childNodes->item(1)) {
			    	$result = $string->item(0)->childNodes->item(1)->nodeValue;
			  	} elseif ($string->item(0)->childNodes->item(0)) {
			    	$result = $string->item(0)->childNodes->item(0)->nodeValue;
			  	} else {
			    	$result = '';
			  	}
			}
		}
		return $result;
	}
		
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}