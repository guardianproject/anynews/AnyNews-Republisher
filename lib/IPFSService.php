<?php
/** 
 * ----------------------------------------------------------------------
 * component: IPFSService (access to the InterPlanetary File System)
 * Conforms to (a very small part of) the V0 IPFS HTTP API.
 * Docs: https://docs.ipfs.io/reference/http/api/
 *
 * Example:
 *	$ipfs = new IPFSService();
 *
 *  $input_file = "my_content.txt";
 *	$ipfs->setFilePath($input_file); // send contents of this file
 *	$hash = $ipfs->add();
 *
 *  $output_file = "new_place.txt";
 *  $ipfs->setFilePath($output_file); // retrieve into this file
 *  $ipfs->get($hash);
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
**/

namespace guardianproject\anynews;

class IPFSService {

	// standard config when running the Go IPFS daemon is running locally
	private $ipfsHostIP      = "localhost";
	private $ipfsHostPort    = "8080";
	private $ipfsHostApiPort = "5001";
	
	private $ipfsAPIPath     = "/api";
	private $ipfsProtocolVersion = "v0";
	private $args;
	private $flags;
	private $filePath;
	
	private $error_codes = array(
		"0"   => "Request timed-out",
		"200" => "The request was processed or is being processed (streaming)",
		"500" => "RPC endpoint returned an error",
		"400" => "Malformed RPC, argument type error, etc",
		"403" => "RPC call forbidden",
		"404" => "RPC endpoint doesn't exist",
		"405" => "HTTP Method Not Allowed"
	);
	private $supported_commands = array(
		// admin type commands
		"version"      => "version",
		"commands"     => "commands",
		"ping"         => "ping",
		"resolve"      => "resolve",
		
		// IPNS commands
		"dns_resolve"  => "dns",			// DNS domains to IPNS names
		"ipns_resolve" => "name/resolve",	// IPNS names to IPFS CIDs
		"ipns_publish" => "name/publish",	// link IPFS CID to IPNS name
			
		// action type commands		
		"add"  => "add",
		"get"  => "cat",  // "get" returns the "file object"; "cat" just the contents
		"put"  => "put",

		// sub-action "pin"		
		"pin"        => "pin/add",
		"unpin"      => "pin/remove",
		"update_pin" => "pin/update",
		"pin_list"   => "pin/ls",
		"pin_verify" => "pin/verify"
	);
	
	// ----------------------------------------------------------------------
	// constructor
	// ----------------------------------------------------------------------	

	function __construct($ip = null, $port = null, $apiPort = null) {
		if ($ip) { $this->ipfsHostIP = $ip; }
		if ($port) { $this->ipfsHostPort = $port; }
		if ($apiPort) { $this->ipfsHostApiPort = $apiPort; }
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// setters
	// ----------------------------------------------------------------------

	public function setFilePath($path) { $this->filePath = $path; }
	public function setIpfsHostIP($ip) { $this->ipfsHostIP = $ip; }
	public function setIpfsHostPort($port) { $this->ipfsHostPort = $port; }
	public function setIpfsHostApiPort($apip) { $this->ipfsHostApiPort = $apip; }

	// ----------------------------------------------------------------------
	// find - resolve an IPFS name to an IPFS hash
	// ----------------------------------------------------------------------

	public function find($hash, $recurse = true) { 
		$args = array('arg' => $hash);
		if ($recurse) { $args['recursive'] = 'true'; }
		else { $args['recursive'] = 'false'; }
		
		$data = $this->executeCommand('ipns_resolve', $args);
		if ($data && array_key_exists('Path', $data)) {
			return $data['Path'];
		}
		
		return null;
	}

	// ----------------------------------------------------------------------
	// publish - link an IPFS hash to an IPNS name 
	// ----------------------------------------------------------------------

	public function publish($hash) {
		if (substr($hash, 0, 3) != '/ip') {
			$hash = '/ipfs/' . $hash;
		} 		
		$args = array(
			'arg' => $hash,
			'lifetime' => '48h',
			'ttl' => '48h',
			'key' => 'self'	
		);
		
		return $this->executeCommand('ipns_publish', $args);
	}

	// ----------------------------------------------------------------------
	// add (the content of) a file to IPFS, and return its hash
	// ----------------------------------------------------------------------

	public function add() {
		if (! $this->filePath) {
			throw new Exception('input file path not defined');
		}
		
		$url  = $this->createBaseURL('add'); 
		$response = $this->dataTransfer('add', $url);

		return $response['Hash'];
	}

	// ----------------------------------------------------------------------
	// get raw content from IPFS (via its hash) and return it to a file
	// ----------------------------------------------------------------------

	public function get($hash) {
		if (! $this->filePath) {
			throw new Exception('input file path not defined');
		}
			
		$url = $this->createBaseURL('get'); 
		$args = '?arg=' . $hash;
		$url  .= $args;
		$response = $this->dataTransfer('get', $url);
		print_r($response);
		if ($response) {
			return true;
		}
		
		return false;
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// executeCommand - used with all API verbs that return metadata
	// ----------------------------------------------------------------------

	public function executeCommand($cmd, $arg_array = null) {
		if (! array_key_exists($cmd, $this->supported_commands)) {
			throw new Exception('[' . $cmd . '] is not a supported command');
		}
		
		// build URL
		
		$url = $this->createBaseURL($cmd);
		
		if ($arg_array && count($arg_array) > 0) {
			$url .= '?';
			foreach ($arg_array as $key => $val) {
				$url .= $key . '=' . $val . '&';
			}
			$url = substr($url, 0, strlen($url) - 1);
		}

		$c_handle = curl_init();

		curl_setopt($c_handle, CURLOPT_URL, $url);
		curl_setopt($c_handle, CURLOPT_HEADER, false);
		curl_setopt($c_handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c_handle, CURLOPT_BINARYTRANSFER, false);
		curl_setopt($c_handle, CURLOPT_POST, true);
		curl_setopt($c_handle, CURLOPT_TIMEOUT, 60); // to account for long response times!

		$output = curl_exec($c_handle);
		$info = curl_getinfo($c_handle);
		if ($info['http_code'] != '200') {
			print "FAILED [" . $info['http_code'] . "]: " . $this->error_codes[$info['http_code']] . "\n";
			$res = null;
		} else {
			$res = json_decode($output, true);
		}		 
		curl_close($c_handle);
 
 		// output == associative array representing JSON
		return $res;
	}
	
	// ----------------------------------------------------------------------
	// dataTransfer - used with verbs that send/return real data
	// ----------------------------------------------------------------------

	private function dataTransfer($cmd, $url) {
		if (! array_key_exists($cmd, $this->supported_commands)) {
			throw new Exception('[' . $cmd . '] is not a supported command');
		}

		$c_handle = curl_init();

		curl_setopt($c_handle, CURLOPT_URL, $url);
		curl_setopt($c_handle, CURLOPT_HEADER, false);
		curl_setopt($c_handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($c_handle, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($c_handle, CURLOPT_POST, true);
		curl_setopt($c_handle, CURLOPT_TIMEOUT, 5);

		if ($cmd == 'add') {
			$basename = basename($this->filePath);
			if (! realpath($this->filePath)) { $fullpath = './' . $basename; }
			else { $fullpath = realpath($this->filePath); }
			$cfile = curl_file_create($fullpath, 'application/octet-stream', $basename);
	    	$postFields = ['file' => $cfile];
	        curl_setopt($c_handle, CURLOPT_POSTFIELDS, $postFields);		
		} else {
			$fp = fopen($this->filePath , 'w+');
			curl_setopt($c_handle, CURLOPT_FILE, $fp);
		}
		
		$output = curl_exec($c_handle);
		$info = curl_getinfo($c_handle);
		if ($info['http_code'] != '200') {
			//print "FAILED [" . $info['http_code'] . "]:" . $this->error_codes[$info['http_code']] . "\n";
			$res = null;
		} else {
			$res = json_decode($output, true);
		}		 
		curl_close($c_handle);
		if ($cmd != 'add') { fclose($fp); }
 
		// output == associative array representing JSON
		return $res;
	}

	// ----------------------------------------------------------------------
	// createBaseURL
	// ----------------------------------------------------------------------	

	private function createBaseURL($cmd) {
		if (! array_key_exists($cmd, $this->supported_commands)) {
			throw new Exception('[' . $cmd . '] is not a supported command');
		}
		$url = "http://" . $this->ipfsHostIP . ":" . $this->ipfsHostApiPort;
		$url .=  $this->ipfsAPIPath . '/' . $this->ipfsProtocolVersion . '/';
		$url .= $this->supported_commands[$cmd];

		return $url;
	}

/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
