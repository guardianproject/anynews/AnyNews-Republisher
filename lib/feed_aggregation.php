<?php

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\Utilities;
use guardianproject\proxyservices\utilities\MIMEType;
use guardianproject\proxyservices\utilities\PageParser;
use guardianproject\anynews\RssFilter;

const NS_ITUNES = "http://www.itunes.com/dtds/podcast-1.0.dtd";

function runAggregation($root, $http, $cleanup, $oldest_item, $out_of_date_time, $feeds)
{
    Utilities::logger("news aggregator starting for " . count($feeds) . " feeds", E_NOTICE);
    foreach ($feeds as $name => $feed_descriptor) {
        //$name = $feed_descriptor['name'];
        print "$feed_descriptor \n\n";
        $home = $feed_descriptor['home_url'];
        $link  = $feed_descriptor['feed_url'];
        $default_img  = $feed_descriptor['feed_img'];

        // Make sure we have an appropriate directory tree
        if (!prepareToAccept($root, $name)) {
            continue;
        }

        // Validate the feed exists and is in a reputable format
        $res = $http->get($link, true, true);
        if ($res['http_code'] != 200) {
            print "---\nFeed at link $link not found [http code " . $res['http_code'] . "]\n---\n";
            continue;
        }
        if (isValidFeedType($res['content_type']) == null) {
            print "---\nFeed for $name presents unknown content type " . $res['content_type'] .  "\n---\n";
            continue;
        }

        print "->\n-> [" . $name . "] starting $link\n->\n";
        // though my feed type is valid, I can only process RSS; so, 
        // convert ATOM to RSS if necessary
        $rssf = new RssFilter($link, null, $http);
        if ($rssf->feedType() == 'application/xml+atom') {
            Utilities::logger("feed for [" . $name . "][" . $link . "] converted from ATOM to RSS", E_USER_NOTICE);
            $xml = $rssf->disgorgeRss();
        } else {
            $xml = $res['body'];
        }

        $cdir = $root . '/' . $name;

        // Make sure we have an image to represent the channel and its items in app listing.
        // Save it locally and convert internal references to this new, local, file
        print "-> [" . $name . "] checking channel image\n";
        $xml = modifyChannelImage($http, $cdir, $xml, $default_img);

        // Check for <enclosures> in a feed's items (as with podcasts).  Similar to above, 
        // save those files locally and convert internal feed references to this new, local, filename
        print "-> [" . $name . "] acquiring channel media\n";
        $xml = AcquireItemMedia($http, $cdir, $xml, $oldest_item);

        // See if we can find imagery in the article's original HTML page to add to the
        // feed's article text.  Similar to above, save such files locally and convert 
        // internal feed references to this new, local, filename
        print "-> [" . $name . "] acquiring channel imagery\n";
        $xml = AcquireItemImagery($http, $cdir, $xml, $oldest_item);

        // As above, find imagery in each article's (<item>'s) included content,
        // either in the <description> or in <content:encoded>.  Similar to above, 
        // save such files locally and convert internal feed references to this new, 
        // local, filename
        print "-> [" . $name . "] acquiring item content imagery\n";
        $xml = AcquireContentImagery($cdir, $xml, $oldest_item);

        saveFeed($root, $name, $xml);
        print "-> [" . $name . "] completed\n";
        Utilities::logger("aggregated [" . $name . "]", E_NOTICE);
    }

    // Now that we've acquired the up-to-date information (and, incidentally, changed the 
    // timestamp on existing files we didn't need to acquire again), we can remove auxiliary
    // files (media, imagery) that are out of date.

    if ($cleanup) {
        print "-> cleaning up old media files\n";
        Utilities::logger('Cleaning up old media files', E_NOTICE);
        if (!cleanUp($root, $out_of_date_time)) {
            print "Apologies. That didn't end well.\n";
        } else {
            Utilities::logger(count($feeds) . " feeds aggregated to local storage", E_NOTICE);
            print "Lovely. We're done.\n";
        }
    }
}

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
// AUXILIARY FUNCTIONS
// ----------------------------------------------------------------------
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// Make sure it's really a feed
// ----------------------------------------------------------------------

function isValidFeedType($ct) {
	list($rct, $charset) = explode(';', $ct);
	$canonical_types = array(
        //'rdf'   => 'application/rdf+xml',
        //'rdf2'  => 'application/xml+rdf',
        'rss'   => 'application/xml+rss',
        'rss2'  => 'application/rss+xml',        
        'atom'  => 'application/xml+atom',
        'atom2' => 'application/atom+xml',
        'xml'   => 'text/xml',
        'xml2'  => 'application/xml'
    );

	foreach ($canonical_types as $short => $mime) {
		if ($rct == $mime) { return $mime; }
	}
	
	return null;
}

// ----------------------------------------------------------------------
// Build directory hierarchy for this feed
// ----------------------------------------------------------------------

function prepareToAccept($root, $name) {
	if (! file_exists($root)) { 
		if (! mkdir($root)) {
			print "ERROR: unable to make root directory " . $root . "\n";
			return false;
		}
	}
	if (! file_exists($root . '/' .  $name)) { mkdir($root . '/' .  $name); }
	else if (! is_dir($root . '/' .  $name)) {
		print "ERROR: " . $root . '/' .  $name . " is not a directory\n";
		return false;
	}
	if (! file_exists($root . '/' .  $name . '/media')) { mkdir($root . '/' .  $name . '/media'); }
	return true;
}

// ----------------------------------------------------------------------
// Save (already acquired and properly manhandled) feed to directory hierarchy
// ----------------------------------------------------------------------

function saveFeed($root, $name, $xml) {
	file_put_contents($root . '/' . $name . '/index.xml', $xml);
}

// ----------------------------------------------------------------------
// Save media to feed's directory hierarchy
// ----------------------------------------------------------------------

function saveMedia($http, $root, $media_url) {
	global $ffm;
	
	try {
		$mime = new MIMEType();
		$lfn = hash('sha1', $media_url);	// this'll be unique to each URL
		$res = $http->ping($media_url, true, false);
		list ($core_type, $detail_type) = explode('/', $res['content_type']);
		list ($conv_name, $charset) = explode(';', $detail_type);
		$fext = $mime->getFileExtensionForType($core_type . '/' . $conv_name);
	} catch (Exception $e) {
		print 'URL error ' . $e->getMessage() . "\n";
		return null;
	}
	
	// Check if the necessary file already exists.  If so, don't re-download it, 
	// but DO update its timestamp to the current time so it won't be reclaimed
	// on cleanup 
	$destination_path = $root . '/media/' . $lfn . '.' . $fext;
	if (file_exists($destination_path)) {
		print "media file: " . $destination_path . " already exists\n";
		touch($destination_path);
		return $destination_path;
	}

	print "media at $media_url is type [" . $core_type . "][" . $conv_name . "]\n";
	switch($core_type) {
		case 'image':
			if (! compress_image($media_url, $destination_path, 1)) {
 				return null;
 			}		
			break;
		case 'audio':
		case 'video':
			if (! $ffm) {
				$res = $http->get($media_url, true, false, $destination_path);
				if ($res['http_code'] != 200) { return null; }
			} else {
				if (! compress_audio($media_url, $destination_path, 1)) {
 					return null;
 				}
 			}
			break;
		default:
			Utilities::logger("Unknown media file type [" . $core_type . "] at " . $media_url, E_NOTICE);
			return null;
	}

	//print "media saved\n";
	return $destination_path;
}

// ----------------------------------------------------------------------
// Make sure there's a locally-resolvable image to use for the channel
// ----------------------------------------------------------------------

function modifyChannelImage($http, $root, $xml, $default_img) {
	$doc = new DOMDocument();
	@$doc->loadXML($xml);

	$ch = $doc->getElementsByTagName('channel')->item(0);
	if (! $ch) { 
		print "ERROR: no <channel> element in feed; wrong feed format likely" . "\n";
		return null;
	}	
	$c_title = $ch->getElementsByTagName('title')->item(0)->nodeValue;
	$c_link  = $ch->getElementsByTagName('url')->item(0)->nodeValue;

	// check for <image>
	$ch_img = $ch->getElementsByTagName('image')->item(0);
	if ($ch_img) {
		// <channel> defines an image and other metadata we can copy
		$img = new DOMDocument();
		@$img->loadXML($doc->saveXML($ch_img));
		$media  = $img->getElementsByTagName('url')->item(0)->nodeValue;
		$title  = $img->getElementsByTagName('title')->item(0)->nodeValue;
		$link   = $img->getElementsByTagName('link')->item(0)->nodeValue;
		$width  = $img->getElementsByTagName('width')->item(0)->nodeValue;
		$height = $img->getElementsByTagName('height')->item(0)->nodeValue;
		
		print "using <channel> image: " .  $media . "\n";
		$old = $ch_img; 
	}

	if (empty($media)) {
		$tagName = "image";
		$directChildren = [];
		foreach ($ch->childNodes as $child) {
			if ($child->nodeType === XML_ELEMENT_NODE &&
				$child->localName === $tagName &&
				$child->namespaceURI === NS_ITUNES)
			{
				$directChildren[] = $child;
			}
		}
		if (!empty($directChildren)) {
			$firstEl = $directChildren[0];
			$media = $firstEl->getAttribute("href");
			$title = $c_title;
			$link = $c_link;
			print "using <channel> itunes:image: " .  $media . "\n";
		}
	}

	if (empty($media)) {
		if(empty($default_image)) {
			Utilities::logger("Channel image cannot be parsed, and no default provided.");
			return $doc->saveXML();
		}
		// <channel> does not define an image, use default image instead
		// along with other data provided by <channel> 
		$media = $default_img;
		print "using default image: " .  $media . "\n"; 
		$title = $c_title;
		$link = $c_link;
	}
	
	// Acquire the image; store locally with unique name
	$name = saveMedia($http, $root, $media);
	if (! $name) {
		print "ERROR: unable to download image from " . $media . "\n";
		// remove <channel><image> so clients don't make same mistake  
		if ($ch_img) { 
			$doc->removeChild($ch_img);
		}
	} else {
		// Update the XML properly
		// Parse out the unique name
		$components = array_reverse(explode('/', $name));
		$img_link = './media/' . $components[0];
		
		// If the channel had an <image> element, remove it
		if ($ch_img) { $ch->removeChild($ch_img); }
	
		// fabricate fresh <image> element
		$in = $doc->createElement("image");

		// stuff it with content		
		$u = $doc->createElement("url");
		$u->nodeValue = $img_link;
		$in->appendChild($u);
		
		$t = $doc->createElement("title");
		$t->nodeValue = $title;
		$in->appendChild($t);
		
		$l = $doc->createElement("link");
		$l->nodeValue = $link;
		$in->appendChild($l);
		
		list($width, $height, $xtype, $xattr) = getTheImageSize($name);
		if ($width != null) {
			$w = $doc->createElement("width");
			$w->nodeValue = $width;
			$in->appendChild($w);
			
			$h = $doc->createElement("height");
			$h->nodeValue = $height;
			$in->appendChild($h);
		}
		
		// add it to the channel
		$where = findChild($ch, 'link');
		$ch->insertBefore($in, $ch->childNodes[$where]);
	}

	return $doc->saveXML();
}

// ----------------------------------------------------------------------
// Grab any media associated with each feed item as defined in <enclosure>
// Along the way, discard any items that are beyond their sell-by date
// ----------------------------------------------------------------------

function AcquireItemMedia($http, $root, $xml, $oldest_sec) {
	$doc = new DOMDocument();
	@$doc->loadXML($xml);

	$ch = $doc->getElementsByTagName('channel')->item(0);
	if (! $ch) { 
		print "ERROR: no <channel> element in feed; wrong feed format likely" . "\n";
		return null;
	}	

	// initialize array of <item> publication times/dates
	$item_list = $ch->getElementsByTagName('item');
	$seen = array();
	foreach($item_list as $node) {
		$epi = $node->getElementsbyTagName('pubDate')->item(0)->nodeValue;
		$seen[$epi] = 0;
	}

	$i = 0;
	foreach ($item_list as $the_item) {
		if (! $the_item) {
			print "ERROR inspecting item $i\n";
			continue;
		}
		$i++;
		
		$pd = $the_item->getElementsbyTagName('pubDate')->item(0)->nodeValue;
		$seen[$pd] = 1;	
		
		// first test if this item is TOO OLD by our standards.  If so, drop it
		if (tooOld($the_item, $oldest_sec)) {
			$seen[$pd] = "to be removed";
			continue;
		}

		// get the <enclosure> & its individual attributes
		$enc = $the_item->getElementsByTagName('enclosure')->item(0);
		if (! $enc) { 
			$enc = $the_item->getElementsByTagName('media.content')->item(0);
			if (! $enc) {
				continue;
			} 
		} 

		$enc_a  = $enc->attributes;
		$url    = $enc_a->getNamedItem('url')->nodeValue;
		$length = $enc_a->getNamedItem('length')->nodeValue;
		$type   = $enc_a->getNamedItem('type')->nodeValue;

		// Acquire the media; store locally with unique name
		// NOTE: Since local file name is unique to the media's URL (which is unique),
		// we can check if the file already exists and not incur the download penalty 
		// again.
		$name = saveMedia($http, $root, $url);

		// remove the possibly-to-be-replaced <enclosure> element
		$the_item->removeChild($enc);
		
		if (! $name) {
			print "ERROR: unable to download media from " . $url . "\n";
		} else {
			// Update the XML properly
			// Parse out the unique name
			$components = array_reverse(explode('/', $name));
			$media_link = './media/' . $components[0];
		
			// fabricate fresh <enclosure> element
			$mn = $doc->createElement("enclosure");
			$mn->setAttribute('url', $media_link);
			$mn->setAttribute('type', $type);
			$mn->setAttribute('length', $length);
					
			// add it to the <item>
			$where = findChild($the_item, 'description');
			$the_item->insertBefore($mn, $the_item->childNodes[$where]);
		}
		// Check for itunes:image tags alongside the enclosure
		$images = $the_item->getElementsByTagNameNS(NS_ITUNES, "image");
		if ($images->length > 0) {
			$tag = $images->item(0);
			$href = $tag->getAttribute("href");

			$name = saveMedia($http, $root, $href);
			if (! $name) {
				print "ERROR: unable to download itunes:image from " . $href . "\n";
			} else {
				$the_item->removeChild($tag);

				// Update the <item>'s XML
				$components = array_reverse(explode('/', $name));
				$local_media_href = './media/' . $components[0];

				// fabricate fresh <enclosure> element
				$mn = $doc->createElement("itunes:image");
				$mn->setAttribute("href", $local_media_href);

				// add it to the <item>
				$where = findChild($the_item, 'description');
				$the_item->insertBefore($mn, $the_item->childNodes[$where]);
			}
		}
	}

	// remove <item>s classified as "too old"	
	foreach ($seen as $name => $value) {
		if ($value == "to be removed") {
			$to_delete = findByPubDate($doc, $name, $seen);
			if ($to_delete != null) {
				if (! $ch->removeChild($to_delete)) { print "ERROR: could not remove node with pubdate $name\n"; }
			} 
		}
	} 

	return $doc->saveXML();
}

// ----------------------------------------------------------------------
// See if we can find an image to associate with each feed item.
// Along the way, discard any items that are beyond their sell-by date
// ----------------------------------------------------------------------

function AcquireItemImagery($http, $root, $xml, $oldest_sec) {
	$doc = new DOMDocument();
	@$doc->loadXML($xml);
	
	try {
		$pp = new PageParser();
		$http = new URL();
	} catch (Exception $e) {
	}

	$ch = $doc->getElementsByTagName('channel')->item(0);
	if (! $ch) { 
		print "ERROR: no <channel> element in feed; wrong feed format likely" . "\n";
		return null;
	}	

	// initialize array of <item> publication times/dates
	$item_list = $ch->getElementsByTagName('item');
	$seen = array();
	foreach($item_list as $node) {
		$epi = $node->getElementsbyTagName('pubDate')->item(0)->nodeValue;
		$seen[$epi] = 0;
	}

	$i = 0;
	foreach ($item_list as $the_item) {
		if (! $the_item) {
			print "ERROR inspecting item $i\n";
			continue;
		}
		$i++;
		
		$pd = $the_item->getElementsbyTagName('pubDate')->item(0)->nodeValue;
		$seen[$pd] = 1;	
		
		// test if this item is TOO OLD by our standards.  If so, drop it
		if (tooOld($the_item, $oldest_sec)) {
			$seen[$pd] = "to be removed";
			continue;
		}

		// get the <media.content> & its individual attributes
		$enc = $the_item->getElementsByTagName('media.content')->item(0);
		if ($enc) {
			$enc_a  = $enc->attributes;
			$image_url = $enc_a->getNamedItem('url')->nodeValue;
			$image_len = $enc_a->getNamedItem('length')->nodeValue;
			$image_ct  = $enc_a->getNamedItem('type')->nodeValue;
			//print "acquiring image from item: $image_url\n";
		} else {
			// Get the <link> to the actual website article.
			// Podcasts and other media-type feeds will not have this.
			$link = $the_item->getElementsByTagName('link')->item(0)->nodeValue;
			if (! $link) { 
				continue;
			}
	
			// Acquire page link to see if it defines a header image. If so,
			// get link, content type and length
			//print "acquiring page data for item from $link\n";
			$pp->setUrl($link);
			$res = $pp->acquire();
			if ($res['http_code'] != '200') {
				Utilities::logger('page parser: link ' . $link . ' returned error code [' . $res['http_code'] . ']', E_ERROR);
				continue;
			}
			$page_data = json_decode($pp->asJson(), true);
			if (!array_key_exists('metas', $page_data)) {
				file_put_contents('./page_data.json', json_encode($page_data, JSON_PRETTY_PRINT));
				print "Warning: [" . $link . "] (link for item published " . $pd . ") produced no mine-able page data\n";
				print "See file page_data.json for more information\n";
				continue;
			}
			if (! array_key_exists('og:image', $page_data['metas'])) { 
				continue;
			}
			$res = $http->ping($page_data['metas']['og:image'], true, true);
			if (!$res || ($res['http_code'] != '200')) {
				continue;
			}
			
			$image_url = $page_data['metas']['og:image'];
			$image_ct  = $res['content_type'];
			$image_len = $res['content_length'];
		}
		
		//print "<item> image available in page: " . $image_url . " (" . $image_ct . ")(" . $image_len . ")\n";
		
		// Acquire the media; store locally with unique name
		// NOTE: Since local file name is unique to the image's URL (which is unique),
		// we can check if the file already exists and not incur the download penalty 
		// again.
		$name = saveMedia($http, $root, $image_url);
		if (! $name) {
			print "ERROR: unable to download media from " . $image_url . "\n";
		} else {
			// Update the <item>'s XML
			$components = array_reverse(explode('/', $name));
			$media_link = './media/' . $components[0];
		
			// fabricate fresh <enclosure> element
			$mn = $doc->createElement("enclosure");
			$mn->setAttribute('url', $media_link);
			$mn->setAttribute('type', $image_ct);
			$mn->setAttribute('length', $image_len);
							
			// add it to the <item>
			$where = findChild($the_item, 'description');
			$the_item->insertBefore($mn, $the_item->childNodes[$where]);
		}
	}

	// remove <item>s classified as "too old"	
	foreach ($seen as $name => $value) {
		if ($value == "to be removed") {
			$to_delete = findByPubDate($doc, $name, $seen);
			if ($to_delete != null) {
				if (! $ch->removeChild($to_delete)) { print "ERROR: could not remove node with pubdate $name\n"; }
			} 
		}
	} 

	return $doc->saveXML();
}


// ----------------------------------------------------------------------
// Find and handle images in the <item>'s content portion
// ----------------------------------------------------------------------

function AcquireContentImagery($root, $xml, $oldest_sec) {
	$doc = new DOMDocument();
	@$doc->loadXML($xml);
	
	try {
		$pp = new PageParser();
		$http = new URL();
	} catch (Exception $e) {
	}

	$ch = $doc->getElementsByTagName('channel')->item(0);
	if (! $ch) { 
		print "ERROR: no <channel> element in feed; wrong feed format likely" . "\n";
		return null;
	}	

	// initialize array of <item> publication times/dates
	$item_list = $ch->getElementsByTagName('item');
	$seen = array();
	foreach($item_list as $node) {
		$epi = $node->getElementsbyTagName('pubDate')->item(0)->nodeValue;
		$seen[$epi] = 0;
	}

	$i = 0;
	foreach ($item_list as $the_item) {
		if (! $the_item) {
			print "ERROR inspecting item $i\n";
			continue;
		}
		$i++;
		$des = $the_item->getElementsByTagName('description')->item(0);
		if ($des && $des->textContent) {
			//print "($i) description: ";
			//print $des->textContent . "\n";

			$new_text = processEmbeddedImagery($root, $des->textContent);
			$cdata = $doc->createCDATASection($new_text);
			
			//create a new node to replace the old one, and replace
			$new_one = $doc->createElement("description");
			$new_one->appendChild($cdata);
			$des->parentNode->replaceChild($new_one, $des);
			//print "\n======\n" . $new_text . "\n";
		}
		
		// now content:encoded
		$all = $the_item->getElementsByTagNameNS('http://purl.org/rss/1.0/modules/content/', '*');
		foreach ($all as $one) {
			if ($one->tagName == 'content:encoded' && $one->textContent) {
				//print "\n($i)\n" . $one->textContent . "\n";
				$new_text = processEmbeddedImagery($root, $one->textContent);
				$cdata = $doc->createCDATASection($new_text);
				
				//create a new node to replace the old one, and replace
				$new_one = $doc->createElementNS("http://purl.org/rss/1.0/modules/content/","content:encoded");
				$new_one->appendChild($cdata);
				$one->parentNode->replaceChild($new_one, $one);
				//print "\n======\n" . $new_text . "\n";
			}
		}
	}
	
	return $doc->saveXML();
}

// ----------------------------------------------------------------------
// Find and process an <item>'s content for embeded imagery
// ----------------------------------------------------------------------

function processEmbeddedImagery($root, $content_html) {
	$doc = new DOMDocument('1.0', 'utf-8');
	@$doc->loadHTML('<meta charset="utf8">' . $content_html);
	
	$images = $doc->getElementsByTagName('img');
	$good = 0;
	if ($images->count() > 0) {
		try {
			$http = new URL();
		} catch (Exception $e) {
		}

		foreach ($images as $image) {
			$enc_a  = $image->attributes;

			$bad = false;
			for ($i = 0; $i < $enc_a->length; $i++) {
				switch($enc_a->item($i)->name) {
					case 'src':
						$image_url = $enc_a->item($i)->value;
						break;
					case 'alt':
						$image_alt = $enc_a->item($i)->value;
						break;
					case 'width':
						$image_w = $enc_a->item($i)->value;
						if ($image_w < 2) { $bad = true; }
						break;
					case 'height':
						$image_h = $enc_a->item($i)->value;
						if ($image_h < 2) { $bad = true; }
						break;
					case 'ismap':
					case 'usemap':
					case 'crossorigin':
						$bad = true;
						break;	
				}
			}
			
			// get rid of images that require the server or can't be accessed
			if ($bad) { 
				print "--> bad image! <--\n";
				$image->parentNode->removeChild($image);
				continue;
			}
			
			$good++;
			//print "<item> image available in page: " . $image_url . " (" . $image_h . ")(" . $image_w . ")\n";
		
			// ping it for meta data
			$res = $http->ping($image_url, true, true);
			if (!$res || ($res['http_code'] != '200')) {
				print "failed to acquire information from URL: " . $image_url . "\n";
				continue;
			}

			$image_ct  = $res['content_type'];
			$image_len = $res['content_length'];

			// Acquire the media; store locally with unique name
			// NOTE: Since local file name is unique to the image's URL (which is unique),
			// we can check if the file already exists and not incur the download penalty 
			// again.
			$name = saveMedia($http, $root, $image_url);
			if (! $name) {
				print "ERROR: unable to download media from " . $image_url . "\n";
			} else {
				// Update the <IMG> tag
				$components = array_reverse(explode('/', $name));
				$media_link = './media/' . $components[0];
		
				// get attributes (since it might have been compressed)
				$info = getimagesize($name);
				$image_width = $info[0];
				$image_height = $info[1];
	
				// fabricate fresh <IMG> element
				$mn = $doc->createElement("img");
				$mn->setAttribute('src', $media_link);
				$mn->setAttribute('alt', $image_alt);
				$mn->setAttribute('width', $image_width);
				$mn->setAttribute('height', $image_height);
								
				// replace this <img> node
				$image->parentNode->replaceChild($mn, $image);
			}
		}
	}
	//print "EMBEDDED IMAGERY: processed $good images in this content\n";
	// save it to a string of HTML
	$html = $doc->saveHTML($doc);
	// remove unnecessary elements
	$p1 = strpos($html, '<body>') + 6;
	$p2 = strpos($html, '</body>');
	$new = substr($html, $p1, ($p2-$p1));
	
	return $new;
}

// ----------------------------------------------------------------------
// Test if this feed item is too old to include in the feed
// ----------------------------------------------------------------------

function tooOld($item, $max_age) {
	$oldest_allowed = time() - $max_age;
	
	$pubdate = $item->getElementsByTagName('pubDate')->item(0)->nodeValue;
	$pubtime = strtotime($pubdate);
	
	$difference = time() - $pubtime;
	if ($difference > $max_age) {
		$os = date("Y-m-d", $oldest_allowed);
		print "TOO OLD: item ["	. $item->getElementsByTagName('title')->item(0)->nodeValue . "] is beyond " .  $os . "\n"; 
		return true;
	}
	return false;
}

// ----------------------------------------------------------------------
// Find an <item> node by its publication date ('pubDate' element)
// ----------------------------------------------------------------------

function findByPubDate($doc, $pd, $seen) {
	$ch = $doc->getElementsByTagName('channel')->item(0);	
	$item_list = $ch->getElementsByTagName('item');
	$item_count = $item_list->count();
	for ($i = 0; $i < $item_count; $i++) {
		$test = $item_list->item($i);
		$tpd = $test->getElementsByTagName('pubDate')->item(0)->nodeValue;
		if ($tpd == $pd) { 
			if ($seen[$tpd] == 0) { return $test; }
			else { return null; }
		}
	}

	return null;
}

// ----------------------------------------------------------------------
// Find an <item> node given its name
// ----------------------------------------------------------------------

function findChild($item, $name) {
	$children = $item->childNodes;
	$n = 0;
	foreach ($children as $my_child) {
		if ($my_child->nodeName == $name) { return $n; }
		$n++;
	}

	return 0;
}

// ----------------------------------------------------------------------
// Clean out the content directories, removing out-of-data media files
// NOTE: function saveMedia() constantly updates the file timestamp on 
// files "in use", actively "preserving" those files from being reaped here.
// ----------------------------------------------------------------------

function cleanUp($root, $out_of_date_time) {
	$dirs = subdirectories($root);
	if (! $dirs) { return false; }
	
	foreach($dirs as $dir) {
		$actual_dir = $root . '/' . $dir . '/media';
		print "cleanup: scanning " . $actual_dir . "\n";
		$files = directoryContents($actual_dir);
		foreach($files as $fn) {
			$actual_file = $root . '/' . $dir . '/media/' . $fn;
			if (isOld($actual_file, $out_of_date_time)) {
				Utilities::logger('unlinking ' . $actual_file, E_USER_NOTICE);
				unlink($actual_file);
			}
		}
	}
	
	return true;
}

// ----------------------------------------------------------------------
// Is the requested file too old?
// ----------------------------------------------------------------------

function isOld($fn, $out_of_date_time) {
	$fp = fopen($fn, 'r');
	if (! $fp) {
		print "could not open [" . $fn . "]\n";
		return false;
	}
	$fdata = fstat($fp);
	if ($fdata['mtime'] <= $out_of_date_time) { $is_old = true; }
	else { $is_old = false; }
	
	fclose($fp);
	return $is_old;
}

// ----------------------------------------------------------------------
// Get directory contents
// ----------------------------------------------------------------------

function directoryContents($dir) {
	$files = array();
    $flist = scandir($dir);
    if (! $flist) {
        print "unable to scan directory [" . $dir . "]\n";
		return null;
    }
    
    foreach ($flist as $fn) {
    	$actual_entry = $dir . '/' . $fn;
        if (is_file($actual_entry)) { 
        	$files[] = $fn;
        }
    }    

    return $files;
}

// ----------------------------------------------------------------------
// Get subdirectories (content directories) of my root directory
// ----------------------------------------------------------------------

function subdirectories($dir) {
	if (! is_dir($dir)) {
		print "[" . $dir . "] is not a directory\n";
		return null;
	}
    
    $dh = opendir($dir);
    if (! $dh) {
    	print "can not open directory [" . $dir . "]\n";
		return null;
	}

	$dirs = array();
    while (false !== ($entry = readdir($dh))) {
    	$actual_entry = $dir . '/' . $entry;
        if (! is_dir($actual_entry)) { continue; }
        switch ($entry) {
        	case '.':
        	case '..':
        		break;
        	default:
        		$dirs[] = $entry;
        		break;	
        }
    }    

    closedir($dh);
    return $dirs;
}

// ----------------------------------------------------------------------
// image resize/ compression
// ----------------------------------------------------------------------

function compress_image($source, $destination, $quality) {	
    $info = getTheImageSize($source);
    if (! $info) { return false; }

	$width = $info[0];
	$height = $info[1];

	$compressed = true;
    switch($width) {
		case ($width > 4000):
			$percent = 0.2;
			break;
		case ($width > 3000):
			$percent = 0.275;
			break;
		case ($width > 2000):
			$percent = 0.4;
			break;
		case ($width > 1500):
			$percent = 0.55;
			break;
		case ($width > 1000):
			$percent = 0.8;
			break;		
		default:
			$compressed = false;
			$percent = 1; // 100% no resampling			
    }
    
    $new_width  = intval($width * $percent);
    $new_height = intval($height * $percent);
    if ($compressed) {
    	print "image compression: input is [" . $width. "] wide [" . $height . "] high; " .
    		"output will be [" . $new_width. "] wide [" . $new_height . "] high\n";
  	} else {
  		print "image compression: output is uncompressed [" . $width. "] wide [" . $height . "] high\n";
  	} 
  	
  	$tempfile = downloadMediaToTempFile($source);
	switch($info['mime']) {
		case 'image/jpeg':
			$input_img = imagecreatefromjpeg($tempfile);
    		break;
    	case 'image/gif':
    		$input_img = imagecreatefromgif($tempfile);
   			break;
    	case 'image/png':
   			$input_img = imagecreatefrompng($tempfile);
    		break;
    	default:
    		Utilities::logger("Oops! image at [" . $source . "] is of improper file type [" . $info['mime'] . "]", E_ERROR);
    		return false;	
	}
	
	$filtering = -1;	// no special filtering
	
	// copy with resampling and write output file
	$output_img = imagecreatetruecolor($new_width, $new_height);
	imagecopyresampled($output_img, $input_img, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    imagepng($output_img, $destination, $quality, $filtering);
    
    // free memory
    imagedestroy($input_img);
    imagedestroy($output_img);
    if ($tempfile) {  unlink($tempfile); }
    
    return true;
}

// ----------------------------------------------------------------------
// audio compression
// docs at http://ffmpeg.org
// ----------------------------------------------------------------------

function compress_audio($source, $destination, $quality) {
	global $ffm;
	
  	$tempfile = downloadMediaToTempFile($source);

	// to prevent ffmpeg errors
	if (file_exists($destination)) { unlink($destination); }
	
	$retval = null;
	$output = null;
	$cmd = $ffm . " -hide_banner -loglevel error -i " . $tempfile . " -map 0:a:0 -b:a 96k " . $destination;	
	exec($cmd, $output, $retval);

	if (file_exists($tempfile)) { unlink($tempfile); }
	if ($retval != 0) {
		Utilities::logger('failed to compress audio media from ' . $source, E_USER_NOTICE);
		return false;
	}
	return true;
}

// ----------------------------------------------------------------------
// video compression
// docs at http://ffmpeg.org
// ----------------------------------------------------------------------

function compress_video($source, $destination, $quality) {
	global $ffm;
	
  	$tempfile = downloadMediaToTempFile($source);

	// to prevent ffmpeg errors
	if (file_exists($destination)) { unlink($destination); }
	
	$retval = null;
	$output = null;
	$cmd = $ffm . " -hide_banner -loglevel error -i " . $tempfile . " " . $destination;
	exec($cmd, $output, $retval);
	
	if (file_exists($tempfile)) { unlink($tempfile); }
	if ($retval != 0) {
		Utilities::logger('failed to compress video media from ' . $source, E_USER_NOTICE);
		return false;
	}
	return true;
}

// ----------------------------------------------------------------------
// terrible that I have to do this in 2021, but here we are.
// ----------------------------------------------------------------------

function getTheImageSize($source) {
	$loc = downloadMediaToTempFile($source);
	$attrs = getimagesize($loc);
	unlink($loc);
	return $attrs;
}

// ----------------------------------------------------------------------
// acquire source media
// ----------------------------------------------------------------------

function downloadMediaToTempFile($source) {
	$destination = tempnam(sys_get_temp_dir(), 'M_media_M');

	$path = parse_url($source, PHP_URL_PATH);
	list ($ext, $p) = explode('.', strrev($path));	
	$destination .= '.' . strrev($ext);
	
	try {
		$http = new URL();
		$http->acquire($source, true, false, $destination);
	} catch(Exception $e) {
		print "error acquiring media file: " . $e->getMessage() . "\n";
	}

	return $destination;
}

// ----------------------------------------------------------------------
// does this machine host an ffmpeg installation?
// ----------------------------------------------------------------------

function ffmpeg_exists() {
	$retval = null;
	$output = null;
	exec("which ffmpeg", $output, $retval);
	if (($retval != 0) || !$output || ($output[0] == '')) {
		return null;
	}
	
	return $output[0];	
}
