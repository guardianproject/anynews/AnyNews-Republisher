<?php
/**
 * ----------------------------------------------------------------------
 * Initialize use of IPFS
 * Create this node's IPNS "name" by creating some content, ADDing it to
 * IPFS, then PUBLISHing it (which, on first use, creates the name) 
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php';

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\Utilities;
use guardianproject\anynews\IPFSService;

$fpath = './.dummy';
try {
	$ipfs = new IPFSService();
	if (! $ipfs->ipfsRunning()) {
		print "IPFS daemon is not running\n";
		exit(1);
	}
	
	print "initializing IPFS\n";
	// create some dummy content and add it to IPFS
	file_put_contents($fpath, 'hello, world! ' . mt_rand(100000, 2000000));
	$ipfs->setFilePath($fpath);
	$hash = $ipfs->add();
	if (! $hash) {
		print "failed to add contents of file [" . $fpath . "] to IPFS\n";
		exit(1);
	}
	
	print "file [" . $fpath . "] added IPFS CID [" . $hash . "]\n";

	// Publish this content to IPNS, generating this node's IPNS name if I doesn't already
	// have one.  I'm finding this is a very slow operation, often timing out
	if (($out = $ipfs->publish($hash)) == null) {
		print "publish command failed/timed-out. Creation of IPNS name failed. Please re-try\n";
	} else {
		print "---\n---\n--- Your IPNS NAME is: /ipns/" . $out['Name'] . "\n---\n";
		print "--- ADD THIS to your copy of ipfsretriever.php AND\n";
		print "--- PROVIDE THIS NAME TO THOSE WHO WILL BE RETRIEVING YOUR CONTENT\n---\n---\n";
	}
	
	// remove temporary resources
	$ipfs->remove($hash);
	unlink($fpath);
} catch (Exception $e) {
	Utilities::logger("error: " . $e->getMessage(), E_ERROR);
	return false;
}


?>
