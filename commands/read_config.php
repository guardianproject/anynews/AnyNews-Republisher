<?php
/**
 * ----------------------------------------------------------------------
 * provide PHP config variables to shell
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/
 
// DON'T NAMESPACE THIS CLASS 

require_once './inc.php';

$var = $argv[1];
if (AnyNewsConfig::get($var)) { echo AnyNewsConfig::get($var); }
else if (UtilitiesConfig::get($var)) { echo UtilitiesConfig::get($var); }

exit(0);
?>
