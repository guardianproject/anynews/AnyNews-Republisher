<?php
/**
 * ----------------------------------------------------------------------
 * Acquire packaged news feeds via IPFS and unpackage into a local directory
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php';

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\Utilities;
use guardianproject\anynews\IPFSService;

// YOUR IPNS NAME
// the "permalink" for this content in IPFS (that is, the most current version)
$ipns_name = AnyNewsConfig::IPNS_NAME;

// optional path to store incoming content
$root = AnyNewsConfig::FEED_DIRECTORY;
if ($argc > 1) { $root = $argv[1]; }
if (! file_exists($root)) { 
	if (! mkdir($root)) {
		print "ERROR: unable to make root directory " . $root . "\n";
		exit(1);
	}
}
$bare_file = 'cf_blob.tar';
$output_file = $root . '/' . $bare_file;

// fail if no Internet connection
if (! isConnected()) { 
	print 'Not Connected to the Internet [' . date("F j, Y, g:i a") . "]\n";
	exit(0);
}

// download the package
if (! downloadFile($ipns_name, $output_file)) {
	print "error downloading from IPNS name [" . $ipns_name. "]\n";
	exit(1);
}

// unpackage the package
$sc = $f = null;
$cmd = '(cd ' . $root . '; tar xf ' . $bare_file . ')';
exec($cmd, $f, $sc);
if ($sc != 0) {
	print "error unpackaging " . $output_file . " (" . $sc . ")\n";
} else {
	// removed the downloaded file 
	unlink($output_file);
	// note time of last run
	memorializeUpdate();
	print "done" . "\n";
}

exit(0);

// ----------------------------------------------------------------------
// AUXILIARY FUNCTIONS
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// Test for connectivity
// ----------------------------------------------------------------------

function isConnected() {
	try {
		$url = new URL();
		$url->setTimeout(30);
		$res = $url->ping('https://www.google.com', true, true);
	} catch (Exception $e) {
		return false;
	}

	switch($res['http_code']) {
		case '0':   // no network connection
		case '408': // service timeout
		case '503': // service unavailable 
		case '504': // gateway timeout
			return false;
			break;
		default:
			return true;
	}
}

// ----------------------------------------------------------------------
// Download file from IPFS
// ----------------------------------------------------------------------

function downloadFile($name, $output_fn) {
	try {
		$ipfs = new IPFSService();
		if (! $ipfs->ipfsRunning()) {
			print "IPFS daemon is not running\n";
			return false;
		}

		// resolve IPNS name to IPFS CID
		$hash = $ipfs->find($name);
		print "resolved name [" . $name . "] to CID [" . $hash . "]\n";

		// retrieve contents of this CID
		$ipfs->setFilePath($output_fn);
		$res = $ipfs->get($hash);
		print '[get] result: ' . $res . "\n";
	} catch (Exception $e) {
		print "error: " . $e->getMessage() . "\n";
		return false;
	}

	return true;
}

// ----------------------------------------------------------------------
// Leave a timestamp
// ----------------------------------------------------------------------

function memorializeUpdate() {
	$fp = fopen('./.timestamp', 'w');
	fputs($fp, date("Y-m-d H:i:s T"). "\n");
	
	fclose($fp);
}

?>
