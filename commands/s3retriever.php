<?php
/**
 * ----------------------------------------------------------------------
 * Acquire packaged news feeds and unpackage into a local directory
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php';

use guardianproject\proxyservices\utilities\Utilities;
use guardianproject\proxyservices\utilities\URL;

// path to store incoming content
$root = AnyNewsConfig::FEED_DIRECTORY;
if ($argc > 1) { $root = $argv[1]; }
if (! file_exists($root)) { 
	if (! mkdir($root)) {
		print "ERROR: unable to make root directory " . $root . "\n";
		exit(1);
	}
}

$bucket = AnyNewsConfig::S3_BUCKET;

// fail if no Internet connection
if (! isConnected()) { 
	print 'Not Connected to the Internet [' . date("F j, Y, g:i a") . "]\n";
	exit(0);
}

UtilitiesConfig::showLogging(false);

$input_file = AnyNewsConfig::CONTENT_BUNDLE;
$output_file = 's3blob.tar';

// check that S3 content is newer than what we have now
if (! isNewer($bucket, $input_file)) {
	Utilities::logger("No new content to download", E_USER_NOTICE);
	print "Nothing new to download\n";
	exit(0);
} 

print "New content to download\n";

// download the package
if (! downloadFile($bucket, $input_file, $root . '/' . $output_file)) {
	print "error downloading " . $input_file . "\n";
	exit(1);
}

// unpackage the package
$sc = $f = null;
$cmd = '(cd ' . $root . '; tar xf ' . $output_file . ')';
exec($cmd, $f, $sc);
if ($sc != 0) {
	print "error unpackaging " . $input_file . " (" . $sc . ")\n";
} else {
	// removed the downloaded file 
	unlink($root . '/' . $output_file);
	// note time of last run
	memorializeUpdate();
	print "done" . "\n";
}

exit(0);

// ----------------------------------------------------------------------
// AUXILIARY FUNCTIONS
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// Test for connectivity
// ----------------------------------------------------------------------

function isConnected() {
	try {
		$url = new URL();
		$url->setTimeout(30);
		$res = $url->ping('https://www.google.com', true, true);
	} catch (Exception $e) {
		return false;
	}

	switch($res['http_code']) {
		case '0':   // no network connection
		case '408': // service timeout
		case '503': // service unavailable 
		case '504': // gateway timeout
			return false;
			break;
		default:
			return true;
	}
}

// ----------------------------------------------------------------------
// is the file on S3 newer than the last update we did?
// ----------------------------------------------------------------------

function isNewer($bucket, $fn) {
	// get date on current S3 file
	$data = getS3Metadata($bucket, $fn);
	if (! $data) { return false; }
	$s3_date = strtotime($data['LastModified']);

	// get timestamp on our last file acquisition	
	if (! file_exists('./.timestamp')) { return true; }
	$lu_date = strtotime(file_get_contents('./.timestamp'));

	// check 'em
	print "S3 file date: [" . date('g:ia \o\n l jS F Y', $s3_date). "]; local timestamp [" . date('g:ia \o\n l jS F Y', $lu_date) . "]\n";
	if ($s3_date <= $lu_date) { return false; }	
	return true;
}

// ----------------------------------------------------------------------
// Download file from S3 (all bucket/acct data via S3Credential)
// ----------------------------------------------------------------------

function downloadFile($bucket, $fn, $output_fn) {

	$request = AnyNewsConfig::S3_BASE_URL . '/' . $bucket . '/' . $fn;

	try {
		$url = new URL();
		$url->setTimeout(30);
		$res = $url->get($request, true, false, $output_fn);
	} catch (Exception $e) {
		Utilities::logger("error downloading content archive: " . $e->getMessage(), E_ERROR);
		return null;
	}

	Utilities::logger("downloaded content archive [" . $fn . "] to [" . $output_fn . "]", E_NOTICE);
	return $res;
}

// ----------------------------------------------------------------------
// Leave a timestamp
// ----------------------------------------------------------------------

function memorializeUpdate() {
	$fp = fopen('./.timestamp', 'w');
	fputs($fp, date("Y-m-d H:i:s T"). "\n");
	fclose($fp);
	Utilities::logger('content update complete', E_NOTICE);
}

// ----------------------------------------------------------------------
// new S3 helper
// ----------------------------------------------------------------------

function getS3Metadata($bucket, $fn) {
	try {
		$url = new URL();
	} catch (Exception $e) {
		Utilities::logger("error using URL: " . $e->getMessage(), E_ERROR);
		return null;
	}

	$request = AnyNewsConfig::S3_BASE_URL . '/' . $bucket . '/' . $fn;
	$res = $url->ping($request, true, true);
	$res['LastModified'] = $res['headers']['Last-Modified'];
	
	return $res;
}

?>
