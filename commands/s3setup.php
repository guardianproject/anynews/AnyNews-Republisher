<?php
/**
 * ----------------------------------------------------------------------
 * Set up your Amazon S3 Credentials
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects

use guardianproject\anynews\S3Credential;
use guardianproject\proxyservices\utilities\Cryptor as Cryptor;

$cred = array();
if ($argc < 5) {
	print "Usage: " . $argv[0] . " acct_name aws_id aws_key s3_bucketname\n";
	print "\tacct_name: A short string of your choosing identifying your account\n";
	print "\taws_id, aws_key: The Amazon Web Services account ID and access key\n";
	print "\ts3_bucketname: Your previously-created AWS S3 bucket identifier\n";
	exit(0);
} else {
	$cred['un'] = $argv[1];
	$cred['id'] = $argv[2];
	$cred['key'] = $argv[3];
	$cred['bucket'] = $argv[4];
}

UtilitiesConfig::showLogging(false);

try {
	if (UtilitiesConfig::ENCRYPT_CREDENTIALS) {
		// see if we've got an encryption key already
		$kf = UtilitiesConfig::encryptionKeyFile();
		if (! file_exists($kf)) {
			print "NOTICE: Creating a new encryption key in file [" . $kf . "]\n";
			print "This key will be used to encrypt/decrypt your S3 credentials\n";
			if (! file_exists(UtilitiesConfig::privDirectory())) {
				mkdir(UtilitiesConfig::privDirectory());
				print "created new directory " . UtilitiesConfig::privDirectory() . "\n";
			}
			Cryptor::generateKey();
		}
	}
} catch (Exception $e) {
	print "Error setting up S3 credentials: " . $e->getMessage() . "\n";
	exit(1);
}

// account info goes into a database table
try {
	$s3 = new S3Credential();
	$s3->setCredentials($cred);
} catch (Exception $e) {
	print "Error setting up S3 credentials: " . $e->getMessage() . "\n";
	exit(1);
}

// account name (used as DB index) into a local file
file_put_contents(AnyNewsConfig::S3_ACCOUNT_NAME, $cred['un']);
chmod(AnyNewsConfig::S3_ACCOUNT_NAME, 0600);

if (UtilitiesConfig::ENCRYPT_CREDENTIALS) {
	print "S3 account information stored (encrypted database) in " . UtilitiesConfig::privDirectory() . "\n";
} else {
	print "S3 account information stored (database) in " . UtilitiesConfig::privDirectory() . "\n";
}

exit(0);
?>
