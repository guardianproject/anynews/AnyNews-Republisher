<?php
/**
 * ----------------------------------------------------------------------
 * Upload aggregated news feeds to an AWS S3 bucket
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects

use Aws\S3\S3Client;

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\Utilities;

use guardianproject\anynews\S3Service;

// the MAXIMUM UPLOAD FILE SIZE
$max_upload = 4096 * 200000; // 890MB

$root = AnyNewsConfig::FEED_DIRECTORY;
if ($argc > 1) { $root = $argv[1]; }
$upload_file = AnyNewsConfig::CONTENT_BUNDLE;

UtilitiesConfig::showLogging(false);

// Create the tarball
$cmd = '(cd ' . $root . '; tar cf ' . $upload_file . ' .)';
$sc = $f = null;
exec($cmd, $f, $sc);
if ($sc != 0) {
	print "Error creating " . $upload_file . "\n";
} else {
	print "Created "  . $upload_file . ". Uploading to S3.\n";
}

// make sure the file to be uploaded is a managable file size
$fp = fopen($root . '/' . $upload_file, 'r');
if (! $fp) {
	print "can't open upload file " . $root . '/' . $upload_file . ". upload aborted\n";
	exit(1);
}
$fdata = fstat($fp);
if ($fdata['size'] > $max_upload) {
	print "upload file " . $root . '/' . $upload_file . " too big (" . $fdata['size'] . 
		") (max = " . $max_upload . "). upload aborted\n";
	exit(1);
}

// upload the tarball
if (! uploadFile($root . '/' . $upload_file, $upload_file)) {
	print "Error uploading $upload_file\n";
} else {
	print "$upload_file successfully uploaded to S3\n";
}

exit(0);

// ----------------------------------------------------------------------
// Upload file to S3 (all bucket/acct data via S3Credential)
// ----------------------------------------------------------------------

function uploadFile($fpath, $name, $bool = false) {
	try {
		$s3 = new S3Service();
	} catch (Exception $e) {
		Utilities::logger("error acquiring ContentUploadAdaptor for S3: " . $e->getMessage(), E_ERROR);
		return false;
	}

	$s3->setVanityString($name);
	$s3->setServiceTimeout(100);
	$s3->setPrivacy(false);
	$s3->setEncryption(false);
	$s3->setBinary($bool); 
	$s3->setFile($fpath);
	$result = $s3->paste();
	if (! array_key_exists('uuid', $result)) {
		print $result['status'] . "\n";
		return false;
	}
	
	print json_encode($result, JSON_PRETTY_PRINT) . "\n";
	return $result;
}

?>