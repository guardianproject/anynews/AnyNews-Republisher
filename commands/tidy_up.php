<?php
/**
 * ----------------------------------------------------------------------
 * cleanup the stale media in the feed directories 
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects

use guardianproject\proxyservices\utilities\Utilities;

UtilitiesConfig::showLogging(false);

// path to root directory of data to aggregate
$root = AnyNewsConfig::FEED_DIRECTORY;
if ($argc > 1) { $root = $argv[1]; }
print "Working from root directory $root\n";

// how many days old?
$cache_days = AnyNewsConfig::CACHE_DURATION;
if ($argc > 2) {
	$cache_days = $argv[2];
}
$cache_secs = 60 * 60 * 24 * $cache_days;
$sell_by_date = time() - $cache_secs;
print "Removing content beyond [$cache_days] days (sell-by date: " . date('c', $sell_by_date) . ") old\n";

// cleanup old files or not (any value other than 'true' means just test and show)
$cleanup = AnyNewsConfig::CLEANUP_ENABLED;
if ($argc > 3) {
	$cleanup = $argv[3];
	if ($cleanup == 'true') { $cleanup = true; }
	else { 
		print "I will only PRETEND to cleanup\n!";
		$cleanup = false;
	}
}

Utilities::logger('Cleaning up old media files', E_NOTICE);
if (! mediaCleanup($root, $sell_by_date, $cleanup)) {
	print "Apologies. That didn't end well.\n";
} else {
	Utilities::logger("Local media storage cleanup complete", E_NOTICE);
	print "Lovely. We're done.\n";
}
	
exit(0);

// ----------------------------------------------------------------------
// Clean out the content directories, removing out-of-data media files
// NOTE: function saveMedia() constantly updates the file timestamp on 
// files "in use", actively "preserving" those files from being reaped here.
// ----------------------------------------------------------------------

function mediaCleanup($root, $out_of_date_time, $do_it) {
	$dirs = subdirectories($root);
	if (! $dirs) { return false; }
	
	foreach($dirs as $dir) {
		$actual_dir = $root . '/' . $dir . '/media';
		print "scanning " . $actual_dir . "\n";
		$files = directoryContents($actual_dir);
		foreach($files as $fn) {
			$actual_file = $root . '/' . $dir . '/media/' . $fn;
			if (isOld($actual_file, $out_of_date_time)) {
				if ($do_it) {
					Utilities::logger('unlinking ' . $actual_file, E_USER_NOTICE);
					unlink($actual_file);
				} else {
					print "REMOVAL candidate: " . $actual_file . "\n";
				}
			}
		}
	}
	
	return true;
}

// ----------------------------------------------------------------------
// Is the requested file too old?
// ----------------------------------------------------------------------

function isOld($fn, $out_of_date_time) {
	$fp = fopen($fn, 'r');
	if (! $fp) {
		print "could not open [" . $fn . "]\n";
		return false;
	}
	$fdata = fstat($fp);
	if ($fdata['mtime'] <= $out_of_date_time) { $is_old = true; }
	else { $is_old = false; }
	
	fclose($fp);
	return $is_old;
}

// ----------------------------------------------------------------------
// Get directory contents
// ----------------------------------------------------------------------

function directoryContents($dir) {
	$files = array();
    $flist = scandir($dir);
    if (! $flist) {
        print "unable to scan directory [" . $dir . "]\n";
		return null;
    }
    
    foreach ($flist as $fn) {
    	$actual_entry = $dir . '/' . $fn;
        if (is_file($actual_entry)) { 
        	$files[] = $fn;
        }
    }    

    return $files;
}

// ----------------------------------------------------------------------
// Get subdirectories (content directories) of my root directory
// ----------------------------------------------------------------------

function subdirectories($dir) {
	if (! is_dir($dir)) {
		print "[" . $dir . "] is not a directory\n";
		return null;
	}
    
    $dh = opendir($dir);
    if (! $dh) {
    	print "can not open directory [" . $dir . "]\n";
		return null;
	}

	$dirs = array();
    while (false !== ($entry = readdir($dh))) {
    	$actual_entry = $dir . '/' . $entry;
        if (! is_dir($actual_entry)) { continue; }
        switch ($entry) {
        	case '.':
        	case '..':
        		break;
        	default:
        		$dirs[] = $entry;
        		break;	
        }
    }    

    closedir($dh);
    return $dirs;
}

?>
