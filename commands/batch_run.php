<?php
/**
 * ----------------------------------------------------------------------
 * An all-in-one process for republishing to a remote s3 bucket
 * (without a tarball involved).
 *
 * All data is loaded and persisted to S3.
 *
 * ----------------------------------------------------------------------
 * @author Abel Luck <abel@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

error_reporting(E_ERROR | E_PARSE | E_NOTICE);
date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects

use guardianproject\proxyservices\utilities\URL;
use guardianproject\anynews\S3Service;

require_once './lib/feed_aggregation.php';
require_once './lib/s3_uploading.php';

function loadFeeds($s3, $main_folder)
{
    $feeds_json = $main_folder . '/feeds.json';
    $string = $s3->retrieve($feeds_json);
    if (!$string) throw new Exception("Failed to load feeds.json from bucket.");
    return json_decode($string, true);
}
function loadLocalFeeds() {
    $feeds_file = "feeds/feeds.json";
    $fl = file_get_contents($feeds_file);
    if (!$fl) {
        print 'ERROR: file ' . $feeds_file . ' not found or empty' . "\n";
        exit(1);
    }
    return json_decode($fl, true);
}
function s3prep($s3, $feeds, $main_folder)
{
    foreach ($feeds as $service => $links) {
        $folder_name = $main_folder . '/' . $service;
        // if this master folder exists, everything beneath it will exist, too
        if (!$s3->folderExists($folder_name)) {
            if ($s3->createFolder($folder_name)) {
                print "I created folder " . $folder_name . "\n";
            } else {
                print "error creating $folder_name\n";
                continue;
            }
            if ($s3->createFolder($folder_name . '/media')) {
                print "I created folder " . $folder_name . "/media\n";
            }
            if ($s3->createFolder($folder_name . '/app')) {
                print "I created folder " . $folder_name . "/app\n";
            }
        } else {
            print "folder [" . $folder_name . "] already exists\n";
        }
    }
}

function httpClient($bypass_token)
{
    $headers = [
        "User-Agent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36'"
    ];

    if(!empty($bypass_token)) {
        $headers[] = "Bypass-Rate-Limit-Token: ". $bypass_token;
    }
    Url::setDefaultReqHeaders($headers);
    $url = new URL();
    $url->setTimeout(90);
    return $url;
}

function main()
{
    UtilitiesConfig::showLogging(false);


    $startup_time = time();
    $main_folder = $_SERVER["ANYNEWS_MAIN_FOLDER"] ?? 'feeds';
    $age_limit_seconds = $_SERVER["ANYNEWS_AGE_LIMIT_SECONDS"] ?? 30 * 24 * 60 * 60;   // thirty days
    $cleanup = $_SERVER["ANYNEWS_CLEANUP_FILES"] ?? false;
    $local_root = $_SERVER["ANYNEWS_LOCAL_DATA_PATH"] ?? "data/feeds";
    $cache_days = $_SERVER["ANYNEWS_S3_CACHE_DURATION_DAYS"] ?? 7;
    $dry_run = $_SERVER["ANYNEWS_S3_UPLOAD_DRY_RUN"] ?? false;
    $bypass_token = $_SERVER["ANYNEWS_CDN_BYPASS_TOKEN"] ?? "";
    $private = false;
    $url = httpClient($bypass_token);

    $s3 = new S3Service();
    $feeds = loadFeeds($s3, $main_folder);
    // $feeds = loadLocalFeeds();
    s3prep($s3, $feeds, $main_folder);
    runAggregation($local_root, $url, $cleanup, $age_limit_seconds, $startup_time, $feeds);
    uploadDirectory($dry_run, $s3, $local_root, $main_folder, $cache_days, $private);
}

main();
