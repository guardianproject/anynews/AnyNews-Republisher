<?php
/**
 * ----------------------------------------------------------------------
 * Collect and aggregate news feeds into local directory hierarchy with
 * media references locally defined and files compressed.  Optionally, 
 * clip old feed items using a "sell by" date.
 *
 * Configure setting in AnyNewsConfig.php.  Override with command line args.
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects
use guardianproject\proxyservices\utilities\URL;
require_once './lib/feed_aggregation.php';

UtilitiesConfig::showLogging(false);

// path to root directory of data to aggregate
$root = AnyNewsConfig::FEED_DIRECTORY;
if ($argc > 1) { $root = $argv[1]; }

// file containing feeds to retrieve
$feeds_file = AnyNewsConfig::FEED_LIST;
if ($argc > 2) {
	$feeds_file = $argv[2];
}

// cleanup old files or not
$cleanup = AnyNewsConfig::AUTO_CLEANUP;
if ($argc > 3) {
	$cleanup = $argv[3];
	if ($cleanup == 'true') { $cleanup = true; }
	else { $cleanup = false; }
}

// Set a "sell-by" date for <item>s in the feed.  If an <item> is older than
// the specified number of seconds, it will NOT be included in the packaged output.
// This is especially useful for PODCASTS;
// NUM_OF_DAYS * HOURS PER DAY * MINUTES PER HOUR * SECONDS PER MINUTE
$oldest_item = AnyNewsConfig::OLDEST_ITEM;
if ($argc > 3) {
	$oldest_item = $argv[3];
}

// Do I have an installation of the FFMPEG compression kit on this system?
$ffm = ffmpeg_exists();
if (! $ffm) {
	print "notice: audio compression DISABLED (no installation of 'ffmpeg' available)\n";
} else {
	print "notice: audio compression ENABLED\n";
}

// --------------------------------------------------------------------------------
// let's begin
// --------------------------------------------------------------------------------

$fl = file_get_contents($feeds_file);
if (! $fl) {
	print 'ERROR: file ' . $feeds_file . ' not found or empty' . "\n";
	exit(1);
}
$feeds = json_decode($fl, true);
if (! $feeds) {
	print 'ERROR: file ' . $feeds_file . ' is wrong format (not valid JSON) or empty' . "\n";
	exit(1);
}

// Grab the startup timestamp. Use this later for cleanup.

$startup_time = time();

try {
	$url = new URL();
	$url->setUserAgent('EthicalFeedAggregator/1.0');
	$url->setTimeout(90);
} catch (Exception $e) {
	print 'TOOLS ERROR: ' . $e->getMessage() . "\n";
	exit(1);
}


runAggregation($root, $url, $cleanup, $oldest_item, $startup_time, $feeds);
?>
