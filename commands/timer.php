<?php
/**
 * ----------------------------------------------------------------------
 * Some hosting environments have web console-only access to cron, allowing 
 * job timing on only limited schedules (once-per-day, once-per-hour).
 * This script gives more fine-grained control.  It returns a non-zero 
 * exit code if criteria are not met, which can be trapped in a shell script
 * to manage execution.
 *
 * Example (with cron set to run every hour):
 *    timer.php h 3 6 9 0
 * will return 0 only if the hour (by the local clock) is 0,3,6,9.  Both
 * hours [h] and minutes [m] are acceptable criteria
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

if ($argc < 2) {
	print "usage: " . $argv[0] .  "h|m num num num ...\n";
	exit(1);
}

$valid_types = array('h','m');
$type = $argv[1];
if (! in_array($type, $valid_types)) {
	print "type must be hour [h] or minute [m].\n";
	exit(1);
}

if ($argc < 3) {
	print "one or more valid periods required.\n";
	exit(1);
}

for ($i = 2; $i < $argc; $i++) {
	switch($type) {
		case 'h':
			$val = intval(date('H', time()));
			break;
		case 'm':
			$val = intval(date('i', time()));	
			break;
		default: 
			print "invalid type [$type]\n";
			exit(1);
	}

	if ($val == intval($argv[$i])) { exit(0); }
}
//print "mistimed [" . $val . "] != [" . intval($argv[$i]) . "]\n";
exit(0);
?>
