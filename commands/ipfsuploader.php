<?php
/**
 * ----------------------------------------------------------------------
 * Upload aggregated news feeds to IPFS
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\Utilities;
use guardianproject\anynews\IPFSService;

$root = AnyNewsConfig::FEED_DIRECTORY;
if ($argc > 1) { $root = $argv[1]; }
$upload_file = AnyNewsConfig::CONTENT_BUNDLE;

// Create the tarball
$cmd = '(cd ' . $root . '; tar cf ' . $upload_file . ' .)';
$sc = $f = null;
exec($cmd, $f, $sc);
if ($sc != 0) {
	print "Error creating " . $upload_file . "\n";
} else {
	print "Created "  . $upload_file . ". Uploading to IPFS.\n";
}

// upload the tarball
if (($hash = uploadFile($root . '/' . $upload_file, $upload_file)) == null) {
	print "Error uploading $upload_file\n";
} else {
	Utilities::logger("Uploaded [" . $upload_file . "] to IPFS at [" . $hash . "]", E_NOTICE);
	print "$upload_file successfully uploaded to IPFS as $hash\n";
}

exit(0);

// ----------------------------------------------------------------------
// Upload file to IPFS
// ----------------------------------------------------------------------

function uploadFile($fpath, $name, $bool = false) {
	try {
		$ipfs = new IPFSService();
		if (! $ipfs->ipfsRunning()) {
			print "IPFS daemon is not running\n";
			return false;
		}
	} catch (Exception $e) {
		Utilities::logger("error: " . $e->getMessage(), E_ERROR);
		return false;
	}

	$ipfs->setFilePath($fpath);
	$hash = $ipfs->add();
	print "file [" . $fpath . "] added IPFS CID [" . $hash . "]\n";
	
	// I'm finding this is a very slow operation, often timing out
	if (($out = $ipfs->publish($hash)) == null) {
		print "publish command failed/timed-out. IPNS linkage failed\n";
	} else {
		print "published " . $hash . " to: " . $out['Name'] . "\n";
	}
	
	return $hash;
}

?>