<?php
/**
 * ----------------------------------------------------------------------
 * manage the list of feeds aggregated
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php'; // our service objects

use guardianproject\proxyservices\utilities\URL;
use guardianproject\proxyservices\utilities\Utilities;

use guardianproject\anynews\FeedManager as FeedManager;

$cmds = array('help', 'add', 'list', 'remove', 'update', 'verify', 'check', 'backups');
if ($argc < 2) {
	usage();
	exit(0);
}

$cmd = $argv[1];
try {
	$fm = new FeedManager();

	switch($cmd) {
		case 'help':
			usage();
			break;
		case 'add':
			if ($argc != 6) {
				usage();
			} else {
				$feedn = $argv[2];
				if ($fm->exists($feedn)) {
					print "feed [" . $feedn . "] already exists. please use the [update] command\n";
				}
				$fm->add($feedn, $argv[3],$argv[4],$argv[5]);
				$fm->save();
				print "feed [" . $feedn . "] added\n";
			}
			break;
		case 'remove':
			if ($argc > 2) { 
				$feedn = $argv[2];
				if ($fm->exists($feedn)) { 
					$fm->remove($feedn);
					$fm->save();
					print "feed [" . $feedn . "] removed\n";
				} else { print "no feed named [" . $feedn . "]\n"; }
			} else {
				print "[remove] command requires feed_name\n";
			}
			break;
		case 'update':
			if ($argc != 6) {
				usage();
			} else {
				$feedn = $argv[2];
				if (! $fm->exists($feedn)) {
					print "feed [" . $feedn . "] does not exist. please use the [add] command\n";
				} else {
					$fm->update($feedn, $argv[3], $argv[4], $argv[5]);
					$fm->save();
					print "feed [" . $feedn . "] updated\n";
				}
			}
			break;
		case 'list':
			if ($argc > 2) { 
				$feedn = $argv[2];
				if ($fm->exists($feedn)) {
					$c = $fm->get($feedn);
					print 'name:     ' . $feedn . "\n"; 
					print 'home URL: ' . $c['home_url'] . "\n"; 
					print 'feed URL: ' . $c['feed_url'] . "\n"; 
					print 'img URL:  ' . $c['feed_img'] . "\n"; 
				} 
				else { print "no feed named [" . $feedn . "]\n"; }
			} else {
				$names = $fm->list();
				if ($names && (count($names) > 0)) { print implode(' | ', $names) . "\n"; }
				else { print "no feeds\n"; }
			}
			break;
		case 'backups':
			$list = $fm->listBackups();
			print "Backups are stored in directory [" . AnyNewsConfig::LIST_BACKUP_DIRECTORY . "]\n";
			print "Available backups:\n" . implode("\n", $list) . "\n";
		 	break;
		case 'verify':
			if ($argc != 3) {
				usage();
			} else {
				$feedn = $argv[2];
				if ($fm->exists($feedn)) { $fm->verify($feedn); } 
				else { print "no feed named [" . $feedn . "]\n"; }
			}
			break;
		case 'check':
			if ($argc != 3) {
				usage();
			} else {
				$site_url = $argv[2];
				$fm->check($site_url); 
			}
			break;		
		default:
			print "[" . $cmd . "] is not among the available commands\n";
			usage();
			break;	
	}
} catch (Exception $e) {
	print "ERROR: " . $e->getMessage() . "\n";
	exit(1);
}

exit(0);

function usage() {
	global $argv;
	print "Usage: " . $argv[0] . ' command <arg1>...<arg2>...<arg3>...<arg4>' . "\n";
	print "Commands:\n";
	print "\tlist (print feed names)\n";
	print "\tlist name (print current properties for the named feed)\n";
	print "\tremove name (remove the named feed)*\n";
	print "\tadd name homepage_url feed_url default_image_url (add a new feed)*\n";
	print "\tupdate name homepage_url feed_url default_image_url (update an existing feed)*\n";
	print "\tverify name (verify a feed's URLs are all valid)\n";
	print "\tcheck URL (visit a URL to see if it offers a news feed)\n";
	print "\tbackups (list the available backup files)\n";
	print "* - these commands backup existing file before updating it (use command [backups] to see saved editions\n";
}
?>