#!/bin/bash
set -e
# For use with cron:
# 	Run the content aggregator and uploader together, with timing control
#

# verbose mode?
MODE=$1

# full path to the aggegator's home and command directories
AHOME=${REPUBLISHER_HOME:-$HOME/Software/guardianproject/anynews}
COMMANDS=./commands

PHP="$(which php)"

# path into which data will be stored
DPATH=$($PHP $COMMANDS/read_config.php FEED_DIRECTORY)

# control the hours at which the aggregator/uploader actually run
# (assumes cron is running at hourly intervals)
(cd $AHOME; $PHP $COMMANDS/timer.php h 0 1 2 3 6 9 12 13 14 15 18 20 21 22)
if [[ $? == 0 ]]
then
	if [[ $MODE == "verbose" ]]
	then
		#(cd $AHOME; $PHP $COMMANDS/feed_aggregator.php $DPATH)
		(cd $AHOME; $PHP $COMMANDS/s3uploader.php $DPATH)
	else
		(cd $AHOME; $PHP $COMMANDS/feed_aggregator.php $DPATH) > /dev/null
		(cd $AHOME; $PHP $COMMANDS/s3uploader.php $DPATH) > /dev/null
	fi
fi
