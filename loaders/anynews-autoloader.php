<?php
/**
 * ----------------------------------------------------------------------
 * Autoloader for Guardian Project AnyNews Feed-Uploader Classes
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

$_utilities_ns = 'guardianproject\anynews';

// assignment/mapping array
$mapping = array(
	'guardianproject\anynews\ContentUploader' => './lib/ContentUploader.php',
	'guardianproject\anynews\S3Service'       => './lib/S3Service.php',
	'guardianproject\anynews\S3Credential'    => './lib/S3Credential.php',
	'guardianproject\anynews\IPFSService'     => './lib/IPFSService.php',
	'guardianproject\anynews\FeedManager'     => './lib/FeedManager.php',
	'guardianproject\anynews\RssFilter'       => './lib/RssFilter.php'
);

// This assigns a closure (anonymous function) as our autoloader. The closure can only 
// autoload the classes named in the assignment array, and will throw an exception if
// if can not find a mapping

spl_autoload_register(function ($class) use ($mapping, $_utilities_ns) {
    // if class is invoked without namespace, add it
    if (strpos($class, $_utilities_ns) === false) {
    	$class = $_utilities_ns . '\\' . $class;
    }
    
    if (isset($mapping[$class])) {
        require $mapping[$class];
        //print $class . 'LOADER: loaded ' . $class . ' from ' . $mapping[$class] . "\n";
    }
}, true);
?>
