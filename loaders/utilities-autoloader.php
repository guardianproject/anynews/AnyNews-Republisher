<?php
/**
 * ----------------------------------------------------------------------
 * Autoloader for Guardian Project Proxy Service Utility Classes
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

$_utilities_ns = 'guardianproject\proxyservices\utilities';

// assignment/mapping array
$mapping = array(
	'guardianproject\proxyservices\utilities\DataStore'    => './proxyservices/utilities/DataStore.php',
	'guardianproject\proxyservices\utilities\HttpResponse' => './proxyservices/utilities/HttpResponse.php',
	'guardianproject\proxyservices\utilities\MIMEType'     => './proxyservices/utilities/MIMEType.php',
	'guardianproject\proxyservices\utilities\URL'          => './proxyservices/utilities/URL.php',
	'guardianproject\proxyservices\utilities\UUID'         => './proxyservices/utilities/UUID.php',	
	'guardianproject\proxyservices\utilities\PageParser'   => './proxyservices/utilities/PageParser.php',
	'guardianproject\proxyservices\utilities\SiteIcon'     => './proxyservices/utilities/SiteIcon.php',
	'guardianproject\proxyservices\utilities\Utilities'    => './proxyservices/utilities/Utilities.php',
	'guardianproject\proxyservices\utilities\Cryptor'      => './proxyservices/utilities/Cryptor.php'
);

// This assigns a closure (anonymous function) as our autoloader. The closure can only 
// autoload the classes named in the assignment array, and will throw an exception if
// if can not find a mapping

spl_autoload_register(function ($class) use ($mapping, $_utilities_ns) {
    // if class is invoked without namespace, add it
    if (strpos($class, $_utilities_ns) === false) {
    	$class = $_utilities_ns . '\\' . $class;
    }
    
    if (isset($mapping[$class])) {
        require $mapping[$class];
        //print $class . ' LOADER: loaded ' . $class . ' from ' . $mapping[$class] . "\n";
    }
}, true);
