FROM php:8-cli-bullseye AS base
WORKDIR /app

ENV PHPEXT_DEPS \
      libbz2-dev \
      libcurl3-dev \
      libonig-dev \
      libssl-dev \
      zlib1g-dev \
      libjpeg-dev \
      libzip-dev \
      libfreetype6-dev \
      libwebp-dev \
      libjpeg62-turbo-dev \
      libpng-dev \
      libxpm-dev

# NOTE: PHPIZE_DEPS comes from the base image
# https://github.com/docker-library/php/blob/master/8.2/bullseye/cli/Dockerfile#L20
# these are deps used only during installation of php extensions, but have no place
# in a production image
# documentation on the gd configure flags is here
#  -> https://www.php.net/manual/en/image.installation.php
RUN set -ex; \
    apt-get update; \
    apt-get install --yes --no-install-recommends \
      sqlite3 \
      libbz2-1.0 \
      libonig5 \
      zlib1g \
      libzip4 \
      libfreetype6 \
      libwebp6 \
      libjpeg62-turbo \
      libpng16-16 \
      libxpm4 \
      ${PHPEXT_DEPS} \
    ; \
    docker-php-ext-configure gd \
      --enable-gd \
      --with-webp=/usr/include \
      --with-jpeg=/usr/include \
      --with-freetype=/usr/include \
      --with-xpm=/usr/include \
      --with-freetype=/usr/include \
    ; \
    docker-php-ext-install \
      bz2 \
      curl \
      bcmath \
      mbstring \
      gd \
      zip \
    ; \
    apt-get remove --purge --yes \
      ${PHPEXT_DEPS} \
      ${PHPIZE_DEPS} \
      ; \
    apt-get autoremove --yes; \
    apt-get autoclean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* cache/* /var/lib/log/*;

RUN set -ex; \
    curl https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz -o ffmpeg.tar.xz -s; \
    tar -xf ffmpeg.tar.xz; \
    mv ffmpeg-*-amd64-static/ffmpeg /usr/bin; \
    rm -rf ffmpeg-*-amd64-static; \
    rm -f ffmpeg.tar.xz

RUN echo 'memory_limit=2048M\n' > /usr/local/etc/php/conf.d/docker-php-xxx-custom.ini

FROM composer:2 AS build
WORKDIR /app

COPY composer.json .
COPY composer.lock .
RUN composer install --no-dev --no-scripts --ignore-platform-reqs

COPY . .
RUN composer dumpautoload --optimize

FROM base AS final
COPY --from=build /app /app
ARG UID=1000
ARG GID=1000
RUN set -ex; groupadd --gid ${UID} app; \
  useradd --uid ${GID} --gid app --shell /bin/bash app; \
  mkdir /app/config /app/data; \
  chown -R app:app /app
USER app
ENV REPUBLISHER_HOME /app
ENV PATH /app:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
WORKDIR /app
