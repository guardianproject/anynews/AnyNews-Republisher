<?php
/**
 * ----------------------------------------------------------------------
 * all necessary objects and libraries
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/
require_once './vendor/autoload.php';
$dotenv = \Dotenv\Dotenv::createUnsafeMutable(__DIR__);
$dotenv->safeLoad(__DIR__ . '/.env');

// un-namespaced classes & variables we need to pull in manually
require_once './config/AnyNewsConfig.php';
require_once './config/UtilitiesConfig.php';

// these AutoLoaders pull in our class definitions when we request them with 'use'
//require_once './aws-autoloader.php';
require_once './loaders/utilities-autoloader.php';
require_once './loaders/anynews-autoloader.php';
?>
