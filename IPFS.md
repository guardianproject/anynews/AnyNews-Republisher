# IPFS as an Intermediate Store

Amazon's S3 service is stable, durable and difficult for hostile regimes to block because it is so valuable to businesses those regimes don't want to harm.  However, S3 is not a free service and, depending on the volume of material published (particularly media), costs can become significant.  

As an alternative to S3, we provide an *experimental* solution based on the Interplanetary File System (IPFS).  IPFS uses *decentralized* storage provided by a network of volunteers and a protocol that makes its service harder to block.  

Is there a cost?  Yes: You'll need to run an IPFS "node" on your publishing computer and keep that node mostly-connected to the Internet. You'll not only be publishing your material but, by adding yourself as a node on the IPFS network, you'll be cooperating in moving files among users - including files you didn't create yourself. Don't worry, this is firewalled off from the contents of your hard drive.  In effect, it's a *give to get" service.  By the way, there's no *account* necessary on IPFS, so those steps we've previous documented about credentialing are unnecessary here.

IPFS runs on Linux, Windows and MacOS machines and by default acquires approximately 2GB of disk storage to use as its content buffer. For computers closer to the edge of the network, IPFS does not consume a large amount of compute time or network access.

*IMPORTANT: A running IPFS daemon is required for both Publishers and Republishers*.

## Setting up IPFS

[HERE](https://ipfs.io/#why) is documentation about IPFS generally.  [HERE](https://docs.ipfs.io/install/command-line/#system-requirements) is how you obtain and install IPFS for your machine type (we require only the small command-line package). Once installed, you'll want to run IPFS in `daemon` mode and, if possible, use a tool like `cron` to assure it is running any time your computer is running.  

## AnyNews Publishing using IPFS

It's the *uploader* and *retriever* functions that change when you use IPFS instead of Amazon S3.  So, we'll focus on that here.

## IPFS for Publishers

### Getting Started

Before getting started with publishing on IPFS, you'll need to set up your publishing `key` and `name`.  After setting up IPFS and starting the IPFS daemon, run the following script in your AnyNews installation directory:

```PHP
php commands/ipfsinit.php
```

This command makes sure IPFS is running, creates a tiny piece of content, adds it to IPFS and then *publishes* it to the InterPlanetary Name Space (IPNS).  In effect, this initializes your node and your ability to a publish to an *address* (sort of!) that you can give to others (like you can give them HTTP URLs).  You'll see a result that looks like the following:

```PHP
cmd> php commands/ipfsinit.php
initializing IPFS
file [./.dummy] added IPFS CID [QmSXu1h1wJRGJIBBERISHUCVxEwmKY874PSRXRZ]
---
---
--- Your IPNS NAME is: /ipns/k51qzi5uqu5dhrp4mxb5rohfqMOREJIBBERISHxi4uy4fvxpqbnmyrett67s4q
---
--- ADD THIS to your copy of ipfsretriever.php AND
--- PROVIDE THIS NAME TO THOSE WHO WILL BE RETRIEVING YOUR CONTENT
---
---

```

That string starting with `/ipfs/k51...` is your IPNS "name" - the permalink to which you'll publish.  That is, every time you publish, the *address* of the new contents replaces that of the old contents.  Republishers deference this *address*, and retrieve the contents currently at that *address* (independent of what was published before).  This is exactly the same as having a consistently-name file in an S3 bucket.  

You'll need to supply this string to Republishers who use it in their script, ipfsretriever.php.  If you forget your IPNS name, you can recover it with the following command:

```PHP
cmd> ipfs key list -l 
```

which outputs

```PHP
k51qzi5uqu5dhrp4mxbohfJIBBERISH20xi4uy4fvxpqbnmyrett67s4q self  
```

Just prepend '/ipns/' to that string when you provide it.

### Publishing

Publishers use the IPFS-related uploader script (`ipfsuploader.php`) instead of the one supplied for Amazon S3.  Edit the file `AnyNewsConfig.php` to update for IPFS:

```PHP
	// use AWS S3 [s3] or IPFS [ipfs] as your intermediate store 
	const INTERMEDIATE_STORE = 'ipfs';
	
	...
		
	// Publisher's IPFS IPNS NAME
	const IPNS_NAME       = '/ipns/k51qzi5uqu5dhrp4mMOREJIBBERISH20xi4uy4fvxpqbnmyrett67s4q';

```

Next, edit the shell script to use the IPFS component instead of S3:

```/bin/sh
if [ $? == 0 ]
then
	if [ $MODE = "verbose" ]
	then
		(cd $AHOME; $PHP $COMMANDS/ipfsuploader.php $DPATH)
	else
		(cd $AHOME; $PHP $COMMANDS/ipfsuploader.php $DPATH) > /dev/null
	fi
fi
```

And you're good to go!

### Republishers

Republishers use the IPFS-related retrieval script (`ipfsretriever.php`) instead of the one supplied for Amazon S3.  You'll need the publisher's IPNS name, which they will supply.  Edit the file `AnyNewsConfig.php` to update for IPFS:

```PHP
	// use AWS S3 [s3] or IPFS [ipfs] as your intermediate store 
	const INTERMEDIATE_STORE = 'ipfs';
	
	...
		
	// Publisher's IPFS IPNS NAME
	const IPNS_NAME       = '/ipns/k51qzi5uqu5dhrp4mMOREJIBBERISH20xi4uy4fvxpqbnmyrett67s4q';

```

Next, edit the shell script to use the IPFS component instead of S3:

```/bin/sh
if [ $? == 0 ]
then
	if [ $MODE = "verbose" ]
	then
		(cd $AHOME; $PHP $COMMANDS/ipfsretriever.php $DPATH)
	else
		(cd $AHOME; $PHP $COMMANDS/ipfsretriever.php $DPATH) > /dev/null
	fi
fi
```

And you're good to go!

 
